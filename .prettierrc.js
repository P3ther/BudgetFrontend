module.exports = {
  semi: true,
  singleQuote: true,
  trailingComma: 'all',
  bracketSpacing: true,
  printWidth: 110,
  arrowParens: 'always',
  indent_size: 2,
};
