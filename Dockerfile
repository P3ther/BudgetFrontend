# stage1 as builder
FROM node:14.21

COPY . .

ENV REACT_APP_BACKEND=http://139.162.171.116:8085

RUN apt-get update
RUN apt-get install python3

# Build the project and copy the files
RUN npm install

ENTRYPOINT ["npm","start"]
