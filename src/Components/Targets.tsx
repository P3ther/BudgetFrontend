import { IconButton } from '@material-ui/core';
import { Link } from 'react-router-dom';

import AddBoxIcon from '@material-ui/icons/AddBox';

import './Styles/Targets.css';

export default function Targets() {
  return (
    <>
      <div className="add-button-container">
        <Link to="/addTarget/">
          <IconButton>
            <AddBoxIcon className="add-button-icon" />
          </IconButton>
        </Link>
      </div>
    </>
  );
}
