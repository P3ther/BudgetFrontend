import React, { FC, Fragment } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import { Button, FormHelperText, InputLabel, TextField } from '@material-ui/core';
import { Controller, useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import RegistrationValidation from '../../Common/Validation/RegistrationValidation';
import { RegistrationInterface } from '../../Interfaces/Interfaces';
import User from '../../Interfaces/User.interface';
import LoginContainer from '../AuthContainer/AuthContainer';
/* Components */
import LoginHeader from '../AuthHeader/AuthHeader';
/* Styles */
import './Register.css';

const RegisterElement: FC = () => {
  const history = useHistory();
  const {
    handleSubmit,
    control,
    errors,
    formState: { isDirty },
  } = useForm<RegistrationInterface>({
    mode: 'onChange',
    resolver: yupResolver(RegistrationValidation),
  });

  const onSubmit = (data: RegistrationInterface) => {
    const newUser: User = {
      username: data.username,
      userEmail: data.email,
      password: data.newPassword,
    };

    console.log(newUser);
  };

  return (
    <>
      <LoginHeader message="Register into the app" />
      <form>
        <InputLabel className="login-input-label" error={!!errors.username}>
          Username
        </InputLabel>
        <Controller
          as={
            <TextField
              size="small"
              fullWidth
              className="login-text-field"
              data-testid="username-input-field"
            />
          }
          name="username"
          variant="outlined"
          control={control}
          s
          defaultValue=""
          error={errors.username}
        />
        <FormHelperText error={!!errors.username}>{errors.username?.message}</FormHelperText>
        <InputLabel className="login-input-label" error={!!errors.email}>
          Email
        </InputLabel>
        <Controller
          as={
            <TextField
              size="small"
              fullWidth
              className="login-text-field"
              data-testid="username-input-field"
            />
          }
          name="email"
          variant="outlined"
          control={control}
          s
          defaultValue=""
          error={errors.email}
        />
        <FormHelperText error={!!errors.email}>{errors.email?.message}</FormHelperText>
        <InputLabel className="login-input-label" error={!!errors.newPassword}>
          Password
        </InputLabel>
        <Controller
          as={
            <TextField
              size="small"
              fullWidth
              className="login-text-field"
              data-testid="username-input-field"
            />
          }
          name="newPassword"
          variant="outlined"
          control={control}
          s
          defaultValue=""
          error={errors.newPassword}
        />
        <FormHelperText error={!!errors.newPassword}>{errors.newPassword?.message}</FormHelperText>
        <InputLabel className="login-input-label" error={!!errors.confirmNewPassword}>
          Confirm Password
        </InputLabel>
        <Controller
          as={
            <TextField
              size="small"
              fullWidth
              className="login-text-field"
              data-testid="username-input-field"
            />
          }
          name="confirmNewPassword"
          variant="outlined"
          control={control}
          s
          defaultValue=""
          error={errors.confirmNewPassword}
        />
        <FormHelperText error={!!errors.confirmNewPassword}>
          {errors.confirmNewPassword?.message}
        </FormHelperText>
        <p className="login-button-container">
          <Button
            variant="outlined"
            className="budget-confirm-button"
            disabled={!isDirty}
            data-testid="login-button"
            onClick={handleSubmit(onSubmit)}
          >
            Register
          </Button>
        </p>
      </form>
      <div className="forgot-password-container">
        <span className="link-budget" onClick={() => history.go(-1)}>
          Go Back
        </span>
      </div>
    </>
  );
};

const Register: FC = () => (
  <LoginContainer>
    <RegisterElement />
  </LoginContainer>
);

export default Register;
