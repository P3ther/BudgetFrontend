import { BudgetInterface } from '../../Interfaces/Interfaces';
import User from '../../Interfaces/User.interface';

export interface InvestmentsProps {
  userProperties?: User;
  budget: BudgetInterface;
}
