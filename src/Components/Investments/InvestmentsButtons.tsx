import { Dispatch, Fragment, SetStateAction } from 'react';
import { Select, MenuItem, Button } from '@material-ui/core';

interface Props {
  setAddInvPlatform: Dispatch<SetStateAction<boolean>>;
  setAddInvMoney: Dispatch<SetStateAction<boolean>>;
  setAddInvGain: Dispatch<SetStateAction<boolean>>;
}

export default function InvestmentsButtons(props: Props) {
  return (
    <>
      <Select
        variant="outlined"
        className="investments-manage-investments-button budget-select-small"
        defaultValue={0}
        value=""
        displayEmpty
        renderValue={() => <div>Manage Investments</div>}
        MenuProps={{
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'right',
          },
          transformOrigin: {
            vertical: 'top',
            horizontal: 'right',
          },
        }}
      >
        <MenuItem
          value={1}
          className="investments-manage-investments-list-item"
          button
          onClick={() => props.setAddInvPlatform(true)}
        >
          Manage platforms
        </MenuItem>
        <MenuItem
          value={2}
          className="investments-manage-investments-list-item"
          button
          onClick={() => props.setAddInvMoney(true)}
        >
          Add invested money
        </MenuItem>
        <MenuItem
          value={3}
          className="investments-manage-investments-list-item"
          button
          onClick={() => props.setAddInvGain(true)}
        >
          Add value of portfolio
        </MenuItem>
      </Select>
      <div className="investments-buttons-container">
        <Button
          color="primary"
          title="Submit"
          style={{
            marginTop: '1vh',
            marginRight: '1vh',
          }}
          onClick={() => props.setAddInvPlatform(true)}
        >
          Manage platforms
        </Button>
        <Button
          color="primary"
          title="Submit"
          style={{
            marginTop: '1vh',
            marginRight: '1vh',
          }}
          onClick={() => props.setAddInvMoney(true)}
        >
          Add invested money
        </Button>
        <Button
          color="primary"
          title="Submit"
          style={{ marginTop: '1vh' }}
          onClick={() => props.setAddInvGain(true)}
        >
          Add value of portfolio
        </Button>
      </div>
    </>
  );
}
