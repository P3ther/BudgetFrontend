import React, { useEffect, useState } from 'react';
import { XYPlot, XAxis, YAxis, VerticalGridLines, HorizontalGridLines, LineSeries } from 'react-vis';
import { Typography, Select, MenuItem, InputLabel, Grid, Paper } from '@material-ui/core';
import moment from 'moment';
import AddInvestedMoney from '../../Dialogs/AddInvestedMoney';
import AddInvestmentGain from '../../Dialogs/AddInvestmentGain';
import InvestmentPlatform from '../../Interfaces/InvestmentPlatform.interface';
import AddInvestmentsPlatform from '../../Dialogs/AddInvestmentsPlatform';
import '../../../node_modules/react-vis/dist/style.css';
import './Investments.scss';
import { InvestmentsProps } from './InvestmentsProps';
import { investmentPlatformApi } from '../../Api/InvestmentPlatformApi';
import InvestmentsButtons from './InvestmentsButtons';
import Investment from '../../Interfaces/Investment.interface';
import DateTimeRange from '../../Interfaces/StartEndTime.interface';
import { GraphTimeValues } from '../../Enums/GraphEnums';
import LoadingOverlay from '../LoadingOverlay/LoadingOverlay';
import { parseUnitAndCurrency } from '../../Common/CurrencyHelper';

export default function Investments(props: InvestmentsProps) {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [addInvMoney, setAddInvMoney] = useState<boolean>(false);
  const [addInvGain, setAddInvGain] = useState<boolean>(false);
  const [addInvPlatform, setAddInvPlatform] = useState<boolean>(false);
  const [allPlatforms, setAllPlatforms] = useState<InvestmentPlatform[]>([]);
  const [selectedPlatform, setSelectedPlatform] = useState<InvestmentPlatform | null>(null);
  const [graphDateRange, setGraphDateRange] = useState<DateTimeRange>({
    startDateTime: new Date(moment().startOf('year').toString()),
    endDateTime: new Date(),
  } as DateTimeRange);
  const [graphValue, setGraphValue] = useState<any[]>([]);
  const [graphGain, setGraphGain] = useState<any[]>([]);
  const [totalValue, setTotalValue] = useState<number>(0);
  const [totalGain, setTotalGain] = useState<number>(0);

  useEffect(() => {
    /* Load all required data on load */
    handleLoadInvestmentPlatforms();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleLoadInvestmentPlatforms = async () => {
    await investmentPlatformApi
      .listInvestmentPlatforms()
      .then((res) => {
        let totalValueTemp: number = 0;
        let totalGainTemp: number = 0;

        res.forEach((platform: InvestmentPlatform) => {
          if (platform.totalValue !== undefined) totalValueTemp += platform.totalValue;

          if (platform.totalGain !== undefined) totalGainTemp += platform.totalGain;
        });
        setTotalValue(totalValueTemp);
        setTotalGain(totalGainTemp);
        setAllPlatforms(res);
        setIsLoading(false);
      })
      .catch((err) => console.warn(err));
    if (selectedPlatform !== null) {
      handleSelectPlatform(selectedPlatform);
    }
  };

  const handleSelectPlatform = async (platform: InvestmentPlatform) => {
    await investmentPlatformApi
      .getInvestmentPlatform(platform.id)
      .then((res) => {
        setSelectedPlatform(res);
        setIsLoading(false);
      })
      .catch((err) => console.warn(err));
  };

  const handleSelectAllPlatforms = () => {
    setSelectedPlatform(null);
  };

  const handleDateRangeChange = (value: GraphTimeValues) => {
    if (value === GraphTimeValues.YTD) {
      setGraphDateRange({
        startDateTime: new Date(moment().startOf('year').toString()),
        endDateTime: new Date(),
      });
    } else if (value === GraphTimeValues.Month1) {
      setGraphDateRange({
        startDateTime: new Date(moment().subtract('1', 'month').toString()),
        endDateTime: new Date(),
      });
    } else if (value === GraphTimeValues.Months3) {
      setGraphDateRange({
        startDateTime: new Date(moment().subtract('3', 'month').toString()),
        endDateTime: new Date(),
      });
    } else if (value === GraphTimeValues.Months6) {
      setGraphDateRange({
        startDateTime: new Date(moment().subtract('6', 'month').toString()),
        endDateTime: new Date(),
      });
    } else if (value === GraphTimeValues.Year) {
      setGraphDateRange({
        startDateTime: new Date(moment().subtract('1', 'year').toString()),
        endDateTime: new Date(),
      });
    }
  };

  const parseDataForGraph = () => {
    const returnedValue: any[] = [];
    const returnedGain: any[] = [];

    if (selectedPlatform !== null) {
      for (
        let i = new Date(graphDateRange.startDateTime);
        i <= new Date(graphDateRange.endDateTime);
        i.setDate(i.getDate() + 1)
      ) {
        let tempValue: number = 0;
        let tempGain: number = 0;
        const tempInvestments: Investment[] = selectedPlatform.investmentsOnPlatform.filter(
          (investment: Investment) =>
            new Date(investment.dateTime) > new Date(graphDateRange.startDateTime) &&
            new Date(investment.dateTime) < new Date(i),
        );

        tempInvestments.forEach((investment: Investment) => {
          if (investment.investment !== undefined) {
            tempValue += investment.investment;
          }

          if (investment.gain !== undefined) {
            tempGain += investment.gain;
          }
        });
        returnedGain.push({ x: new Date(i.toString()), y: tempGain });
        returnedValue.push({ x: new Date(i.toString()), y: tempValue });
      }
    } else {
      const tempInvestmentsList: Investment[] = [];

      allPlatforms?.forEach((platform: InvestmentPlatform) =>
        platform.investmentsOnPlatform.forEach((investment: Investment) =>
          tempInvestmentsList.push(investment),
        ),
      );
      for (
        let i = new Date(graphDateRange.startDateTime);
        i <= new Date(graphDateRange.endDateTime);
        i.setDate(i.getDate() + 1)
      ) {
        let tempValue: number = 0;
        let tempGain: number = 0;
        const tempInvestments: Investment[] = tempInvestmentsList.filter(
          (investment: Investment) =>
            new Date(investment.dateTime) > new Date(graphDateRange.startDateTime) &&
            new Date(investment.dateTime) < new Date(i),
        );

        tempInvestments.forEach((investment: Investment) => {
          if (investment.investment !== undefined) {
            tempValue += investment.investment;
          }

          if (investment.gain !== undefined) {
            tempGain += investment.gain;
          }
        });
        returnedGain.push({ x: new Date(i.toString()), y: tempGain });
        returnedValue.push({ x: new Date(i.toString()), y: tempValue });
      }
    }

    setGraphValue(returnedValue);
    setGraphGain(returnedGain);
  };

  useEffect(() => {
    parseDataForGraph();
  }, [selectedPlatform, graphDateRange]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div className="budget-main-container">
      <AddInvestedMoney
        isOpened={addInvMoney}
        setIsOpened={setAddInvMoney}
        handleLoadInvestmentPlatforms={handleLoadInvestmentPlatforms}
        platforms={allPlatforms}
      />
      <AddInvestmentGain
        isOpened={addInvGain}
        setIsOpened={setAddInvGain}
        handleLoadInvestmentPlatforms={handleLoadInvestmentPlatforms}
        platforms={allPlatforms}
      />
      <AddInvestmentsPlatform
        isOpened={addInvPlatform}
        setIsOpened={setAddInvPlatform}
        handleLoadInvestmentPlatforms={handleLoadInvestmentPlatforms}
        platforms={allPlatforms}
      />
      <LoadingOverlay isOpened={isLoading} />
      {!isLoading && (
        <>
          <Grid container>
            <Grid item xs={12} sm={5}>
              <Typography className="budget-header investments-header">Investments</Typography>
            </Grid>
            <Grid item xs={12} sm={7} className="investments-manage-investments-container">
              <InvestmentsButtons
                setAddInvPlatform={setAddInvPlatform}
                setAddInvMoney={setAddInvMoney}
                setAddInvGain={setAddInvGain}
              />
            </Grid>
          </Grid>
          <Grid container style={{ textAlign: 'center' }}>
            <Grid item xs={12} sm={6}>
              <p className="budget-header">
                {selectedPlatform !== null ? (
                  <>
                    <div>Total money invested on platform: {selectedPlatform?.name}</div>
                    <div>
                      {parseUnitAndCurrency(
                        selectedPlatform.totalValue,
                        props.userProperties?.defaultCurrency?.sign,
                      )}
                    </div>
                  </>
                ) : (
                  <>
                    <div>Total money invested: </div>
                    <div>{parseUnitAndCurrency(totalValue, props.userProperties?.defaultCurrency?.sign)}</div>
                  </>
                )}
              </p>
              <p className="budget-header">
                {selectedPlatform !== null ? (
                  <>
                    <div>Total gain on platform: {selectedPlatform?.name}</div>
                    <div>
                      {parseUnitAndCurrency(
                        selectedPlatform.totalGain,
                        props.userProperties?.defaultCurrency?.sign,
                      )}
                    </div>
                  </>
                ) : (
                  <>
                    <div> Total gains of the investment:</div>
                    <div>{parseUnitAndCurrency(totalGain, props.userProperties?.defaultCurrency?.sign)}</div>
                  </>
                )}
              </p>
            </Grid>
            <Grid item xs={12} sm={6}>
              <InputLabel>Display details for the platform:</InputLabel>
              <Select
                fullWidth
                className="budget-select-small"
                variant="outlined"
                defaultValue={-1}
                disabled={allPlatforms?.length === 0}
              >
                <MenuItem value={-1} onClick={() => handleSelectAllPlatforms()}>
                  All platforms
                </MenuItem>
                {allPlatforms?.map((platform: InvestmentPlatform) => (
                  <MenuItem value={platform.id} onClick={() => handleSelectPlatform(platform)}>
                    {platform.name}
                  </MenuItem>
                ))}
              </Select>
              <InputLabel className="budget-input-label">Select Time Period:</InputLabel>
              <Select fullWidth className="budget-select-small" defaultValue={0} variant="outlined">
                <MenuItem value={0} onClick={() => handleDateRangeChange(GraphTimeValues.YTD)}>
                  Year To Date
                </MenuItem>
                <MenuItem value={1} onClick={() => handleDateRangeChange(GraphTimeValues.Month1)}>
                  Last Month
                </MenuItem>
                <MenuItem value={2} onClick={() => handleDateRangeChange(GraphTimeValues.Months3)}>
                  Last Three Months
                </MenuItem>
                <MenuItem value={3} onClick={() => handleDateRangeChange(GraphTimeValues.Months6)}>
                  Last Six Months
                </MenuItem>
                <MenuItem value={4} onClick={() => handleDateRangeChange(GraphTimeValues.Year)}>
                  Last Year
                </MenuItem>
              </Select>
            </Grid>
          </Grid>
          <Paper className="investments-graph-container">
            <XYPlot height={500} width={1210} stackBy="y" xType="time-utc">
              <XAxis tickLabelAngle={-30} />
              <YAxis width={60} orientation="left" />
              <VerticalGridLines />
              <HorizontalGridLines />
              <LineSeries data={graphValue} curve="curveMonotoneX" stroke="#90be6d" />
              <LineSeries data={graphGain} curve="curveMonotoneX" stroke="#70E617" />
            </XYPlot>
          </Paper>
        </>
      )}
    </div>
  );
}
