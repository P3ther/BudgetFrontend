import { Dispatch, SetStateAction, useEffect, useState } from 'react';
import { Button, Card, CardContent, Container, Grid } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { Editor } from 'draft-js';
/* Components */
import { noteApi } from '../../Api/NoteApi';
import LoadingOverlay from '../LoadingOverlay/LoadingOverlay';
/* Interfaces */
import { Note, NoteBE } from '../../Interfaces/Note.interface';
/* Styles */
import './ListNotes.scss';
import { handleNoteBE } from './NoteHelper';

interface Props {
  notes: Note[];
  setNotes: Dispatch<SetStateAction<Note[]>>;
}

export default function ListNotes(props: Props) {
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    noteApi.listNotes().then((res) => {
      const updatedResults: Note[] = [];

      res.forEach((result: NoteBE) => {
        updatedResults.push(handleNoteBE(result));
      });
      props.setNotes(updatedResults);
    });
    setIsLoading(false);
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <div className="budget-main-container list-notes-container">
      <Grid container>
        <Grid item xs={6}>
          <div className="budget-header list-recipes-header">Notes:</div>
        </Grid>
        <Grid item xs={6} className="list-notes-add-note-container">
          <Link to="/createNote/" className="list-recipes-add-button-link">
            <Button variant="outlined" size="small" className="budget-confirm-button">
              Add New Note
            </Button>
          </Link>
        </Grid>
      </Grid>
      <LoadingOverlay isOpened={isLoading} />
      {!isLoading && (
        <div className="list-notes-cards-container">
          {props.notes.map((note: Note) => (
            <Container className="list-notes-card-container">
              <Link to={`/displayNote/${note.id}/`} className="budget-link">
                <Card variant="outlined" className="list-notes-card">
                  <CardContent className="list-notes-card-content">
                    <div className="budget-header-h6 list-note-header-container">{note.noteName}</div>
                    <div className="list-notes-card-text">
                      <div className="list-notes-card-overlay" />
                      <Editor editorState={note.noteDetails} readOnly onChange={() => { }} />
                    </div>
                  </CardContent>
                </Card>
              </Link>
            </Container>
          ))}
        </div>
      )}
    </div>
  );
}
