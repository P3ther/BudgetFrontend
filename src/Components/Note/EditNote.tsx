import { Dispatch, Fragment, SetStateAction, useEffect, useState } from 'react';
import { Button, InputLabel, TextField } from '@material-ui/core';
import { convertToRaw, EditorState } from 'draft-js';
import { Controller, useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import LoadingOverlay from '../LoadingOverlay/LoadingOverlay';
import { noteApi } from '../../Api/NoteApi';
import { handleNoteBE } from './NoteHelper';
import RecipeInstructionsEditor from '../Recipe/RecipeInstructionsEditor';
import { Note, NoteBE } from '../../Interfaces/Note.interface';
import './EditNote.scss';

interface Props {
  notes: Note[];
  setNotes: Dispatch<SetStateAction<Note[]>>;
  editedNoteId: number;
}

export default function EditNote(props: Props) {
  const history = useHistory();
  const [editedNote, setEditedNote] = useState<Note>({} as Note);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [doesntExist, setDoesntExist] = useState<boolean>(false);
  const { handleSubmit, control, errors } = useForm<Note>({
    mode: 'onChange',
    // esolver: yupResolver(ExpenseValidation),
  });

  useEffect(() => {
    const foundNote: Note | undefined = props.notes.find(
      (note: Note) => parseInt(note.id.toString()) === parseInt(props.editedNoteId.toString()),
    );

    if (foundNote !== undefined) {
      setEditedNote(foundNote);
      setIsLoading(false);
    } else {
      noteApi
        .getNote(props.editedNoteId)
        .then((res) => {
          setEditedNote(handleNoteBE(res));
          setIsLoading(false);
        })
        .catch((err) => {
          setDoesntExist(true);
          console.warn(err);
        });
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const onSubmit = (data: Note) => {
    const editedNoteSubmit: NoteBE = {
      id: editedNote.id,
      noteName: data.noteName,
      noteDetails: JSON.stringify(convertToRaw(data.noteDetails.getCurrentContent())),
      dateCreated: new Date(editedNote.dateCreated),
      lastUpdated: new Date(),
    };

    noteApi
      .updateNote(editedNoteSubmit)
      .then(() => history.push(`/displayNote/${editedNote.id}/`))
      .catch((err) => console.warn(err));
  };

  if (doesntExist) {
    return <div className="budget-header-h4">Error 404, this note does not exists</div>;
  }

  return (
    <div className="budget-main-container list-notes-container">
      <LoadingOverlay isOpened={isLoading} />
      {!isLoading && (
        <>
          <InputLabel>Note Name:</InputLabel>
          <Controller
            as={<TextField fullWidth size="small" />}
            name="noteName"
            control={control}
            variant="outlined"
            defaultValue={editedNote.noteName}
          />
          <InputLabel>Note:</InputLabel>
          <Controller
            name="noteDetails"
            control={control}
            error={errors.noteDetails}
            defaultValue={EditorState.createWithContent(editedNote.noteDetails.getCurrentContent())}
            render={({ value, onChange }) => (
              <RecipeInstructionsEditor editorState={value} setEditorState={onChange} />
            )}
          />
          <div className="edit-note-buttons-container">
            <Button className="budget-error-button-text" onClick={() => history.goBack()}>
              Cancel
            </Button>
            <Button variant="outlined" className="budget-confirm-button" onClick={handleSubmit(onSubmit)}>
              Submit
            </Button>
          </div>
        </>
      )}
    </div>
  );
}
