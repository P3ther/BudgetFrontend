import React from 'react';
import { Button, InputLabel, TextField } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { Controller, useForm } from 'react-hook-form';
import { convertToRaw, EditorState } from 'draft-js';
import RecipeInstructionsEditor from '../Recipe/RecipeInstructionsEditor';
/* Components */
import { noteApi } from '../../Api/NoteApi';
/* Interfaces */
import { NewNoteBE, Note } from '../../Interfaces/Note.interface';
/* Styles */
import './ListNotes.scss';

interface Props { }

export default function CreateNote(props: Props) {
  const history = useHistory();
  // const [isLoading, setIsLoading] = useState<boolean>(true);
  const { handleSubmit, control, errors } = useForm<Note>({
    mode: 'onChange',
    // resolver: yupResolver(ExpenseValidation),
  });
  /*
  useEffect(() => {
    setIsLoading(false);
  }, []);
*/
  const onSubmit = (data: Note) => {
    const newNote: NewNoteBE = {
      noteName: data.noteName,
      noteDetails: JSON.stringify(convertToRaw(data.noteDetails.getCurrentContent())),
      dateCreated: new Date(),
      lastUpdated: new Date(),
    };

    noteApi
      .createNote(newNote)
      .then((res) => history.push(`/displayNote/${res.id}/`))
      .catch((err) => console.warn(err));
  };

  return (
    <div className="budget-main-container list-notes-container">
      <InputLabel error={!!errors.noteName}>Note Name:</InputLabel>
      <Controller
        as={<TextField fullWidth size="small" />}
        name="noteName"
        variant="outlined"
        control={control}
        error={errors.noteName}
        helperText={errors.noteName ? errors.noteName.message : ''}
      />
      <InputLabel error={!!errors.noteDetails}>Note:</InputLabel>
      <Controller
        name="noteDetails"
        control={control}
        error={errors.noteDetails}
        defaultValue={EditorState.createEmpty()}
        render={({ value, onChange }) => (
          <RecipeInstructionsEditor editorState={value} setEditorState={onChange} />
        )}
      />
      <div className="create-recipe-button-container">
        <Button className="budget-error-button-text" onClick={() => history.goBack()}>
          Cancel
        </Button>
        <Button variant="outlined" className="budget-confirm-button" onClick={handleSubmit(onSubmit)}>
          Submit
        </Button>
      </div>
    </div>
  );
}
