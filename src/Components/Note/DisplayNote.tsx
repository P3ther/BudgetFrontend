import { Dispatch, Fragment, SetStateAction, useEffect, useState } from 'react';
import { Button, Grid, InputLabel, TextField } from '@material-ui/core';
import { Editor } from 'draft-js';
import { Link } from 'react-router-dom';
import ConfirmDeleteNote from './ConfirmDeleteNote';
import { handleNoteBE } from './NoteHelper';
import LoadingOverlay from '../LoadingOverlay/LoadingOverlay';
import { noteApi } from '../../Api/NoteApi';
import { Note } from '../../Interfaces/Note.interface';
import './DisplayNote.scss';

interface Props {
  notes: Note[];
  setNotes: Dispatch<SetStateAction<Note[]>>;
  displayedNoteId: number;
}

export default function DisplayNote(props: Props) {
  const [displayedNote, setDisplayedNote] = useState<Note>({} as Note);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [doesntExist, setDoesntExist] = useState<boolean>(false);
  const [confirmDelete, setConfirmDelete] = useState<boolean>(false);

  useEffect(() => {
    const foundNote: Note | undefined = props.notes.find(
      (note: Note) => parseInt(note.id.toString()) === parseInt(props.displayedNoteId.toString()),
    );

    if (foundNote !== undefined) {
      setDisplayedNote(foundNote);
      setIsLoading(false);
    } else {
      noteApi
        .getNote(props.displayedNoteId)
        .then((res) => {
          setDisplayedNote(handleNoteBE(res));
          setIsLoading(false);
        })
        .catch((err) => {
          setDoesntExist(true);
          console.warn(err);
        });
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  if (doesntExist) {
    return <div className="budget-header-h4">Error 404, this note does not exists</div>;
  }

  return (
    <div className="budget-main-container list-notes-container">
      <ConfirmDeleteNote isOpened={confirmDelete} setIsOpened={setConfirmDelete} deleteNote={displayedNote} />
      <LoadingOverlay isOpened={isLoading} />
      {!isLoading && (
        <>
          <Grid container>
            <Grid item xs={6} className="display-note-name-container">
              <InputLabel>Note Name:</InputLabel>
            </Grid>
            <Grid item xs={6} className="display-note-buttons-container">
              <Button
                size="small"
                className="budget-error-button-text"
                onClick={() => setConfirmDelete(true)}
              >
                Delete
              </Button>
              <Link to={`/editNote/${displayedNote.id}/`} className="budget-link">
                <Button size="small" className="budget-confirm-button-text ">
                  Edit
                </Button>
              </Link>
            </Grid>
          </Grid>
          <TextField
            fullWidth
            size="small"
            name="noteName"
            variant="outlined"
            defaultValue={displayedNote.noteName}
            disabled
            inputProps={{ className: 'display-note-name' }}
          />
          <InputLabel>Note:</InputLabel>
          <div className="display-note-details-container">
            <Editor editorState={displayedNote.noteDetails} readOnly onChange={() => { }} />
          </div>
        </>
      )}
    </div>
  );
}
