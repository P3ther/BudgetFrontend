import { convertFromRaw, EditorState } from 'draft-js';
import { Note, NoteBE } from '../../Interfaces/Note.interface';

export const handleNoteBE = (noteBE: NoteBE): Note => {
  let updatedDetails: EditorState = EditorState.createEmpty();
  let updatedResult: Note;

  try {
    updatedDetails = EditorState.createWithContent(convertFromRaw(JSON.parse(noteBE.noteDetails)));
  } catch (err) {
    console.warn(err);
  } finally {
    updatedResult = {
      id: noteBE.id,
      noteName: noteBE.noteName,
      noteDetails: updatedDetails,
      dateCreated: noteBE.dateCreated,
      lastUpdated: noteBE.lastUpdated,
    };
  }

  return updatedResult;
};
