import { Dispatch, SetStateAction } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { Note } from '../../Interfaces/Note.interface';
import { noteApi } from '../../Api/NoteApi';

interface Props {
  isOpened: boolean;
  setIsOpened: Dispatch<SetStateAction<boolean>>;
  deleteNote: Note;
}

export default function ConfirmDeleteNote(props: Props) {
  const history = useHistory();
  const doDelete = () => {
    noteApi
      .deleteNote(props.deleteNote.id)
      .then(() => history.push('/notes/'))
      .catch((err) => console.warn(err));
  };

  return (
    <Dialog open={props.isOpened} fullWidth maxWidth="xs">
      <DialogTitle>Delete {props.deleteNote.noteName}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Are you sure you want to delete this note?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          className="budget-confirm-button"
          onClick={() => props.setIsOpened(false)}
          style={{ backgroundColor: '#90be6d' }}
        >
          No
        </Button>
        <Button variant="contained" onClick={() => doDelete()} className="budget-error-button ">
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  );
}
