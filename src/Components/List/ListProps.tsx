import { BudgetInterface, CategoryInterface } from '../../Interfaces/Interfaces';
import Currency from '../../Interfaces/Currency.interface';

export interface ListProps {
  allCategories: CategoryInterface[];
}
