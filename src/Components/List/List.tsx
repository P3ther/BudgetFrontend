import React, { useState, useEffect, FC } from 'react';
import {
  Typography,
  TextField,
  Grid,
  Button,
  InputLabel,
  Backdrop,
  CircularProgress,
} from '@material-ui/core';
import { Controller, useForm } from 'react-hook-form';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import { Link } from 'react-router-dom';
import DateTimeRange from '../../Interfaces/StartEndTime.interface';
import MoneyChangeTable from '../../Common/MoneyChangeTable';
import './List.scss';
import { getBudget, getCategories, getCurrencies, getDateTimeRange } from '../../Redux/Selectors';

const ListComponent: FC = () => {
  const { handleSubmit, control } = useForm<DateTimeRange>();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const dispatch = useDispatch();
  const currencies = useSelector(getCurrencies);
  const budget = useSelector(getBudget);
  const allCategories = useSelector(getCategories);

  const handleChangeDateTime = (data: DateTimeRange) => {
    const changeDate: DateTimeRange = {
      startDateTime: new Date(data.startDateTime),
      endDateTime: new Date(data.endDateTime),
    };

    dispatch({
      type: 'dateRange',
      payload: changeDate,
    });
  };
  const dateRange = useSelector(getDateTimeRange);

  useEffect(() => {
    if (Object.keys(budget).length > 0) {
      setIsLoading(false);
    }
  }, [budget]);

  if (!isLoading) {
    return (
      <div className="list-container">
        <Grid container>
          <Grid item xs={12} sm={6}>
            <Typography variant="h4" className="list-header budget-header">
              List of Incomes/Expenses
            </Typography>
          </Grid>
          <Grid item xs={12} sm={6} className="list-export-button-container">
            <Link to="/export/" className="list-export-button-link">
              <Button className="budget-confirm-button-text">Export</Button>
            </Link>
          </Grid>
        </Grid>
        <form onSubmit={handleSubmit(handleChangeDateTime)}>
          <div className="list-date-form-container">
            <div className="list-date-select-container">
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <InputLabel>From:*</InputLabel>
                  <Controller
                    as={<TextField type="datetime-local" size="small" required fullWidth />}
                    name="startDateTime"
                    variant="outlined"
                    control={control}
                    defaultValue={moment(new Date(dateRange?.startDateTime)).format('YYYY-MM-DDTHH:mm')}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <InputLabel>To:*</InputLabel>
                  <Controller
                    as={<TextField type="datetime-local" size="small" required fullWidth />}
                    name="endDateTime"
                    variant="outlined"
                    control={control}
                    defaultValue={moment(new Date(dateRange.endDateTime)).format('YYYY-MM-DDTHH:mm')}
                  />
                </Grid>
              </Grid>
            </div>
            <div className="list-filter-button-container">
              <Button type="submit" variant="outlined" className="budget-confirm-button">
                Apply Filter
              </Button>
            </div>
          </div>
        </form>
        <Grid container spacing={4} className="list-tables-container">
          <Grid item lg={6} xs={12}>
            <Typography variant="h6" className="list-table-header">
              Incomes:
            </Typography>
            <MoneyChangeTable
              type="Income"
              isCategoryFilterable
              tableValues={budget.incomeList}
              allCategories={allCategories}
              currencies={currencies}
            />
          </Grid>
          <Grid item lg={6} xs={12}>
            <Typography variant="h6" className="list-table-header">
              Expenses:
            </Typography>
            <MoneyChangeTable
              type="Expense"
              isCategoryFilterable
              tableValues={budget.expensesList}
              allCategories={allCategories}
              currencies={currencies}
            />
          </Grid>
        </Grid>
      </div>
    );
  }

  return (
    <Backdrop open={isLoading} transitionDuration={1000}>
      <CircularProgress color="inherit" />
    </Backdrop>
  );
};

export default ListComponent;
