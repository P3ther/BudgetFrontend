import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@material-ui/core';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
/* Styles */
import './LogoutDialog.scss';

interface Props {
  isLogoutDialogOpened: boolean;
  setIsLogoutDialogOpened: Function;
  handleLogout: Function;
}

export default function LogoutDialog({ isLogoutDialogOpened, setIsLogoutDialogOpened, handleLogout }: Props) {
  return (
    <Dialog open={isLogoutDialogOpened} fullWidth PaperProps={{ className: 'logout-dialog' }}>
      <DialogTitle style={{ textAlign: 'center' }}>
        <ErrorOutlineIcon />
      </DialogTitle>
      <DialogContent>
        <DialogContentText>Are you sure you want to log out?</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button className="budget-error-button-text" onClick={() => setIsLogoutDialogOpened(false)}>
          No
        </Button>
        <Button variant="outlined" className="budget-confirm-button" onClick={() => handleLogout()}>
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  );
}
