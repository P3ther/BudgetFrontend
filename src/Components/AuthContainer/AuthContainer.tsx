import React, { FC } from 'react';
import { Paper } from '@material-ui/core';
import AuthHeader from '../AuthHeader/AuthHeader';
import './AuthContainer.css';

interface Props {
  headerText?: string;
}

const AuthContainer: FC<Props> = ({ headerText = '', children }) => (
  <div className="login-container">
    <Paper className="login-paper-container">
      <AuthHeader message={headerText || ''} />
      {children}
    </Paper>
  </div>
);

export default AuthContainer;
