import { Dispatch, Fragment, SetStateAction, useState } from 'react';
import { Grid, IconButton, List, ListItem, ListItemText } from '@material-ui/core';
import AddCircleRoundedIcon from '@material-ui/icons/AddCircleRounded';
import Moment from 'react-moment';
import BudgetPagination from '../Common/BudgetPagination';
import Task from '../Interfaces/Task.interface';
import CreateTask from './Tasks/CreateTask';
import EditTask from './Tasks/EditTask';

import './Styles/ToDoTasksList.scss';

interface Props {
  tasks: Task[];
  setTasks: Dispatch<SetStateAction<Task[]>>;
}
export default function ToDoTasksList({ tasks, setTasks }: Props) {
  const [addNewTask, setAddNewTask] = useState<boolean>(false);
  const [editTask, setEditTask] = useState<boolean>(false);
  const [editedTask, setEditedTask] = useState<Task | undefined>(undefined);
  const rowsPerPage: number = 5;
  const [page, setPage] = useState<number>(0);

  const handlePreviousPage = () => {
    setPage(page - 1);
  };

  const handleNextPage = () => {
    setPage(page + 1);
  };

  const handleEditTask = (id: number) => {
    setEditedTask(tasks.find((x) => x.id === id));
    setEditTask(true);
  };

  return (
    <div className="todo-tasks-container">
      <Grid container>
        <Grid item xs={6} className="budget-header">
          To-Do Tasks
        </Grid>
        <Grid item xs={6} className="todo-taks-list-icon-container">
          <IconButton
            disabled={addNewTask || editTask}
            onClick={() => setAddNewTask(!addNewTask)}
            className="todo-taks-list-list-icon-button"
          >
            <AddCircleRoundedIcon className="todo-taks-list-list-icon " />
          </IconButton>
        </Grid>
      </Grid>
      {!addNewTask && !editTask ? (
        <>
          <List className="todo-tasks-list-container" dense>
            {tasks
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((task: Task, key: number) => (
                <ListItem button className="todo-taks-list-item" onClick={() => handleEditTask(task.id!)}>
                  <ListItemText
                    primary={task.taskName}
                    primaryTypographyProps={{ className: 'todo-taks-list-text' }}
                    secondary={
                      <>
                        <div className="todo-tasks-date-created-container ">
                          {'Date Created: '}
                          <Moment format="DD/MM/YYYY HH:mm" utc local>
                            {task.dateCreated}
                          </Moment>
                        </div>
                        <div className="todo-tasks-date-expired-container">
                          {'Expiration Date: '}
                          <Moment format="DD/MM/YYYY HH:mm" utc local>
                            {task.dateExpired}
                          </Moment>
                        </div>
                      </>
                    }
                    className="todo-taks-list-text-container"
                  />
                </ListItem>
              ))}
          </List>
          <div className="todo-taks-pagination-container">
            {tasks.length > rowsPerPage && (
              <BudgetPagination
                numberOfRows={tasks.length}
                rowsPerPage={rowsPerPage}
                nextPage={handleNextPage}
                previousPage={handlePreviousPage}
                typesOfPagination="category"
              />
            )}
          </div>
        </>
      ) : (
        <div />
      )}
      {addNewTask && <CreateTask setAddNewTask={setAddNewTask} setTasks={setTasks} />}
      {editTask && <EditTask setEditTask={setEditTask} editedTask={editedTask!} setTasks={setTasks} />}
    </div>
  );
}
