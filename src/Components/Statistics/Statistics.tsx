import React, { FC, useEffect, useState } from 'react';
import { Grid } from '@material-ui/core';
import moment from 'moment';
import { useSelector } from 'react-redux';
import { parseUnitAndCurrency } from '../../Common/CurrencyHelper';
import { BudgetInterface } from '../../Interfaces/Interfaces';
import { getBudget, getDateTimeRange, getUserProperties } from '../../Redux/Selectors';
import './Statistics.scss';

const Statistics: FC = () => {
  const [savedValue, setSavedValue] = useState<number>(0);
  const [averageIncome, setAverageIncome] = useState<number>(0);
  const dateRange = useSelector(getDateTimeRange);
  const budget: BudgetInterface = useSelector(getBudget);
  const userProperties = useSelector(getUserProperties);

  useEffect(() => {
    const months: number = moment(dateRange?.startDateTime).diff(dateRange?.endDateTime, 'months', false);

    if (Math.abs(months) < 1) {
      setSavedValue(budget?.totalBalance);
      setAverageIncome(budget?.totalIncome);
    } else {
      setSavedValue(Math.round(budget.totalBalance / Math.abs(months)));
      setAverageIncome(Math.round(budget.totalIncome / Math.abs(months)));
    }
  }, [dateRange, budget, userProperties]);

  return (
    <div className="todo-tasks-container">
      <Grid container>
        <Grid item xs={6} className="budget-header">
          Statistics
        </Grid>
        <Grid item xs={6} className="todo-taks-list-icon-container" />
      </Grid>
      <>
        <Grid container style={{ textAlign: 'center' }} spacing={1}>
          <Grid item xs={6}>
            <div className="statistics-item-container ">
              <div>Average Money saved in selected period:</div>
              {parseUnitAndCurrency(savedValue, userProperties?.defaultCurrency?.sign)}
            </div>
          </Grid>
          <Grid item xs={6}>
            <div className="statistics-item-container ">
              <div>Expected Money saved in selected period:</div>
              {parseUnitAndCurrency(0, userProperties?.defaultCurrency?.sign)}
            </div>
          </Grid>
          <Grid item xs={6}>
            <div className="statistics-item-container ">
              <div>Average income in selected period:</div>
              {parseUnitAndCurrency(averageIncome, userProperties?.defaultCurrency?.sign)}
            </div>
          </Grid>
          <Grid item xs={6}>
            <div className="statistics-item-container ">
              <div>Expected income in selected period:</div>
              {parseUnitAndCurrency(budget?.expectedIncome, userProperties?.defaultCurrency?.sign)}
            </div>
          </Grid>
        </Grid>
      </>
    </div>
  );
};

export default Statistics;
