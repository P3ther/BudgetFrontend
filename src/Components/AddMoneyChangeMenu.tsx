import { Dispatch, SetStateAction } from 'react';
import { Menu, MenuItem } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { ReducerActionsEnum } from '../Redux/ReduxStore';

interface Props {
  menuAnchor: HTMLElement | null;
  setMenuAnchor: Dispatch<SetStateAction<HTMLElement | null>>;
  setIsAddIncomeOpened: Dispatch<SetStateAction<boolean>>;
}

export default function AddMoneyChangeMenu({ menuAnchor, setMenuAnchor, setIsAddIncomeOpened }: Props) {
  const dispatch = useDispatch();
  const open = Boolean(menuAnchor);
  const handleClose = () => {
    setMenuAnchor(null);
  };
  const handleAddIncome = () => {
    setMenuAnchor(null);
    setIsAddIncomeOpened(true);
  };
  const handleAddExpense = () => {
    dispatch({
      type: ReducerActionsEnum.addExpense,
      payload: true,
    });
    setMenuAnchor(null);
  };

  return (
    <Menu
      anchorEl={menuAnchor}
      open={open}
      onClose={handleClose}
      transformOrigin={{ horizontal: 'center', vertical: -45 }}
      anchorOrigin={{ horizontal: 'center', vertical: 'bottom' }}
    >
      <MenuItem onClick={handleAddExpense}>Add Expense</MenuItem>
      <MenuItem onClick={handleAddIncome}>Add Income</MenuItem>
    </Menu>
  );
}
