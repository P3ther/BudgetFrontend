import React, { Fragment, useEffect, useState } from 'react';
import {
  Button,
  Card,
  CardContent,
  Container,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
  CircularProgress,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useSelector } from 'react-redux';
import { CategoryValidation } from '../../Common/Validation/CategoryValidation';
import Currency from '../../Interfaces/Currency.interface';
import { categoryApi } from '../../Api/CategoryApi';

import './EditCategories.scss';
import { CategoryInterface } from '../../Interfaces/Interfaces';
import { getCategories, getCurrencies } from '../../Redux/Selectors';

interface Props {
  categoryId: string;
}

export default function EditCategory({ categoryId }: Props) {
  const history = useHistory();
  const currencies = useSelector(getCurrencies);
  const allCategories = useSelector(getCategories);
  const [editedCategory, setEditedCategory] = useState<CategoryInterface>(
    allCategories.find((x) => x.id === Number(categoryId)) || ({} as CategoryInterface),
  );
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const {
    handleSubmit,
    control,
    errors,
    formState: { isDirty, isValid },
    reset,
  } = useForm<CategoryInterface>({
    mode: 'onChange',
    resolver: yupResolver(CategoryValidation),
  });

  useEffect(() => {
    setEditedCategory(allCategories.find((x) => x.id === Number(categoryId)) || ({} as CategoryInterface));
  }, [allCategories]); // eslint-disable-line react-hooks/exhaustive-deps

  const onSubmit = async (data: CategoryInterface) => {
    setIsLoading(true);
    const editedCategorySubmit: CategoryInterface = {
      id: editedCategory!.id,
      name: data.name,
      limit: data.limit,
      type: data.type,
      currencyId: data.currencyId,
    };

    await categoryApi
      .updateCategory(editedCategorySubmit)
      .then((res) => {
        setEditedCategory(res);
        reset(res);
      })
      .catch((err) => console.warn(err))
      .finally(() => setIsLoading(false));
  };

  return (
    <>
      <div style={{ height: '27px', verticalAlign: 'top' }}>
        <Typography variant="h5">Edit Category: {editedCategory?.name}</Typography>
      </div>
      <Container maxWidth="sm">
        <Card variant="outlined">
          <CardContent>
            {editedCategory === undefined ? (
              <div style={{ width: '100%', textAlign: 'center' }}>
                <CircularProgress />
              </div>
            ) : (
              <>
                <InputLabel style={{ marginTop: '10px' }}>Name of the Category</InputLabel>
                <Controller
                  as={<TextField fullWidth size="small" disabled={isLoading} />}
                  name="name"
                  variant="outlined"
                  control={control}
                  error={errors.name}
                  helperText={errors.name ? errors.name.message : ''}
                  defaultValue={editedCategory?.name}
                />
                <InputLabel>Limiting value of the Category</InputLabel>
                <Controller
                  as={<TextField fullWidth size="small" disabled={isLoading} />}
                  name="limit"
                  variant="outlined"
                  control={control}
                  type="number"
                  error={errors.limit}
                  helperText={errors.limit ? errors.limit.message : ''}
                  defaultValue={editedCategory?.limit}
                />
                <InputLabel error={!!errors.currencyId}>Currency</InputLabel>
                <Controller
                  as={
                    <Select
                      defaultValue={editedCategory?.currencyId}
                      fullWidth
                      style={{ height: '40px' }}
                      disabled={isLoading}
                    >
                      {currencies.map((currency: Currency, key: number) => (
                        <MenuItem key={key} value={currency.id}>
                          {`${currency.name} (${currency.sign})`}
                        </MenuItem>
                      ))}
                    </Select>
                  }
                  name="currencyId"
                  variant="outlined"
                  control={control}
                  type="number"
                  error={errors.currencyId}
                  helperText={errors.currencyId ? errors.currencyId.message : ''}
                  defaultValue={editedCategory?.currencyId}
                />
                <InputLabel style={{ marginTop: '10px' }}>Type of the Category</InputLabel>
                <Controller
                  as={
                    <Select fullWidth style={{ height: '40px' }} disabled={isLoading}>
                      <MenuItem value={1}>Income</MenuItem>
                      <MenuItem value={0}>Expense</MenuItem>
                    </Select>
                  }
                  name="type"
                  control={control}
                  variant="outlined"
                  error={errors.type}
                  defaultValue={editedCategory?.type}
                />
                {errors.type ? (
                  <FormHelperText error style={{ marginLeft: '14px' }}>
                    {errors.type.message}
                  </FormHelperText>
                ) : (
                  ''
                )}
                <div className="edit-categories-button-container">
                  <Button
                    variant="outlined"
                    className="edit-categories-reset-button budget-error-button"
                    onClick={() => history.goBack()}
                  >
                    Back
                  </Button>
                  <Button
                    variant="outlined"
                    className="edit-categories-reset-button budget-warning-button"
                    onClick={() => reset(editedCategory)}
                  >
                    Reset
                  </Button>
                  <Button
                    variant="outlined"
                    onClick={handleSubmit(onSubmit)}
                    className="budget-confirm-button"
                    disabled={!isDirty || !isValid || isLoading}
                  >
                    Submit
                  </Button>
                </div>
              </>
            )}
          </CardContent>
        </Card>
      </Container>
    </>
  );
}
