import React, { useEffect, useState } from 'react';
import {
  TableBody,
  TableCell,
  TableRow,
  Table,
  TableHead,
  CircularProgress,
  Button,
} from '@material-ui/core';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import HorizontalBarGraph from '../../Common/HorizontalBarGraph';
import BudgetPagination from '../../Common/BudgetPagination';
import BudgetMenu from '../../Common/BudgetMenu';
import ConfirmDeleteCategory from '../../Dialogs/ConfirmDeleteCategory';
import { CategoryInterface } from '../../Interfaces/Interfaces';
/* Styles */
import './Categories.scss';
import { parseUnitAndCurrency } from '../../Common/CurrencyHelper';
import { getCategories } from '../../Redux/Selectors';

export default function Categories() {
  const rowsPerPage: number = 5;
  const [page, setPage] = useState<number>(0);
  const visibleCategories = useSelector(getCategories);
  const [categories, setCategories] = useState<CategoryInterface[] | null>(null);
  const [categoryMenuAnchor, setCategoryMenuAnchor] = useState<null | HTMLElement>(null);
  const [confirmDeleteCategory, setConfirmDeleteCategory] = useState<boolean>(false);
  const [selectedCategory, setSelectedCategory] = useState<CategoryInterface>({} as CategoryInterface);

  useEffect(() => {
    if (visibleCategories?.length > 0) {
      setCategories(visibleCategories);
    }
  }, [visibleCategories]);

  const handlePreviousPage = () => {
    setPage(page - 1);
  };

  const handleNextPage = () => {
    setPage(page + 1);
  };
  const openCategoryMenu = (
    event: React.MouseEvent<HTMLButtonElement>,
    categoryToDelete: CategoryInterface,
  ) => {
    setCategoryMenuAnchor(event.currentTarget);
    setSelectedCategory(categoryToDelete);
  };

  const handleOpenConfirmDeleteCategory = () => {
    setConfirmDeleteCategory(true);
  };

  if (categories !== null) {
    return (
      <>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell align="center" style={{ width: '16.5%' }}>
                Name
              </TableCell>
              <TableCell align="center" style={{ width: '16.5%' }}>
                Limit Value
              </TableCell>
              <TableCell
                align="center"
                style={{ width: '16.5%' }}
                className="categories-table-percentage-row"
              >
                Percentage
              </TableCell>
              <TableCell align="center" style={{ width: '16.5%' }}>
                Current Value
              </TableCell>
              <TableCell align="center" style={{ width: '16.5%' }}>
                List
              </TableCell>
              <TableCell align="center" style={{ width: '16.5%' }}>
                More
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {categories
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row: CategoryInterface, key: number) => (
                <TableRow key={row.id} style={{ height: '40px', overflow: 'hidden' }}>
                  <TableCell align="center" className="moneyChangeTableDate">
                    {row.name}
                  </TableCell>
                  <TableCell align="center" className="moneyChangeTableName">
                    {parseUnitAndCurrency(row.limit, row.currencySign)}
                  </TableCell>
                  <TableCell align="center" className="moneyChangeTablePrice categories-table-percentage-row">
                    <HorizontalBarGraph
                      width={28}
                      percentage={(row.value! / row.limit) * 100}
                      barColor={row.type === 1 ? '#6ea672' : '#f9c74f'}
                      color="#f0e7d8"
                    />
                  </TableCell>
                  <TableCell align="center" className="moneyChangeTableCategory">
                    {parseUnitAndCurrency(row.value, row.currencySign)}
                  </TableCell>
                  <TableCell align="center" className="moneyChangeTableCategory">
                    <Link to={`/category/${row.id}/items/`}>
                      <Button size="small" variant="outlined" className="list-button">
                        View Items
                      </Button>
                    </Link>
                  </TableCell>
                  <TableCell align="center" className="moneyChangeTableCategory">
                    <Button
                      size="small"
                      variant="outlined"
                      className="more-options-button"
                      onClick={(e) => openCategoryMenu(e, row)}
                    >
                      <MoreHorizIcon />
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
        <div className="category-pagination">
          <BudgetPagination
            numberOfRows={visibleCategories.length}
            rowsPerPage={rowsPerPage}
            nextPage={handleNextPage}
            previousPage={handlePreviousPage}
            typesOfPagination="category"
          />
        </div>
        <BudgetMenu
          menuAnchor={categoryMenuAnchor}
          setMenuAnchor={setCategoryMenuAnchor}
          options={[
            {
              name: 'Edit',
              isLink: true,
              linkAddres: `/editCategory/${selectedCategory.id}/`,
            },
            {
              name: 'Delete',
              onClickFunction: handleOpenConfirmDeleteCategory,
              isLink: false,
            },
          ]}
        />
        <ConfirmDeleteCategory
          isOpened={confirmDeleteCategory}
          setIsOpened={setConfirmDeleteCategory}
          deletedCategory={selectedCategory}
        />
      </>
    );
  }

  return (
    <div className="categories-loading-container">
      <CircularProgress />
    </div>
  );
}
