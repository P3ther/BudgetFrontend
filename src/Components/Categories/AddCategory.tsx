import React, { useState } from 'react';
import {
  Button,
  Card,
  CardContent,
  Container,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Typography,
} from '@material-ui/core';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { CategoryValidation } from '../../Common/Validation/CategoryValidation';
import Currency from '../../Interfaces/Currency.interface';
import { categoryApi } from '../../Api/CategoryApi';
import { NewCategoryInterface } from '../../Interfaces/Interfaces';
import './AddCategory.scss';
import LoadingOverlay from '../LoadingOverlay/LoadingOverlay';
import { getCurrencies } from '../../Redux/Selectors';

export default function AddCategory() {
  const history = useHistory();
  const currencies = useSelector(getCurrencies);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const {
    handleSubmit,
    control,
    errors,
    formState: { isDirty, isValid },
    reset,
  } = useForm<NewCategoryInterface>({
    mode: 'onChange',
    resolver: yupResolver(CategoryValidation),
  });

  const onSubmit = async (data: NewCategoryInterface) => {
    setIsLoading(true);
    const newCategory: NewCategoryInterface = {
      name: data.name,
      limit: data.limit,
      type: data.type,
      currencyId: data.currencyId,
    };

    await categoryApi
      .createCategory(newCategory)
      .then((res) => {
        reset(res);
      })
      .catch((err) => console.warn(err))
      .finally(() => setIsLoading(false));
  };

  return (
    <div className="budget-main-container">
      <div className="budget-header">
        <Typography variant="h5">Add Category:</Typography>
      </div>
      <Container maxWidth="sm">
        <Card variant="outlined" className="add-categories-card-container">
          <CardContent className="add-categories-card-content">
            <LoadingOverlay isOpened={currencies.length < 1} />
            {!(currencies.length < 1) && (
              <>
                <InputLabel style={{ marginTop: '10px' }}>Name of the Category</InputLabel>
                <Controller
                  as={<TextField fullWidth size="small" disabled={isLoading} />}
                  name="name"
                  variant="outlined"
                  control={control}
                  error={errors.name}
                  helperText={errors.name ? errors.name.message : ''}
                />
                <InputLabel>Limiting value of the Category</InputLabel>
                <Controller
                  as={<TextField fullWidth size="small" disabled={isLoading} />}
                  name="limit"
                  variant="outlined"
                  control={control}
                  type="number"
                  error={errors.limit}
                  helperText={errors.limit ? errors.limit.message : ''}
                  defaultValue={0}
                />
                <InputLabel error={!!errors.currencyId}>Currency</InputLabel>
                <Controller
                  as={
                    <Select fullWidth style={{ height: '40px' }} disabled={isLoading}>
                      {currencies.map((currency: Currency, key: number) => (
                        <MenuItem key={key} value={currency.id}>
                          {`${currency.name} (${currency.sign})`}
                        </MenuItem>
                      ))}
                    </Select>
                  }
                  name="currencyId"
                  variant="outlined"
                  control={control}
                  type="number"
                  error={errors.currencyId}
                  helperText={errors.currencyId ? errors.currencyId.message : ''}
                  defaultValue={currencies[0]?.id}
                />
                <InputLabel style={{ marginTop: '10px' }}>Type of the Category</InputLabel>
                <Controller
                  as={
                    <Select fullWidth style={{ height: '40px' }} disabled={isLoading}>
                      <MenuItem value={1}>Income</MenuItem>
                      <MenuItem value={0}>Expense</MenuItem>
                    </Select>
                  }
                  name="type"
                  control={control}
                  variant="outlined"
                  error={errors.type}
                  defaultValue={0}
                />
                {errors.type ? (
                  <FormHelperText error style={{ marginLeft: '14px' }}>
                    {errors.type.message}
                  </FormHelperText>
                ) : (
                  ''
                )}
                <div className="edit-categories-button-container">
                  <Button
                    variant="outlined"
                    className="edit-categories-reset-button budget-error-button"
                    onClick={() => history.goBack()}
                  >
                    Back
                  </Button>
                  <Button
                    variant="outlined"
                    className="edit-categories-reset-button budget-warning-button"
                    onClick={() =>
                      reset({
                        name: '',
                        limit: 0,
                        type: 0,
                        currencyId: 0,
                      } as NewCategoryInterface)
                    }
                  >
                    Reset
                  </Button>
                  <Button
                    variant="outlined"
                    onClick={handleSubmit(onSubmit)}
                    className="budget-confirm-button"
                    disabled={!isDirty || !isValid || isLoading}
                  >
                    Submit
                  </Button>
                </div>
              </>
            )}
          </CardContent>
        </Card>
      </Container>
    </div>
  );
}
