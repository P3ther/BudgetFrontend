import React, { FC, useEffect, useMemo, useState } from 'react';
import {
  Button,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
} from '@material-ui/core';
import Moment from 'react-moment';
import { useHistory } from 'react-router-dom';
import { RootStateOrAny, useSelector } from 'react-redux';
import MoneyChange from '../../Interfaces/MoneyChange.interface';
import { categoryApi } from '../../Api/CategoryApi';
import { parseUnitAndCurrency } from '../../Common/CurrencyHelper';
/* Styles */
import './ListCategoryItems.css';

interface Props {
  categoryId: string;
}

const ListCategoryItems: FC<Props> = (props) => {
  const history = useHistory();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [moneyChange, setMoneyChange] = useState<MoneyChange[]>([]);
  const dateRange = useSelector((state: RootStateOrAny) => state.dateRange);
  const gridSize = useMemo(() => {
    if (window.screen.width <= 480) return 12;

    return 6;
  }, []);

  useEffect(() => {
    categoryApi
      .readCategoryByTime(props.categoryId, dateRange)
      .then((res) => {
        if (res.type === 1) {
          setMoneyChange(res.incomeList!);
        } else {
          setMoneyChange(res.expenseList!);
        }
      })
      .catch((err) => console.warn(err));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <>
      <TableContainer>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell align="center" style={{ width: '20%' }}>
                Date/Time
              </TableCell>
              <TableCell align="center" style={{ width: '40%' }}>
                Name
              </TableCell>
              <TableCell align="center" style={{ width: '20%' }}>
                Price
              </TableCell>
              <TableCell align="center" style={{ width: '20%' }}>
                Category
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {moneyChange
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row: MoneyChange, key: number) => (
                <TableRow key={row.id} style={{ height: '50px', overflow: 'hidden' }}>
                  <TableCell align="center">
                    <Moment format="DD.MM.YYYY kk:mm" utc local>
                      {row.date}
                    </Moment>
                  </TableCell>
                  <TableCell align="center">
                    <span
                      style={{
                        display: 'inline-block',
                        width: '20vh',
                        whiteSpace: 'nowrap',
                        overflow: 'hidden',
                        textOverflow: 'ellipsis',
                      }}
                    >
                      {row.name}
                    </span>
                  </TableCell>
                  <TableCell align="center">{parseUnitAndCurrency(row.price, row.currencySign)}</TableCell>
                  <TableCell align="center">{row.categoryName}</TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Grid container>
        <Grid
          item
          xs={gridSize}
          className="list-category-item-go-back-button-container"
          onClick={() => history.goBack()}
        >
          <Button className="budget-confirm-button" fullWidth={gridSize === 12}>
            Back
          </Button>
        </Grid>
        <Grid item xs={gridSize}>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25, 100]}
            component="div"
            count={moneyChange.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </Grid>
      </Grid>
    </>
  );
};

export default ListCategoryItems;
