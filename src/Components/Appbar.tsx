import React, { Dispatch, SetStateAction, useState } from 'react';
import { Paper, IconButton, Typography } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import AddBoxIcon from '@material-ui/icons/AddBox';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import { useDispatch, useSelector } from 'react-redux';
import LeftMobileMenu from './LeftMenu/Mobile/LeftMobileMenu';
import LogoutDialog from './Login/LogoutDialog';
import AddMoneyChangeMenu from './AddMoneyChangeMenu';
import './Styles/Appbar.scss';
import { getAuthenticated } from '../Redux/Auth/auth.selectors';
import { logout } from '../Redux/Auth/auth.actions';

interface Props {
  isLogedin: boolean;
  setIsLogedin: Dispatch<SetStateAction<boolean>>;
  setIsProfileDrawer: Dispatch<SetStateAction<boolean>>;
  showAddMoneyChange: boolean;
  setIsAddIncomeOpened: Dispatch<SetStateAction<boolean>>;
}

export default function Appbar({
  isLogedin,
  setIsLogedin,
  setIsProfileDrawer,
  showAddMoneyChange,
  setIsAddIncomeOpened,
}: Props) {
  const history = useHistory();
  const dispatch = useDispatch();
  const isAuthenticated = useSelector(getAuthenticated);
  const [isLogoutDialogOpened, setIsLogoutDialogOpened] = useState<boolean>(false);
  const [addMoneyChangeMenuAnchor, setAddMoneyChangeMenuAnchor] = useState<null | HTMLElement>(null);
  const [leftMenuMobile, setLeftMenuMobile] = useState<boolean>(false);
  const openAddMoneyChangeMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAddMoneyChangeMenuAnchor(event.currentTarget);
  };
  const handleLogout = () => {
    dispatch(logout());
    setIsLogoutDialogOpened(false);
    history.push('/');
  };

  const handleClickLogoDesktop = () => {
    if (window.location.pathname === '/') history.go(0);
    else history.push('/');
  };

  const handleClickLogoMobile = () => {
    setLeftMenuMobile(!leftMenuMobile);
  };

  return (
    <Paper elevation={4} className="appbar-main">
      <div className="appbar-app-name-link-container">
        <LeftMobileMenu isOpened={leftMenuMobile} setIsOpened={setLeftMenuMobile} />
        <Typography
          component="div"
          className="appbar-app-name-container-desktop"
          data-testid="budget-logo"
          onClick={handleClickLogoDesktop}
        >
          <div className="appbar-app-icon-container">
            <AssignmentIndIcon className="appbar-app-icon" />
            <span className="appbar-app-name">Budget App</span>
          </div>
        </Typography>
        <Typography
          component="div"
          className="appbar-app-name-container-mobile"
          data-testid="budget-logo"
          onClick={handleClickLogoMobile}
        >
          <div className="appbar-app-icon-container">
            <AssignmentIndIcon className="appbar-app-icon" />
            <span className="appbar-app-name">Budget App</span>
          </div>
        </Typography>
      </div>
      <div className="appbar-right-icon-container" data-testid="budget-appbar-icons">
        {showAddMoneyChange && isAuthenticated ? (
          <IconButton onClick={openAddMoneyChangeMenu} className="appbar-right-icon-button">
            <AddBoxIcon className="appbar-right-icon" />
          </IconButton>
        ) : (
          ''
        )}
        {isAuthenticated ? (
          <IconButton onClick={() => setIsProfileDrawer(true)} className="appbar-right-icon-button">
            <AccountBoxIcon className="appbar-right-icon" />
          </IconButton>
        ) : (
          ''
        )}
        {isAuthenticated ? (
          <IconButton
            onClick={() => setIsLogoutDialogOpened(true)}
            id="logout-button"
            className="appbar-right-icon-button"
          >
            <ExitToAppIcon className="appbar-right-icon" />
          </IconButton>
        ) : (
          <IconButton className="appbar-right-icon-button">
            <VpnKeyIcon className="appbar-right-icon" />
          </IconButton>
        )}
      </div>
      <LogoutDialog
        isLogoutDialogOpened={isLogoutDialogOpened}
        setIsLogoutDialogOpened={setIsLogoutDialogOpened}
        handleLogout={handleLogout}
      />
      <AddMoneyChangeMenu
        menuAnchor={addMoneyChangeMenuAnchor}
        setMenuAnchor={setAddMoneyChangeMenuAnchor}
        setIsAddIncomeOpened={setIsAddIncomeOpened}
      />
    </Paper>
  );
}
