import React, { Dispatch, SetStateAction } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  InputLabel,
  MenuItem,
  TextField,
} from '@material-ui/core';

import './Styles/CreateIngredientDialog.scss';
import { Controller, useForm } from 'react-hook-form';
import { LoadIngredients } from '../../Common/DataHandling/LoadIngredients';
import { ingredientApi } from '../../Api/ingredientApi';
import { ingredientUnits } from '../../Enums/IngredientUnit';
import { RecipeIngredients } from '../../Interfaces/Food/RecipeIngredients.interface';
import { Ingredient, NewIngredient } from '../../Interfaces/Food/Ingredient.interface';

interface Props {
  isOpened: boolean;
  setIsOpened: Dispatch<SetStateAction<boolean>>;
  setAllIngredients: Dispatch<SetStateAction<Ingredient[]>>;
  selectedRecipeIngredients: RecipeIngredients[];
}

export default function CreateIngredientDialog({
  isOpened,
  setIsOpened,
  setAllIngredients,
  selectedRecipeIngredients,
}: Props) {
  const { handleSubmit, control, errors } = useForm<NewIngredient>({
    mode: 'onChange',
    // resolver: yupResolver(ExpenseValidation),
  });

  const handleClose = () => {
    setIsOpened(false);
  };

  const onSubmit = async (data: NewIngredient) => {
    const newIngredient: NewIngredient = {
      ingredientName: data.ingredientName,
      ingredientCalories: data.ingredientCalories,
      ingredientUnit: data.ingredientUnit,
    };

    await ingredientApi
      .createIngredient(newIngredient)
      .then((res) => {
        LoadIngredients(setAllIngredients, selectedRecipeIngredients).then(() => setIsOpened(false));
      })
      .catch((err) => console.warn(err));
  };

  return (
    <Dialog open={isOpened} onClose={handleClose} fullWidth maxWidth="sm">
      <DialogTitle>Create new Ingredient</DialogTitle>
      <DialogContent>
        <DialogContentText>
          <form>
            <InputLabel error={!!errors.ingredientName} className="create-ingredient-dialog-input-label">
              Ingredient Name:
            </InputLabel>
            <Controller
              as={<TextField fullWidth size="small" />}
              name="ingredientName"
              variant="outlined"
              control={control}
              error={errors.ingredientName}
              helperText={errors.ingredientName ? errors.ingredientName.message : ''}
            />
            <InputLabel error={!!errors.ingredientName} className="create-ingredient-dialog-input-label">
              Ingredient Calories:
            </InputLabel>
            <Controller
              as={<TextField fullWidth size="small" />}
              name="ingredientCalories"
              type="number"
              variant="outlined"
              control={control}
              error={errors.ingredientName}
              helperText={errors.ingredientName ? errors.ingredientName.message : ''}
            />
            <InputLabel error={!!errors.ingredientName}>Default Unit:</InputLabel>
            <Controller
              as={
                <TextField select fullWidth size="small">
                  {ingredientUnits.map((unit: string, key: number) => (
                    <MenuItem key={key} value={unit}>
                      {unit}
                    </MenuItem>
                  ))}
                </TextField>
              }
              name="ingredientUnit"
              variant="outlined"
              type="number"
              control={control}
              error={errors.ingredientName}
              helperText={errors.ingredientName ? errors.ingredientName.message : ''}
            />
          </form>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="secondary">
          Close
        </Button>
        <Button
          variant="outlined"
          color="primary"
          className="budget-confirm-button"
          onClick={handleSubmit(onSubmit)}
        >
          Submit
        </Button>
      </DialogActions>
    </Dialog>
  );
}
