import { Fragment, useEffect, useState } from 'react';
import { Collapse, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import { ingredientApi } from '../../Api/ingredientApi';
import EditIngredient from './EditIngredient';

import './ManageIngredients.scss';
import UnsavedChanges from '../../Dialogs/UnsavedChanges';
import { Ingredient } from '../../Interfaces/Food/Ingredient.interface';

export default function ManageIngredients() {
  const [ingredients, setIngredients] = useState<Ingredient[]>([]);
  const [selectedIngredient, setSelectedIngredient] = useState<number>(-1);
  const [tempSelectedIngredient, setTempSelectedIngredient] = useState<number>(-1);
  const [isEdited, setIsEdited] = useState<boolean>(false);
  const [unsavedChanges, setUnsavedChanges] = useState<boolean>(false);

  useEffect(() => {
    loadIngredientsData();
  }, []);

  const loadIngredientsData = () => {
    ingredientApi.listIngredient().then((res) => setIngredients(res));
  };

  const handleSelectIngredient = (id?: number, changes?: boolean) => {
    if (changes === true) {
      if (id !== undefined) {
        setSelectedIngredient(id);
      } else {
        setSelectedIngredient(-1);
      }

      setUnsavedChanges(false);
    } else if (isEdited) {
      setUnsavedChanges(true);
      if (id !== undefined) {
        setTempSelectedIngredient(id);
      } else {
        setTempSelectedIngredient(-1);
      }
    } else if (id !== undefined) {
      setSelectedIngredient(id);
    } else {
      setSelectedIngredient(-1);
    }
  };

  return (
    <>
      <div className="budget-main-container">
        <div className="list-recipes-header-container">
          <div className="budget-header list-recipes-header">List Of All Ingredients</div>
        </div>
        <List component="div" dense style={{ width: '100%', display: 'block' }}>
          {ingredients.map((ingredient: Ingredient) => (
            <>
              <ListItem
                button
                onClick={
                  ingredient.id === selectedIngredient
                    ? () => handleSelectIngredient()
                    : () => handleSelectIngredient(ingredient.id)
                }
              >
                <ListItemIcon />
                <ListItemText primary={ingredient.ingredientName} secondary=" " />
                {ingredient.id === selectedIngredient ? <ExpandLess /> : <ExpandMore />}
              </ListItem>
              <Collapse in={ingredient.id === selectedIngredient} unmountOnExit>
                <EditIngredient
                  ingredient={ingredient}
                  isEdited={isEdited}
                  setIsEdited={setIsEdited}
                  setIngredients={setIngredients}
                  ingredients={ingredients}
                  setUnsavedChanges={setUnsavedChanges}
                  loadIngredientsData={loadIngredientsData}
                />
              </Collapse>
            </>
          ))}
        </List>
      </div>
      <UnsavedChanges
        isOpened={unsavedChanges}
        setIsOpened={setUnsavedChanges}
        handleYes={() => handleSelectIngredient(tempSelectedIngredient, true)}
      />
    </>
  );
}
