import React, { Dispatch, SetStateAction, useEffect } from 'react';
import { Button, InputLabel, MenuItem, TextField } from '@material-ui/core';
import { Controller, useForm } from 'react-hook-form';
import { ingredientApi } from '../../Api/ingredientApi';

import './ManageIngredients.scss';
import { LoadIngredients } from '../../Common/DataHandling/LoadIngredients';
import { ingredientUnits } from '../../Enums/IngredientUnit';
import { Ingredient } from '../../Interfaces/Food/Ingredient.interface';

interface Props {
  ingredient: Ingredient;
  isEdited: boolean;
  setIsEdited: Dispatch<SetStateAction<boolean>>;
  setIngredients: Dispatch<SetStateAction<Ingredient[]>>;
  ingredients: Ingredient[];
  setUnsavedChanges: Dispatch<SetStateAction<boolean>>;
  loadIngredientsData: () => void;
}

export default function EditIngredient({
  ingredient,
  isEdited,
  setIsEdited,
  setIngredients,
  ingredients,
  setUnsavedChanges,
  loadIngredientsData,
}: Props) {
  const {
    handleSubmit,
    control,
    errors,
    formState: { isDirty },
    reset,
  } = useForm<Ingredient>({
    mode: 'onChange',
    // resolver: yupResolver(ExpenseValidation),
  });

  useEffect(() => {
    reset(ingredient);
  }, [ingredient]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (isDirty) {
      setIsEdited(true);
    } else {
      setIsEdited(false);
    }
  }, [isDirty]); // eslint-disable-line react-hooks/exhaustive-deps

  const onSubmit = (data: Ingredient) => {
    const editedIngredient: Ingredient = {
      id: ingredient.id,
      ingredientName: data.ingredientName,
      ingredientCalories: data.ingredientCalories,
      ingredientUnit: data.ingredientUnit,
    };

    ingredientApi
      .updateIngredient(editedIngredient)
      .then((res) => LoadIngredients(setIngredients, null))
      .catch((err) => console.warn(err));
  };

  const handleDelete = () => {
    ingredientApi
      .deleteIngredient(ingredient.id)
      .then(() => {
        loadIngredientsData();
        setIsEdited(false);
      })
      .catch((err) => console.warn(err));
  };

  return (
    <div style={{ height: '22vh' }}>
      <InputLabel error={!!errors.ingredientName}>Ingredient Name:</InputLabel>
      <Controller
        as={<TextField fullWidth size="small" />}
        name="ingredientName"
        variant="outlined"
        control={control}
        error={errors.ingredientName}
        helperText={errors.ingredientName ? errors.ingredientName.message : ''}
        defaultValue={ingredient.ingredientName}
      />
      <InputLabel error={!!errors.ingredientName}>Ingredient Calories:</InputLabel>
      <Controller
        as={<TextField fullWidth size="small" />}
        name="ingredientCalories"
        variant="outlined"
        type="number"
        control={control}
        error={errors.ingredientName}
        helperText={errors.ingredientName ? errors.ingredientName.message : ''}
        defaultValue={ingredient.ingredientCalories}
      />
      <InputLabel error={!!errors.ingredientName}>Default Unit:</InputLabel>
      <Controller
        as={
          <TextField select fullWidth size="small">
            {ingredientUnits.map((unit: string, key: number) => (
              <MenuItem key={key} value={unit}>
                {unit}
              </MenuItem>
            ))}
          </TextField>
        }
        name="ingredientUnit"
        variant="outlined"
        type="number"
        control={control}
        error={errors.ingredientName}
        helperText={errors.ingredientName ? errors.ingredientName.message : ''}
        defaultValue={ingredient.ingredientUnit}
      />
      <Button
        variant="outlined"
        className="budget-confirm-button"
        onClick={handleSubmit(onSubmit)}
        size="small"
      >
        Submit
      </Button>
      <Button
        className="budget-error-button-text"
        onClick={() => handleDelete()}
        size="small"
        disabled={
          ingredient.recipeIngredients === undefined ? false : ingredient?.recipeIngredients?.length > 0
        }
      >
        Delete
      </Button>
    </div>
  );
}
