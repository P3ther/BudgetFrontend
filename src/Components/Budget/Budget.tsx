import React, { Dispatch, SetStateAction, useEffect, useState } from 'react';
import { Grid } from '@material-ui/core';
import { useSelector } from 'react-redux';
import Categories from '../Categories/Categories';
import BudgetCard from '../BudgetCards/BudgetCard';
import BudgetDialog from '../../Dialogs/BudgetDialog';
import BalanceCard from '../BudgetCards/BalanceCard';
import InvestmentCard from '../BudgetCards/InvestmentCard';
import LoadingOverlay from '../LoadingOverlay/LoadingOverlay';
import Statistics from '../Statistics/Statistics';
import TimeCard from '../BudgetCards/TimeCard';
import ToDoTasksList from '../ToDoTasksList';
import Task from '../../Interfaces/Task.interface';
import { getLoadingState, getUserProperties } from '../../Redux/Selectors';
import './Budget.scss';
import { getAuthenticated } from '../../Redux/Auth/auth.selectors';

interface Props {
  setShowAddMoneyChange: Dispatch<SetStateAction<boolean>>;
  loadingData: boolean;
  tasks: Task[];
  setTasks: Dispatch<SetStateAction<Task[]>>;
}

export default function BudgetComponent({ setShowAddMoneyChange, loadingData, tasks, setTasks }: Props) {
  const [isBudgetDialogOpened, setIsBudgetDialogOpened] = useState(false);
  const firstDayOfMonth: Date = new Date();
  const loading = useSelector(getLoadingState);
  const userProperties = useSelector(getUserProperties);
  const isAuthenticated = useSelector(getAuthenticated);

  firstDayOfMonth.setDate(1);

  useEffect(() => {
    console.log(`Is authenticated? ${isAuthenticated}`);
    setShowAddMoneyChange(false);

    return () => {
      setShowAddMoneyChange(true);
    };
  }, [isAuthenticated]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    console.log(`Is Authenticated? ${isAuthenticated}`);
    console.log(`Loading? ${loading}`);
  }, [isAuthenticated, loading]);

  return (
    <div className="budget-main-container">
      <LoadingOverlay isOpened={loading} />
      {!loading && (
        <>
          <Grid container>
            <Grid item xs={12} sm={6} lg={3} className="budget-card-container">
              <BudgetCard defaultCurrency={userProperties?.defaultCurrency!} loadingData={loadingData} />
            </Grid>
            <Grid item xs={12} sm={6} lg={3} className="budget-card-container">
              <TimeCard setDialogOpen={setIsBudgetDialogOpened} />
            </Grid>
            <Grid item xs={12} sm={6} lg={3} className="budget-card-container">
              <BalanceCard defaultCurrency={userProperties?.defaultCurrency!} loadingData={loadingData} />
            </Grid>
            <Grid item xs={12} sm={6} lg={3} className="budget-card-container">
              <InvestmentCard loadingData={loadingData} />
            </Grid>
          </Grid>
          <Grid container spacing={4}>
            <Grid item xs={12} lg={6}>
              <ToDoTasksList tasks={tasks} setTasks={setTasks} />
            </Grid>
            <Grid item xs={12} lg={6}>
              <Statistics />
            </Grid>
          </Grid>
          <Grid container>
            <Grid item xs={6}>
              <div className="budget-header">Categories </div>
            </Grid>
            <Grid item xs={6} style={{ textAlign: 'right' }} />
          </Grid>
          <Categories />
        </>
      )}
      <BudgetDialog
        isBudgetDialogOpened={isBudgetDialogOpened}
        setIsBudgetDialogOpened={setIsBudgetDialogOpened}
      />
    </div>
  );
}
