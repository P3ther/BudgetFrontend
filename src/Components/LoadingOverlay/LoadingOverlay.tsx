import { useEffect, useState } from 'react';

import { CircularProgress } from '@material-ui/core';

/* Style */
import './LoadingOverlay.scss';

interface Props {
  isOpened: boolean;
}
const LoadingOverlay = (props: Props) => {
  const [displayOverlay, setdisplayOverlay] = useState<boolean>(false);
  const [loadingOverlayContainerClass, setloadingOverlayContainerClass] =
    useState<string>('loading-overlay-container');

  useEffect(() => {
    if (props.isOpened === false) {
      setloadingOverlayContainerClass('loading-overlay-container loading-overlay-container-removal');
      const timer1 = setTimeout(() => setdisplayOverlay(false), 800);

      return () => {
        clearTimeout(timer1);
      };
    }

    setdisplayOverlay(true);
    setloadingOverlayContainerClass('loading-overlay-container');
  }, [props.isOpened]);

  if (displayOverlay) {
    return (
      <div className={loadingOverlayContainerClass} data-testid="loading-overlay">
        <div className="loading-overlay-message-container">
          <div className="loading-overlay-message-container">Loading Data...</div>
          <CircularProgress />
        </div>
      </div>
    );
  }

  return null;
};

export default LoadingOverlay;
