import { FormEvent, useState } from 'react';
import { Paper, TextField, InputLabel, Button, Typography } from '@material-ui/core';
import { Link, useHistory } from 'react-router-dom';
import InputMask from 'react-input-mask';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useDispatch } from 'react-redux';
import { ResetPasswordValidation } from '../../Common/Validation/ResetPasswordValidation';
import { userApi } from '../../Api/UserApi';
import User from '../../Interfaces/User.interface';
import { ReducerActionsEnum } from '../../Redux/ReduxStore';
/* Interfaces */
import { ResetPasswordInterface } from '../../Interfaces/Interfaces';
/* Styles */
import './ResetPassword.scss';

enum ResetPasswordState {
  verifyCode,
  resetPassword,
}

export default function RessetPassword() {
  const history = useHistory();
  const dispatch = useDispatch();
  const [resetPasswordState, setResetPasswordState] = useState<ResetPasswordState>(
    ResetPasswordState.verifyCode,
  );
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string>('');
  const [userObject, setUserObject] = useState<User>({} as User);
  const { handleSubmit, control, errors } = useForm<ResetPasswordInterface>({
    mode: 'onBlur',
    resolver: yupResolver(ResetPasswordValidation),
  });

  const handleChangeUser = (username: string, verificationCode: string) => {
    const user = userObject;

    if (username.includes('@')) {
      user.userEmail = username;
    } else if (username.length > 0) {
      user.username = username;
    } else if (verificationCode.length > 0) {
      user.forgotPasswordCode = verificationCode.replace('-', '');
    }

    setUserObject(user);
  };

  const verifyCode = async (evt: FormEvent<HTMLFormElement>) => {
    evt.preventDefault();
    userApi
      .verifyRegistrationCode(userObject)
      .then(() => setResetPasswordState(ResetPasswordState.resetPassword))
      .catch(() => setErrorMessage('This was an error'))
      .finally(() => setIsLoading(false));
    setIsLoading(true);
  };

  const resetPassword = (data: ResetPasswordInterface) => {
    const user: User = userObject;

    user.password = data.newPassword;
    userApi
      .resetPassword(user)
      .then(() => {
        dispatch({
          type: ReducerActionsEnum.successSnackbar,
          payload: true,
        });
        dispatch({
          type: ReducerActionsEnum.succesSnackbarMessage,
          payload: 'Password successfully updated!',
        });
        history.push('/');
      })
      .catch((err) => {
        if (err.response?.status === 403) console.warn('Forgot Password Code is incorrect');
      })
      .finally(() => setIsLoading(false));
  };

  return (
    <div className="login-container">
      <Paper className="login-paper-container">
        {resetPasswordState === ResetPasswordState.verifyCode && (
          <>
            <form onSubmit={(evt) => verifyCode(evt)}>
              <div className="login-header-container">
                <Typography className="budget-header-h2 login-budget-header">Budget App</Typography>
                <span>Input verification code to reset password</span>
              </div>
              <InputLabel className="login-input-label" error={errorMessage.length > 0}>
                Username or email:
              </InputLabel>
              <TextField
                size="small"
                disabled={isLoading}
                fullWidth
                className="login-text-field"
                inputProps={{ className: 'login-text-field-input' }}
                id="reset-password-code"
                variant="outlined"
                helperText={errorMessage}
                error={errorMessage.length > 0}
                onChange={(evt) => handleChangeUser(evt.target.value, '')}
              />
              <InputLabel className="login-input-label" error={errorMessage.length > 0}>
                Verification Code:
              </InputLabel>
              <InputMask
                mask="9999-9999"
                value={userObject.registrationCode}
                onChange={(evt) => handleChangeUser('', evt.target.value)}
                disabled={isLoading}
              >
                {() => (
                  <TextField
                    size="small"
                    disabled={isLoading}
                    fullWidth
                    className="login-text-field"
                    inputProps={{ className: 'login-text-field-input' }}
                    id="reset-password-code"
                    variant="outlined"
                    helperText={errorMessage}
                    error={errorMessage.length > 0}
                  />
                )}
              </InputMask>
              <p className="login-button-container">
                <Button
                  variant="outlined"
                  type="submit"
                  className="budget-confirm-button"
                  id="submit-login"
                  disabled={isLoading}
                >
                  Send
                </Button>
              </p>
            </form>
            <div className="forgot-password-container">
              <Link className="link-budget" to="/">
                Back to Login
              </Link>
            </div>
          </>
        )}
        {resetPasswordState === ResetPasswordState.resetPassword && (
          <>
            <div className="login-header-container">
              <Typography className="budget-header-h2 login-budget-header">Budget App</Typography>
              <span>New password:</span>
            </div>
            <form onSubmit={handleSubmit(resetPassword)}>
              <InputLabel className="login-input-label">Type in new password:</InputLabel>
              <Controller
                as={
                  <TextField
                    size="small"
                    className="login-text-field"
                    InputProps={{ className: 'login-text-field-input' }}
                    id="login-username"
                  />
                }
                name="newPassword"
                variant="outlined"
                control={control}
                disabled={isLoading}
                defaultValue=""
                error={errors.newPassword}
                helperText={errors.newPassword?.message}
              />
              <InputLabel className="login-input-label">Confirm new password:</InputLabel>
              <Controller
                as={
                  <TextField
                    size="small"
                    className="login-text-field"
                    inputProps={{ form: { autocomplete: 'off' }, className: 'login-text-field-input' }}
                    id="login-username"
                  />
                }
                name="confirmNewPassword"
                variant="outlined"
                control={control}
                disabled={isLoading}
                defaultValue=""
                error={errors.confirmNewPassword}
                helperText={errors.confirmNewPassword?.message}
              />
              <p className="login-button-container">
                <Button variant="outlined" type="submit" className="budget-confirm-button" id="submit-login">
                  Send
                </Button>
              </p>
            </form>
            <div className="forgot-password-container">
              <span
                className="link-budget"
                onClick={() => setResetPasswordState(ResetPasswordState.verifyCode)}
              >
                Back
              </span>
            </div>
          </>
        )}
      </Paper>
    </div>
  );
}
