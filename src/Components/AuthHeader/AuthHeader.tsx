import React, { FC } from 'react';
import { Typography } from '@material-ui/core';

interface Props {
  message: string;
}

const AuthHeader: FC<Props> = ({ message }) => (
  <div className="login-header-container">
    <Typography className="budget-header-h2 login-budget-header">Budget App</Typography>
    <span>{message}</span>
  </div>
);

export default AuthHeader;
