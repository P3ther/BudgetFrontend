import { Drawer, Grid, List, ListItem, ListItemIcon, ListItemText, Typography } from '@material-ui/core';
import { Dispatch, FC, SetStateAction } from 'react';
import { useHistory } from 'react-router-dom';
/* Icons */
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import DashboardIcon from '@material-ui/icons/Dashboard';
import DateRangeIcon from '@material-ui/icons/DateRange';
import EventIcon from '@material-ui/icons/Event';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import KitchenIcon from '@material-ui/icons/Kitchen';
import ListIcon from '@material-ui/icons/List';
import NoteIcon from '@material-ui/icons/Note';
import PieChartIcon from '@material-ui/icons/PieChart';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import TrackChangesIcon from '@material-ui/icons/TrackChanges';
/* Styles */
import './LeftMobileMenu.css';

interface Props {
  isOpened: boolean;
  setIsOpened: Dispatch<SetStateAction<boolean>>;
}
const LeftMobileMenu: FC<Props> = (props: Props) => {
  const history = useHistory();
  const handleSelection = (address: string) => {
    history.push(address);
    props.setIsOpened(false);
  };

  return (
    <Drawer
      variant="temporary"
      anchor="left"
      open={props.isOpened}
      onClose={() => props.setIsOpened(false)}
      PaperProps={{ className: 'left-menu-paper' }}
      className="left-menu-container"
    >
      <Grid container spacing={0}>
        <Grid item xs={10}>
          <div className="appbar-app-name-link-container">
            <Typography
              component="div"
              className="appbar-app-name-container-mobile"
              data-testid="budget-logo"
            >
              <div className="appbar-app-icon-container">
                <AssignmentIndIcon className="appbar-app-icon" />
                <span className="appbar-app-name">Budget App</span>
              </div>
            </Typography>
          </div>
        </Grid>
        <Grid item xs={2}>
          <div className="close-icon-container appbar-app-name-link-container">
            <HighlightOffIcon className="close-icon" onClick={() => props.setIsOpened(!props.isOpened)} />
          </div>
        </Grid>
      </Grid>
      <List component="nav" className="main-left-menu-list-container">
        <ListItem
          button
          className="main-left-menu-list-item main-left-menu-list-link"
          onClick={() => handleSelection('/')}
        >
          <ListItemIcon className="main-left-menu-list-icon">
            <DashboardIcon />
          </ListItemIcon>
          <ListItemText primary="Dashboard" />
        </ListItem>
        <ListItem
          button
          className="main-left-menu-list-item main-left-menu-list-link"
          onClick={() => handleSelection('/list/')}
        >
          <ListItemIcon className="main-left-menu-list-icon">
            <ListIcon />
          </ListItemIcon>
          <ListItemText primary="List" />
        </ListItem>
        <ListItem
          button
          className="main-left-menu-list-item main-left-menu-list-link"
          onClick={() => handleSelection('/savings/')}
        >
          <ListItemIcon className="main-left-menu-list-icon">
            <AccountBalanceIcon />
          </ListItemIcon>
          <ListItemText primary="Savings" />
        </ListItem>
        <ListItem
          button
          className="main-left-menu-list-item main-left-menu-list-link"
          onClick={() => handleSelection('/investments/')}
        >
          <ListItemIcon className="main-left-menu-list-icon">
            <ShowChartIcon />
          </ListItemIcon>
          <ListItemText primary="Investments" />
        </ListItem>
        <ListItem
          button
          className="main-left-menu-list-item main-left-menu-list-link"
          onClick={() => handleSelection('/graphs/')}
        >
          <ListItemIcon className="main-left-menu-list-icon">
            <PieChartIcon />
          </ListItemIcon>
          <ListItemText primary="Graphs" />
        </ListItem>
        <ListItem
          button
          className="main-left-menu-list-item main-left-menu-list-link"
          onClick={() => handleSelection('/recipes/')}
        >
          <ListItemIcon className="main-left-menu-list-icon">
            <KitchenIcon />
          </ListItemIcon>
          <ListItemText primary="Recipes" />
        </ListItem>
        <ListItem
          button
          className="main-left-menu-list-item main-left-menu-list-link"
          onClick={() => handleSelection('/notes/')}
        >
          <ListItemIcon className="main-left-menu-list-icon">
            <NoteIcon />
          </ListItemIcon>
          <ListItemText primary="Notes" />
        </ListItem>
        <ListItem
          button
          className="main-left-menu-list-item main-left-menu-list-link"
          onClick={() => handleSelection('/calendar/')}
        >
          <ListItemIcon className="main-left-menu-list-icon">
            <DateRangeIcon />
          </ListItemIcon>
          <ListItemText primary="Calendar" />
        </ListItem>
        <ListItem button className="main-left-menu-list-item" disabled>
          <ListItemIcon className="main-left-menu-list-icon">
            <TrackChangesIcon />
          </ListItemIcon>
          <ListItemText primary="Targets" />
        </ListItem>
        <ListItem button className="main-left-menu-list-item" disabled>
          <ListItemIcon className="main-left-menu-list-icon">
            <EventIcon />
          </ListItemIcon>
          <ListItemText primary="Monthly Expenses" />
        </ListItem>
      </List>
    </Drawer>
  );
};

export default LeftMobileMenu;
