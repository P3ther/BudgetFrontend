import React, { FC } from 'react';
import { Card, CardContent, CircularProgress, Divider, Grid, Typography } from '@material-ui/core';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import { useSelector } from 'react-redux';
import { parseUnitAndCurrency } from '../../Common/CurrencyHelper';
import { BudgetInterface } from '../../Interfaces/Interfaces';
import { getBudget, getDefaultCurrency } from '../../Redux/Selectors';

interface Props {
  loadingData: boolean;
}

const InvestmentCard: FC<Props> = ({ loadingData }) => {
  const budget: BudgetInterface = useSelector(getBudget);
  const defaultCurrency = useSelector(getDefaultCurrency);

  return (
    <Card variant="outlined" className="card-container">
      <CardContent className="card-content">
        <Grid container className="card-grid">
          <Grid item xs={4} sm={6}>
            <div className="card-icon-container">
              <TrendingUpIcon className="card-icon" />
            </div>
          </Grid>
          <Grid item xs={4} sm={6} className="card-center-text">
            {loadingData ? (
              <CircularProgress />
            ) : (
              <>
                <Typography variant="subtitle1">Investment Value:</Typography>
                <p>{parseUnitAndCurrency(budget?.totalInvestmentsValue, defaultCurrency?.sign)}</p>
              </>
            )}
          </Grid>
        </Grid>
        <Divider className="card-divider" />
        {loadingData ? (
          <div />
        ) : (
          <>
            <div style={{ marginTop: '12.5px' }}>
              {`Investment Gains: ${parseUnitAndCurrency(
                budget?.totalInvestmentsGain,
                defaultCurrency?.sign,
              )}`}
            </div>
          </>
        )}
      </CardContent>
    </Card>
  );
};

export default InvestmentCard;
