import React, { FC } from 'react';
import { Button, Card, CardContent, Divider, Grid, Typography, CircularProgress } from '@material-ui/core';
import AccountBalanceWalletOutlinedIcon from '@material-ui/icons/AccountBalanceWalletOutlined';
import { useSelector, useDispatch } from 'react-redux';
import { BudgetInterface } from '../../Interfaces/Interfaces';
import Currency from '../../Interfaces/Currency.interface';
import { parseUnitAndCurrency } from '../../Common/CurrencyHelper';
import { getBudget } from '../../Redux/Selectors';
import { changeAddExpenseDialog, changeAddIncomeDialog } from '../../Redux/App/app.action';
import './BudgetCard.scss';

interface Props {
  defaultCurrency: Currency;
  loadingData: boolean;
}

const BudgetCard: FC<Props> = ({ defaultCurrency, loadingData }) => {
  const dispatch = useDispatch();
  const budget: BudgetInterface = useSelector(getBudget);

  const openAddExpense = () => {
    dispatch(changeAddExpenseDialog());
  };

  const openAddIncome = () => {
    dispatch(changeAddIncomeDialog());
  };

  return (
    <Card variant="outlined" className="card-container">
      <CardContent className="card-content">
        <Grid container className="card-grid">
          <Grid item xs={4} sm={6}>
            <div className="card-icon-container">
              <AccountBalanceWalletOutlinedIcon className="card-icon" />
            </div>
          </Grid>

          <Grid item xs={8} sm={6} className="card-center-text">
            {loadingData ? (
              <CircularProgress />
            ) : (
              <>
                <Typography variant="subtitle1">Amount Saved:</Typography>

                <p>{parseUnitAndCurrency(budget?.totalBalance, defaultCurrency?.sign)}</p>
              </>
            )}
          </Grid>
        </Grid>
        <Divider className="card-divider" />
        <Button
          size="small"
          variant="outlined"
          onClick={openAddExpense}
          className="card-button-margin budget-card-add-button card-button"
        >
          Add Expense
        </Button>
        <Button
          size="small"
          variant="outlined"
          onClick={openAddIncome}
          className="budget-card-add-button card-button"
        >
          Add Income
        </Button>
      </CardContent>
    </Card>
  );
};

export default BudgetCard;
