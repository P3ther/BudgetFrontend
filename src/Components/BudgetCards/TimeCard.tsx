import React, { Dispatch, SetStateAction } from 'react';
import { Button, Card, CardContent, Divider, Grid, Typography } from '@material-ui/core';
import EventOutlinedIcon from '@material-ui/icons/EventOutlined';
import Moment from 'react-moment';
import { useSelector } from 'react-redux';
import { getDateTimeRange } from '../../Redux/Selectors';
import './TimeCard.scss';

interface Props {
  setDialogOpen: Dispatch<SetStateAction<boolean>>;
}

export default function TimeCard({ setDialogOpen }: Props) {
  const dateRange = useSelector(getDateTimeRange);

  return (
    <Card variant="outlined" className="card-container">
      <CardContent className="card-content">
        <Grid container className="card-grid">
          <Grid item xs={4}>
            <div className="card-icon-container">
              <EventOutlinedIcon className="card-icon" />
            </div>
          </Grid>
          <Grid item xs={8} className="card-center-text">
            <Typography variant="subtitle1">Date:</Typography>
            <div className="time-card-date-time ">
              <Moment format="DD.MM.YYYY HH:mm" utc local>
                {dateRange?.startDateTime}
              </Moment>
            </div>
            <div>To:</div>
            <div className="time-card-date-time ">
              <Moment format="DD.MM.YYYY HH:mm" utc local>
                {dateRange?.endDateTime}
              </Moment>
            </div>
          </Grid>
        </Grid>
        <Divider className="card-divider" />
        <Button size="small" variant="outlined" onClick={() => setDialogOpen(true)} className="card-button">
          Change Time Period
        </Button>
      </CardContent>
    </Card>
  );
}
