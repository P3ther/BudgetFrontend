import React, { FC, Fragment, useEffect, useState } from 'react';
import { Card, CardContent, Divider, Grid } from '@material-ui/core';
import { useSelector } from 'react-redux';
import ExposureOutlinedIcon from '@material-ui/icons/ExposureOutlined';
import HorizontalBarGraph from '../../Common/HorizontalBarGraph';
import Currency from '../../Interfaces/Currency.interface';
import { parseUnitAndCurrency } from '../../Common/CurrencyHelper';
import { BudgetInterface } from '../../Interfaces/Interfaces';
import { getBudget } from '../../Redux/Selectors';
import './BalanceCard.scss';

interface Props {
  defaultCurrency: Currency;
  loadingData: boolean;
}

const BalanceCard: FC<Props> = ({ defaultCurrency, loadingData }) => {
  const [percentageExpense, setPercentageExpense] = useState<number>(0);
  const [percentageIncome, setPercentageIncome] = useState<number>(0);
  const budget: BudgetInterface = useSelector(getBudget);

  useEffect(() => {
    if (budget) {
      setPercentageExpense((budget.totalExpense / budget.expectedExpense) * 100);
      setPercentageIncome((budget.totalIncome / budget.expectedIncome) * 100);
    }
  }, [budget]);

  return (
    <Card variant="outlined" className="card-container">
      <CardContent className="card-content">
        <Grid container className="card-grid">
          <Grid item xs={4} sm={6}>
            <div className="card-icon-container">
              <ExposureOutlinedIcon className="card-icon" />
            </div>
          </Grid>
          <Grid item xs={4} sm={6} className="card-center-text">
            <HorizontalBarGraph percentage={percentageIncome} barColor="#6ea672" color="#94a395" />
            <div className="balance-card-graph-divider">
              <HorizontalBarGraph percentage={percentageExpense} barColor="#f9c74f" color="#f0e7d8" />
            </div>
          </Grid>
        </Grid>
        <Divider className="card-divider" />
        {loadingData ? (
          <></>
        ) : (
          <>
            <div>
              {`Income: ${parseUnitAndCurrency(
                budget?.totalIncome,
                defaultCurrency?.sign,
              )} / ${parseUnitAndCurrency(budget?.expectedIncome, defaultCurrency?.sign)}`}
            </div>
            <div>
              {`Expense: ${parseUnitAndCurrency(
                budget?.totalExpense,
                defaultCurrency?.sign,
              )} / ${parseUnitAndCurrency(budget?.expectedExpense, defaultCurrency?.sign)}`}
            </div>
          </>
        )}
      </CardContent>
    </Card>
  );
};

export default BalanceCard;
