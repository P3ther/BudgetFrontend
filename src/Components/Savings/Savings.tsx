import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import {
  Hint,
  HorizontalGridLines,
  VerticalBarSeries,
  VerticalBarSeriesPoint,
  VerticalGridLines,
  XAxis,
  XYPlot,
  YAxis,
} from 'react-vis';
import { savingsApi } from '../../Api/SavingsApi';
import { SavingsInterface } from '../../Interfaces/Interfaces';
import DateTimeRange from '../../Interfaces/StartEndTime.interface';
import LoadingOverlay from '../LoadingOverlay/LoadingOverlay';

interface Props { }

const Savings: FC<Props> = (props: Props) => {
  const [isLoading, setIsloading] = useState<boolean>(true);
  const [dateRange, setDateRange] = useState<DateTimeRange>({
    startDateTime: new Date(moment().startOf('year').toString()),
    endDateTime: new Date(),
  } as DateTimeRange);
  const [expectedSavingsGraphValues, setExpectedSavingsGraphValues] = useState<GraphValue[]>([]);
  const [actualSavingsGraphValues, setActualSavingsGraphValues] = useState<GraphValue[]>([]);
  const [yValue, setYValue] = useState<number>(0);
  const [mousePosition, setMousePosition] = useState<any>(null);

  useEffect(() => {
    const fetchData = async () => {
      const tempExpectedGraphValue: GraphValue[] = [];
      const tempActualGraphValue: GraphValue[] = [];
      const tempValues: number[] = [];

      await savingsApi.readSavingsForRange(dateRange).then(async (res) => {
        res.forEach((value: SavingsInterface) => {
          tempValues.push(value.value, value.savedValue);
          tempExpectedGraphValue.push({ x: moment(value.date).unix(), y: value.value });
          tempActualGraphValue.push({ x: moment(value.date).unix(), y: value.savedValue });
        });
        setYValue(Math.max(...tempValues) + Math.max(...tempValues) / 10);
        setExpectedSavingsGraphValues(tempExpectedGraphValue);
        setActualSavingsGraphValues(tempActualGraphValue);
      });
    };

    fetchData();
    setIsloading(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const hoverGraph = (evt: VerticalBarSeriesPoint | null) => {
    setMousePosition(evt);
    setDateRange(dateRange);
  };

  return isLoading ? (
    <LoadingOverlay isOpened={isLoading} />
  ) : (
    <div className="budget-main-container list-notes-container" id="graph-container">
      <XYPlot
        height={document.getElementById('graph-container')?.offsetHeight! - 50}
        width={document.getElementById('graph-container')?.offsetWidth! - 50}
        xDomain={[
          moment(dateRange.startDateTime).subtract('months', 1).unix(),
          moment(dateRange.endDateTime).add('months', 1).unix(),
        ]}
        yDomain={[0, yValue]}
      >
        <VerticalGridLines />
        <HorizontalGridLines />
        <XAxis
          tickLabelAngle={-30}
          tickFormat={(v) => `${moment.unix(v).format('MM/YYYY')}`}
          marginLeft={50}
        />
        <YAxis orientation="left" width={50} />
        <VerticalBarSeries
          barWidth={0.4}
          color="#000"
          data={expectedSavingsGraphValues}
          opacity={1}
          fill="#4e855f"
          onValueMouseOver={(evt, evt2) => hoverGraph(evt)}
          onValueMouseOut={() => hoverGraph(null)}
        />
        <VerticalBarSeries
          barWidth={0.4}
          stroke="#000"
          data={actualSavingsGraphValues}
          opacity={1}
          fill="#70a781"
          onValueMouseOver={(evt) => hoverGraph(evt)}
          onValueMouseOut={() => hoverGraph(null)}
        />
        {mousePosition && (
          <Hint
            value={{
              value: [mousePosition.y],
            }}
          />
        )}
      </XYPlot>
    </div>
  );
};

interface GraphValue {
  x: number;
  y: number;
}

export default Savings;
