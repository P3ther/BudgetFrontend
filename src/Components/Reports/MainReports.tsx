import React from 'react';
import { Card, CardContent, Container } from '@material-ui/core';
import './MainReports.scss';

interface Props { }

export default function MainReports(props: Props) {
  return (
    <>
      <Container>
        <Card variant="outlined">
          <CardContent>
            <div className="budget-header-h6">Test</div>
            <div className="main-report-content-container">
              <ReportCell />
            </div>
          </CardContent>
        </Card>
      </Container>
    </>
  );
}

const ReportCell = () => (
  <>
    <div className="main-report-cell-container">Test Cell</div>
  </>
);
