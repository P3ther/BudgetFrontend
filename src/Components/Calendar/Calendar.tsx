import { Fragment, useEffect, useState } from 'react';
import { Typography, ListItemText, ListItem, Grid } from '@material-ui/core';
import moment, { Moment } from 'moment';
import './Calendar.scss';

interface Props { }

export default function Calendar(props: Props) {
  const [displayedDates, setDisplayedDates] = useState<Moment[]>([]);

  useEffect(() => {
    const displayedDates: Moment[] = [];

    for (let i: number = 0; i <= 6; i++) {
      const startOfTheWeek: Moment = moment().startOf('week').add('1', 'days');
      const newDate = startOfTheWeek.add(i, 'days');

      displayedDates.push(newDate);
    }

    setDisplayedDates(displayedDates);
  }, []);

  return (
    <>
      <Typography variant="h4" className="list-header budget-header">
        Calendar
      </Typography>
      {displayedDates.map((date: Moment) => (
        <ListItem className="calendar-date-list-item">
          <Grid container>
            <Grid item xs={1}>
              <Typography
                variant="h6"
                className={moment().isSame(date, 'day') ? 'calendar-date-today' : 'calendar-date-not-today'}
              >
                {date.format('ddd')}
              </Typography>
              <Typography
                variant="h2"
                className={moment().isSame(date, 'day') ? 'calendar-date-today' : 'calendar-date-not-today'}
              >
                {date.format('DD')}
              </Typography>
            </Grid>
            <Grid item xs={11}>
              <ListItemText primary={moment().isSame(date, 'day') ? 'Today' : 'Not Today'} />
            </Grid>
          </Grid>
        </ListItem>
      ))}
    </>
  );
}
