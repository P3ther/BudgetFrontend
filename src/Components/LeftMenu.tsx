import { Grid, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { Link } from 'react-router-dom';
/* Icons */
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import DashboardIcon from '@material-ui/icons/Dashboard';
import DateRangeIcon from '@material-ui/icons/DateRange';
import EventIcon from '@material-ui/icons/Event';
import KitchenIcon from '@material-ui/icons/Kitchen';
import ListIcon from '@material-ui/icons/List';
import NoteIcon from '@material-ui/icons/Note';
import PieChartIcon from '@material-ui/icons/PieChart';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import TrackChangesIcon from '@material-ui/icons/TrackChanges';
/* Styles */
import './Styles/LeftMenu.scss';

export default function LeftMenu() {
  return (
    <Grid item xs={2} className="main-left-menu-class" id="left-menu-container">
      <List component="nav" className="main-left-menu-list-container">
        <Link to="/" className="main-left-menu-list-link">
          <ListItem button className="main-left-menu-list-item">
            <ListItemIcon className="main-left-menu-list-icon">
              <DashboardIcon />
            </ListItemIcon>
            <ListItemText primary="Dashboard" className="main-left-menu-list-item-text" />
          </ListItem>
        </Link>
        <Link to="/list/" className="main-left-menu-list-link">
          <ListItem button className="main-left-menu-list-item">
            <ListItemIcon className="main-left-menu-list-icon">
              <ListIcon />
            </ListItemIcon>
            <ListItemText primary="List" className="main-left-menu-list-item-text" />
          </ListItem>
        </Link>
        <Link to="/savings/" className="main-left-menu-list-link">
          <ListItem button className="main-left-menu-list-item">
            <ListItemIcon className="main-left-menu-list-icon">
              <AccountBalanceIcon />
            </ListItemIcon>
            <ListItemText primary="Savings" className="main-left-menu-list-item-text" />
          </ListItem>
        </Link>
        <Link to="/investments/" className="main-left-menu-list-link">
          <ListItem button className="main-left-menu-list-item">
            <ListItemIcon className="main-left-menu-list-icon">
              <ShowChartIcon />
            </ListItemIcon>
            <ListItemText primary="Investments" className="main-left-menu-list-item-text" />
          </ListItem>
        </Link>
        <Link to="/graphs/" className="main-left-menu-list-link">
          <ListItem button className="main-left-menu-list-item">
            <ListItemIcon className="main-left-menu-list-icon">
              <PieChartIcon />
            </ListItemIcon>
            <ListItemText primary="Graphs" className="main-left-menu-list-item-text" />
          </ListItem>
        </Link>
        <Link to="/recipes/" className="main-left-menu-list-link">
          <ListItem button className="main-left-menu-list-item">
            <ListItemIcon className="main-left-menu-list-icon">
              <KitchenIcon />
            </ListItemIcon>
            <ListItemText primary="Recipes" className="main-left-menu-list-item-text" />
          </ListItem>
        </Link>
        <Link to="/notes/" className="main-left-menu-list-link">
          <ListItem button className="main-left-menu-list-item">
            <ListItemIcon className="main-left-menu-list-icon">
              <NoteIcon />
            </ListItemIcon>
            <ListItemText primary="Notes" className="main-left-menu-list-item-text" />
          </ListItem>
        </Link>
        <Link to="/calendar/" className="main-left-menu-list-link">
          <ListItem button className="main-left-menu-list-item">
            <ListItemIcon className="main-left-menu-list-icon">
              <DateRangeIcon />
            </ListItemIcon>
            <ListItemText primary="Calendar" className="main-left-menu-list-item-text" />
          </ListItem>
        </Link>
        <ListItem button className="main-left-menu-list-item" disabled>
          <ListItemIcon className="main-left-menu-list-icon">
            <TrackChangesIcon />
          </ListItemIcon>
          <ListItemText primary="Targets" className="main-left-menu-list-item-text" />
        </ListItem>
        <ListItem button className="main-left-menu-list-item" disabled>
          <ListItemIcon className="main-left-menu-list-icon">
            <EventIcon />
          </ListItemIcon>
          <ListItemText primary="Monthly Expenses" className="main-left-menu-list-item-text" />
        </ListItem>
      </List>
    </Grid>
  );
}
