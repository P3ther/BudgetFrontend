import { ChangeEvent, Dispatch, SetStateAction } from 'react';
import { Ingredient } from '../../Interfaces/Food/Ingredient.interface';
import { RecipeIngredients } from '../../Interfaces/Food/RecipeIngredients.interface';

export const handleSelectRecipeIngredient = (
  selectedRecipeIngredients: RecipeIngredients[],
  setSelectedRecipeIngredients: Dispatch<SetStateAction<RecipeIngredients[]>>,
  allIngredients: Ingredient[],
  setAllIngredients: Dispatch<SetStateAction<Ingredient[]>>,
  id: number,
  key: number,
) => {
  const tempSelectedRecipeIngredients: RecipeIngredients[] = [...selectedRecipeIngredients];
  const tempAllIngredients: Ingredient[] = [...allIngredients];
  const foundIngredient: Ingredient | undefined = allIngredients.find(
    (ingredient: Ingredient) => ingredient?.id! === id,
  );

  if (foundIngredient !== undefined) {
    const newRecipeIngredient: RecipeIngredients = {
      ingredient: foundIngredient,
      quantity: 0,
    };

    tempSelectedRecipeIngredients.push(newRecipeIngredient);
    setSelectedRecipeIngredients(tempSelectedRecipeIngredients);
    tempAllIngredients.splice(key, 1);
    setAllIngredients(tempAllIngredients);
  }
};

export const handleUnselectRecipengredient = (
  selectedRecipeIngredients: RecipeIngredients[],
  setSelectedRecipeIngredients: Dispatch<SetStateAction<RecipeIngredients[]>>,
  allIngredients: Ingredient[],
  setAllIngredients: Dispatch<SetStateAction<Ingredient[]>>,
  id: number,
  key: number,
) => {
  const tempSelectedRecipeIngredients: RecipeIngredients[] = [...selectedRecipeIngredients];
  const tempAllIngredients: Ingredient[] = [...allIngredients];
  const foundIngredient: RecipeIngredients | undefined = selectedRecipeIngredients.find(
    (recipeIngredient: RecipeIngredients) => recipeIngredient.ingredient?.id! === id,
  );

  if (foundIngredient?.ingredient !== undefined) {
    tempSelectedRecipeIngredients.splice(key, 1);
    setSelectedRecipeIngredients(tempSelectedRecipeIngredients);
    tempAllIngredients.push(foundIngredient.ingredient);
    setAllIngredients(tempAllIngredients);
  }
};

export const handleChangeRecipeUnitValue = (
  selectedRecipeIngredients: RecipeIngredients[],
  setSelectedRecipeIngredients: Dispatch<SetStateAction<RecipeIngredients[]>>,
  evt: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  id?: number,
) => {
  if (id !== undefined) {
    const tempSelectedRecipeIngredients: RecipeIngredients[] = [...selectedRecipeIngredients];

    tempSelectedRecipeIngredients.forEach((recipeIngredient: RecipeIngredients) => {
      if (recipeIngredient.ingredient.id === id) {
        recipeIngredient.quantity = parseInt(evt.target.value);
      }
    });
    setSelectedRecipeIngredients(tempSelectedRecipeIngredients);
  }
};
