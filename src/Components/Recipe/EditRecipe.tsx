import React, { useEffect, useState } from 'react';
import {
  Button,
  CircularProgress,
  Grid,
  InputLabel,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  TextField,
  Typography,
} from '@material-ui/core';
import { Controller, useForm } from 'react-hook-form';
import AddIcon from '@material-ui/icons/Add';
import { useHistory } from 'react-router-dom';
import { EditorState, convertToRaw, convertFromRaw } from 'draft-js';
import DeleteIcon from '@material-ui/icons/Delete';
import { ingredientApi } from '../../Api/ingredientApi';
import CreateIngredientDialog from '../Ingredient/CreateIngredientDialog';
import { Recipe, RecipeBE } from '../../Interfaces/Food/Recipe.interface';
import { recipeApi } from '../../Api/RecipeApi';
import { RecipeIngredients } from '../../Interfaces/Food/RecipeIngredients.interface';

import './Styles/CreateRecipe.scss';
import 'draft-js/dist/Draft.css';
import RecipeInstructionsEditor from './RecipeInstructionsEditor';
import parseUnits from '../../Common/String/parseUnits';
import {
  handleChangeRecipeUnitValue,
  handleSelectRecipeIngredient,
  handleUnselectRecipengredient,
} from './RecipeHelper';
import { Ingredient } from '../../Interfaces/Food/Ingredient.interface';

interface Props {
  selectedRecipe: number;
}

export default function EditRecipe(props: Props) {
  const history = useHistory();
  const [addIngredient, setAddIngredient] = useState<boolean>(false);
  const [allIngredients, setAllIngredients] = useState<Ingredient[]>([]);
  const [selectedRecipeIngredients, setSelectedRecipeIngredients] = useState<RecipeIngredients[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [editedRecipe, setEditedRecipe] = useState<Recipe>({} as Recipe);
  const { handleSubmit, control, errors } = useForm<Recipe>({
    mode: 'onChange',
    // resolver: yupResolver(ExpenseValidation),
  });

  useEffect(() => {
    loadData();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const loadData = async () => {
    let tempSelectedIngredients: RecipeIngredients[] = [];

    await recipeApi
      .getRecipe(props.selectedRecipe)
      .then((res) => {
        const updatedResults: Recipe = {
          id: res.id,
          recipeName: res.recipeName,
          recipeInstructions: EditorState.createWithContent(
            convertFromRaw(JSON.parse(res.recipeInstructions)),
          ),
          recipeIngredients: res.recipeIngredients,
        };

        tempSelectedIngredients = res.recipeIngredients!;
        setEditedRecipe(updatedResults);
        setSelectedRecipeIngredients(res.recipeIngredients!);
      })
      .catch((err) => console.warn(err));
    await ingredientApi
      .listIngredient()
      .then((res) => {
        // eslint-disable-next-line no-param-reassign
        res = res.filter(
          (ingredient: Ingredient, key: number) =>
            !tempSelectedIngredients.find(
              (selectedIngredient: RecipeIngredients) => ingredient.id === selectedIngredient.ingredient.id,
            ),
        );
        setAllIngredients(res);
      })
      .catch((err) => console.warn(err));
    setIsLoading(false);
  };

  const onSubmit = async (data: Recipe) => {
    const editedRecipeSubmit: RecipeBE = {
      id: editedRecipe.id,
      recipeName: data.recipeName,
      recipeInstructions: JSON.stringify(convertToRaw(data.recipeInstructions.getCurrentContent())),
      recipeIngredients: selectedRecipeIngredients,
    };

    await recipeApi
      .updateRecipe(editedRecipeSubmit)
      .then((res) => {
        history.push(`/recipes/${res.id}/`);
      })
      .catch((err) => console.warn(err));
  };

  if (isLoading) {
    return (
      <div style={{ position: 'relative', overflow: 'visible' }}>
        <div className="create-recipe-container-loading-overlay">
          <CircularProgress className="create-recipe-container-loading-overlay-circular-progress" />
        </div>
      </div>
    );
  }

  return (
    <div style={{ position: 'relative', overflow: 'visible' }} className="budget-main-container">
      <div className="budget-header">Edit Recipe</div>
      <Grid container spacing={4}>
        <Grid item xs={12} md={6}>
          <InputLabel error={!!errors.recipeName}>Recipe Name:</InputLabel>
          <Controller
            as={<TextField fullWidth size="small" />}
            name="recipeName"
            variant="outlined"
            control={control}
            error={errors.recipeName}
            helperText={errors.recipeName ? errors.recipeName.message : ''}
            defaultValue={editedRecipe.recipeName}
          />
          <InputLabel error={!!errors.recipeName}>Recipe Instructions:</InputLabel>
          <Controller
            name="recipeInstructions"
            control={control}
            error={errors.recipeName}
            defaultValue={EditorState.createWithContent(editedRecipe.recipeInstructions.getCurrentContent())}
            render={({ value, onChange }) => (
              <RecipeInstructionsEditor editorState={value} setEditorState={onChange} />
            )}
          />
          <div className="create-recipe-button-container">
            <Button className="budget-error-button-text" onClick={() => history.goBack()}>
              Cancel
            </Button>
            <Button variant="outlined" className="budget-confirm-button" onClick={handleSubmit(onSubmit)}>
              Submit
            </Button>
          </div>
        </Grid>
        <Grid item xs={12} md={6}>
          <Paper className="create-recipe-selected-ingredient-container">
            <Typography className="budget-header-h6">Selected Ingredients:</Typography>
            <List dense component="div" role="list">
              {selectedRecipeIngredients.map((selectedIngredient: RecipeIngredients, key: number) => (
                <ListItem role="listitem" button>
                  <ListItemIcon>
                    <TextField
                      defaultValue={selectedIngredient.quantity}
                      size="small"
                      style={{ width: '60px', marginRight: '1vw' }}
                      type="number"
                      onChange={(evt) =>
                        handleChangeRecipeUnitValue(
                          selectedRecipeIngredients,
                          setSelectedRecipeIngredients,
                          evt,
                          selectedIngredient.ingredient.id,
                        )
                      }
                    />
                  </ListItemIcon>
                  <ListItemText
                    primary={`${parseUnits(selectedIngredient.ingredient.ingredientUnit)} 
                    ${selectedIngredient.ingredient.ingredientName}`}
                  />
                  <DeleteIcon
                    onClick={() =>
                      handleUnselectRecipengredient(
                        selectedRecipeIngredients,
                        setSelectedRecipeIngredients,
                        allIngredients,
                        setAllIngredients,
                        selectedIngredient.ingredient.id!,
                        key,
                      )
                    }
                  />
                </ListItem>
              ))}
            </List>
          </Paper>
          <Paper className="create-recipe-all-ingredient-container">
            <Typography className="budget-header-h6">All Available Ingredients:</Typography>
            <List dense component="div" role="list">
              <ListItem role="listitem" button onClick={() => setAddIngredient(true)}>
                <ListItemIcon>
                  <AddIcon />
                </ListItemIcon>
                <ListItemText primary="Add new Ingredient" />
              </ListItem>
              {allIngredients.map((ingredient: Ingredient, key: number) => (
                <ListItem
                  role="listitem"
                  button
                  onClick={() =>
                    handleSelectRecipeIngredient(
                      selectedRecipeIngredients,
                      setSelectedRecipeIngredients,
                      allIngredients,
                      setAllIngredients,
                      ingredient.id!,
                      key,
                    )
                  }
                >
                  <ListItemIcon />
                  <ListItemText primary={ingredient.ingredientName} />
                </ListItem>
              ))}
            </List>
          </Paper>
        </Grid>
      </Grid>
      <CreateIngredientDialog
        isOpened={addIngredient}
        setIsOpened={setAddIngredient}
        setAllIngredients={setAllIngredients}
        selectedRecipeIngredients={selectedRecipeIngredients}
      />
    </div>
  );
}
