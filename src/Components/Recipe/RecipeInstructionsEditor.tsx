import React, { Dispatch, SetStateAction } from 'react';
import { IconButton } from '@material-ui/core';
import { Editor, EditorState, RichUtils } from 'draft-js';

import './Styles/CreateRecipe.scss';
import 'draft-js/dist/Draft.css';

import FormatBoldIcon from '@material-ui/icons/FormatBold';
import FormatUnderlinedIcon from '@material-ui/icons/FormatUnderlined';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import FormatListNumberedIcon from '@material-ui/icons/FormatListNumbered';
import FormatItalicIcon from '@material-ui/icons/FormatItalic';
import UndoIcon from '@material-ui/icons/Undo';
import RedoIcon from '@material-ui/icons/Redo';

interface Props {
  editorState: EditorState;
  setEditorState: Dispatch<SetStateAction<EditorState>>;
}

export default function RecipeInstructionsEditor({ editorState, setEditorState }: Props) {
  const onChange = (editorStateNew: EditorState) => {
    setEditorState(editorStateNew);
  };

  const handleFontStyle = (style: string) => {
    onChange(RichUtils.toggleInlineStyle(editorState, style));
  };
  const createBlockType = (typeOfBlock: string) => {
    onChange(RichUtils.toggleBlockType(editorState, typeOfBlock));
  };

  return (
    <div style={{ height: '500px', border: '1px solid #bec0c0', borderRadius: '5px' }}>
      <div style={{ width: '100%', borderBottom: '1px solid #bec0c0' }}>
        <IconButton onClick={() => handleFontStyle('BOLD')}>
          <UndoIcon />
        </IconButton>
        <IconButton onClick={() => handleFontStyle('BOLD')}>
          <RedoIcon />
        </IconButton>
        <span
          style={{
            width: '15px',
            height: '36px',
            borderRight: '1px solid #bec0c0',
          }}
        >
          {' '}
        </span>
        <IconButton onClick={() => handleFontStyle('BOLD')}>
          <FormatBoldIcon />
        </IconButton>
        <IconButton onClick={() => handleFontStyle('UNDERLINE')}>
          <FormatUnderlinedIcon />
        </IconButton>
        <IconButton onClick={() => handleFontStyle('ITALIC')}>
          <FormatItalicIcon />
        </IconButton>

        <IconButton onClick={() => createBlockType('unordered-list-item')}>
          <FormatListBulletedIcon />
        </IconButton>
        <IconButton onClick={() => createBlockType('ordered-list-item')}>
          <FormatListNumberedIcon />
        </IconButton>
      </div>
      <div style={{ width: '100%', height: '91%', padding: '10px', overflowY: 'auto' }}>
        <Editor editorState={editorState} onChange={setEditorState} />
      </div>
    </div>
  );
}
