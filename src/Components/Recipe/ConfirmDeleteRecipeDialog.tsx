import { Dispatch, SetStateAction } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@material-ui/core';
import { Recipe } from '../../Interfaces/Food/Recipe.interface';
import { recipeApi } from '../../Api/RecipeApi';

/* Styles */
import './Styles/ConfirmDeleteRecipeDialog.scss';

interface Props {
  isOpened: boolean;
  setIsOpened: Dispatch<SetStateAction<boolean>>;
  deletedRecipe: Recipe;
  loadDataListRecipes: () => void;
}

export default function ConfirmDeleteRecipeDialog(props: Props) {
  const handleClose = () => {
    props.setIsOpened(false);
  };

  const handleDelete = () => {
    recipeApi
      .deleteRecipe(props.deletedRecipe.id)
      .then(() => {
        props.loadDataListRecipes();
        props.setIsOpened(false);
      })
      .catch((err) => console.warn(err));
  };

  return (
    <Dialog open={props.isOpened} fullWidth maxWidth="xs">
      <DialogTitle className="confirm-delete-recipe-dialog-dialog-content-text">
        Confirm deletion of the recipe:
        <div>{props.deletedRecipe.recipeName}</div>
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          <div className="confirm-delete-recipe-dialog-dialog-content-text-div">
            Are you sure you want to delete this recipe?
          </div>
          <div className="confirm-delete-recipe-dialog-dialog-content-text-div">
            This operation cannot be undone
          </div>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button variant="outlined" onClick={handleClose} className="budget-confirm-button">
          No
        </Button>
        <Button variant="outlined" onClick={() => handleDelete()} className="budget-error-button">
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  );
}
