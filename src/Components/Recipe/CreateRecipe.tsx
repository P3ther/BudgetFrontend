import React, { ChangeEvent, useEffect, useState } from 'react';
import {
  Button,
  CircularProgress,
  Grid,
  InputLabel,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  TextField,
  Typography,
} from '@material-ui/core';
import { Controller, useForm } from 'react-hook-form';
import AddIcon from '@material-ui/icons/Add';
import { useHistory } from 'react-router-dom';
import { EditorState, convertToRaw } from 'draft-js';
import DeleteIcon from '@material-ui/icons/Delete';
import { ingredientApi } from '../../Api/ingredientApi';
import CreateIngredientDialog from '../Ingredient/CreateIngredientDialog';
import { NewRecipe, NewRecipeBE, Recipe } from '../../Interfaces/Food/Recipe.interface';
import { recipeApi } from '../../Api/RecipeApi';
import { RecipeIngredients } from '../../Interfaces/Food/RecipeIngredients.interface';

import './Styles/CreateRecipe.scss';
import 'draft-js/dist/Draft.css';
import RecipeInstructionsEditor from './RecipeInstructionsEditor';
import parseUnits from '../../Common/String/parseUnits';
import { Ingredient } from '../../Interfaces/Food/Ingredient.interface';

export default function CreateRecipe() {
  const history = useHistory();
  const [addIngredient, setAddIngredient] = useState<boolean>(false);
  const [allIngredients, setAllIngredients] = useState<Ingredient[]>([]);
  const [selectedRecipeIngredients, setSelectedRecipeIngredients] = useState<RecipeIngredients[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const { handleSubmit, control, errors } = useForm<Recipe>({
    mode: 'onChange',
    // resolver: yupResolver(ExpenseValidation),
  });

  useEffect(() => {
    ingredientApi
      .listIngredient()
      .then((res) => {
        // eslint-disable-next-line no-param-reassign
        res = res.filter(
          (ingredient: Ingredient, key: number) =>
            !selectedRecipeIngredients.find(
              (selectedIngredient: RecipeIngredients) => ingredient.id === selectedIngredient.ingredient.id,
            ),
        );
        setAllIngredients(res);
        setIsLoading(false);
      })
      .catch((err) => console.warn(err));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const onSubmit = async (data: NewRecipe) => {
    const newRecipe: NewRecipeBE = {
      recipeName: data.recipeName,
      recipeInstructions: JSON.stringify(convertToRaw(data.recipeInstructions.getCurrentContent())),
      recipeIngredients: selectedRecipeIngredients,
    };

    await recipeApi
      .createRecipe(newRecipe)
      .then((res) => {
        history.push(`/recipes/${res.id}/`);
      })
      .catch((err) => console.warn(err));
  };

  const handleSelectIngredient = (id: number, key: number) => {
    const tempSelectedRecipeIngredients: RecipeIngredients[] = [...selectedRecipeIngredients];
    const tempAllIngredients: Ingredient[] = [...allIngredients];
    const foundIngredient: Ingredient | undefined = allIngredients.find(
      (ingredient: Ingredient) => ingredient?.id! === id,
    );

    if (foundIngredient !== undefined) {
      const newRecipeIngredient: RecipeIngredients = {
        ingredient: foundIngredient,
        quantity: 0,
      };

      tempSelectedRecipeIngredients.push(newRecipeIngredient);
      setSelectedRecipeIngredients(tempSelectedRecipeIngredients);
      tempAllIngredients.splice(key, 1);
      setAllIngredients(tempAllIngredients);
    }
  };

  const handleUnselectIngredient = (id: number, key: number) => {
    const tempSelectedRecipeIngredients: RecipeIngredients[] = [...selectedRecipeIngredients];
    const tempAllIngredients: Ingredient[] = [...allIngredients];
    const foundIngredient: RecipeIngredients | undefined = selectedRecipeIngredients.find(
      (recipeIngredient: RecipeIngredients) => recipeIngredient.ingredient?.id! === id,
    );

    if (foundIngredient?.ingredient !== undefined) {
      tempSelectedRecipeIngredients.splice(key, 1);
      setSelectedRecipeIngredients(tempSelectedRecipeIngredients);
      tempAllIngredients.push(foundIngredient.ingredient);
      setAllIngredients(tempAllIngredients);
    }
  };
  const handleChangeUnitValue = (evt: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, id?: number) => {
    if (id !== undefined) {
      selectedRecipeIngredients.forEach((recipeIngredient: RecipeIngredients) => {
        if (recipeIngredient.ingredient.id === id) {
          recipeIngredient.quantity = parseInt(evt.target.value);
        }
      });
    }
  };

  return (
    <div style={{ position: 'relative', overflow: 'visible' }} className="budget-main-container">
      <div className="budget-header">Create a new Recipe</div>
      <Grid container spacing={4}>
        <Grid item xs={12} md={6}>
          <InputLabel error={!!errors.recipeName}>Recipe Name:</InputLabel>
          <Controller
            as={<TextField fullWidth size="small" />}
            name="recipeName"
            variant="outlined"
            control={control}
            error={errors.recipeName}
            helperText={errors.recipeName ? errors.recipeName.message : ''}
          />
          <InputLabel error={!!errors.recipeName}>Recipe Instructions:</InputLabel>
          <Controller
            name="recipeInstructions"
            control={control}
            error={errors.recipeName}
            defaultValue={EditorState.createEmpty()}
            render={({ value, onChange }) => (
              <RecipeInstructionsEditor editorState={value} setEditorState={onChange} />
            )}
          />
          <div className="create-recipe-button-container">
            <Button className="budget-error-button-text" onClick={() => history.goBack()}>
              Cancel
            </Button>
            <Button variant="outlined" className="budget-confirm-button" onClick={handleSubmit(onSubmit)}>
              Submit
            </Button>
          </div>
        </Grid>
        <Grid item xs={12} md={6}>
          <Paper className="create-recipe-selected-ingredient-container">
            <Typography className="budget-header-h6">Selected Ingredients:</Typography>
            <List dense component="div" role="list">
              {selectedRecipeIngredients.map((selectedIngredient: RecipeIngredients, key: number) => (
                <ListItem role="listitem" button>
                  <ListItemIcon>
                    <TextField
                      size="small"
                      style={{ width: '60px', marginRight: '1vw' }}
                      type="number"
                      onChange={(evt) => handleChangeUnitValue(evt, selectedIngredient.ingredient.id)}
                    />
                  </ListItemIcon>
                  <ListItemText
                    primary={`
                    ${parseUnits(selectedIngredient.ingredient.ingredientUnit)} 
                    ${selectedIngredient.ingredient.ingredientName}
                    `}
                  />
                  <DeleteIcon
                    onClick={() => handleUnselectIngredient(selectedIngredient.ingredient.id!, key)}
                  />
                </ListItem>
              ))}
            </List>
          </Paper>
          <Paper className="create-recipe-all-ingredient-container">
            <Typography className="budget-header-h6">All Available Ingredients:</Typography>
            <List dense component="div" role="list">
              <ListItem role="listitem" button onClick={() => setAddIngredient(true)}>
                <ListItemIcon>
                  <AddIcon />
                </ListItemIcon>
                <ListItemText primary="Add new Ingredient" />
              </ListItem>
              {allIngredients.map((ingredient: Ingredient, key: number) => (
                <ListItem role="listitem" button onClick={() => handleSelectIngredient(ingredient.id!, key)}>
                  <ListItemIcon />
                  <ListItemText primary={ingredient.ingredientName} />
                </ListItem>
              ))}
            </List>
          </Paper>
        </Grid>
      </Grid>
      <CreateIngredientDialog
        isOpened={addIngredient}
        setIsOpened={setAddIngredient}
        setAllIngredients={setAllIngredients}
        selectedRecipeIngredients={selectedRecipeIngredients}
      />
      {isLoading && (
        <div className="create-recipe-container-loading-overlay">
          <CircularProgress className="create-recipe-container-loading-overlay-circular-progress" />
        </div>
      )}
    </div>
  );
}
