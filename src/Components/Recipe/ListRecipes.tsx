import { Fragment, useEffect, useState } from 'react';
import { Button, Collapse, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { EditorState, convertFromRaw } from 'draft-js';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import { Link } from 'react-router-dom';
import DisplayRecipeInstructions from './DisplayRecipeInstructions';
import LoadingOverlay from '../LoadingOverlay/LoadingOverlay';
import { recipeApi } from '../../Api/RecipeApi';
/* Interfaces */
import { Recipe, RecipeBE } from '../../Interfaces/Food/Recipe.interface';
/* Styles */
import './Styles/ListRecipes.scss';

interface Props {
  selectedRecipe: number | undefined;
}

export default function ListRecipes({ selectedRecipe }: Props) {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [recipes, setRecipes] = useState<Recipe[]>([]);
  const [displayedRecipe, setDisplayedRecipe] = useState<number>(
    selectedRecipe !== undefined ? selectedRecipe : -1,
  );

  useEffect(() => {
    loadDataListRecipes();
  }, []);

  const loadDataListRecipes = () => {
    recipeApi
      .listRecipes()
      .then((res) => {
        const newResults: RecipeBE[] = res;
        const updatedResults: Recipe[] = [];

        newResults.forEach((result: RecipeBE) => {
          let updatedInstructions: EditorState = EditorState.createEmpty();

          try {
            updatedInstructions = EditorState.createWithContent(
              convertFromRaw(JSON.parse(result.recipeInstructions)),
            );
          } catch (err) {
            console.warn(err);
          } finally {
            const updatedResult: Recipe = {
              id: result.id,
              recipeName: result.recipeName,
              recipeInstructions: updatedInstructions,
              recipeIngredients: result.recipeIngredients,
            };

            updatedResults.push(updatedResult);
          }
        });
        setRecipes(updatedResults);
        setIsLoading(false);
      })
      .catch((err) => console.warn(err));
  };

  const handleChangeSelectedRecipe = (id?: number) => {
    if (id === undefined) {
      setDisplayedRecipe(-1);
    } else {
      setDisplayedRecipe(id);
    }
  };

  return (
    <div className="budget-main-container" style={{ position: 'relative' }}>
      <LoadingOverlay isOpened={isLoading} />
      {!isLoading && (
        <>
          <div className="list-recipes-header-container">
            <div className="budget-header list-recipes-header">List Of All Recipes</div>
            <div className="budget-header list-recipes-add-button-container">
              <Link to="/ingredients/" className="list-recipes-add-button-link">
                <Button
                  variant="outlined"
                  size="small"
                  className="budget-confirm-button list-recipes-manage-ingredients"
                >
                  Manage Ingredients
                </Button>
              </Link>
              <Link to="/createRecipe/" className="list-recipes-add-button-link">
                <Button
                  variant="outlined"
                  size="small"
                  className="budget-confirm-button list-recipes-create-new-recipe"
                >
                  Create New Recipe
                </Button>
              </Link>
            </div>
          </div>
          <List
            component="div"
            dense
            style={{
              width: '100%',
              display: 'block',
              height: '90%',
            }}
          >
            {recipes.map((recipe: Recipe) => (
              <>
                <ListItem
                  button
                  onClick={
                    recipe.id === Number.parseInt(displayedRecipe.toString())
                      ? () => handleChangeSelectedRecipe()
                      : () => handleChangeSelectedRecipe(recipe.id)
                  }
                >
                  <ListItemIcon />
                  <ListItemText
                    primary={
                      <div className="budget-header-h5 list-recipes-list-item-text">{recipe.recipeName}</div>
                    }
                  />
                  {recipe.id === displayedRecipe ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={recipe.id === Number.parseInt(displayedRecipe.toString())} unmountOnExit>
                  <DisplayRecipeInstructions recipe={recipe} loadDataListRecipes={loadDataListRecipes} />
                </Collapse>
              </>
            ))}
          </List>
        </>
      )}
    </div>
  );
}
