import { Fragment, useState } from 'react';
import { Editor } from 'draft-js';
import { Button } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { Recipe } from '../../Interfaces/Food/Recipe.interface';
import { RecipeIngredients } from '../../Interfaces/Food/RecipeIngredients.interface';
import parseUnits from '../../Common/String/parseUnits';
import ConfirmDeleteRecipeDialog from './ConfirmDeleteRecipeDialog';

/* Styles */
import './Styles/DisplayRecipeInstructions.scss';

interface Props {
  recipe: Recipe;
  loadDataListRecipes: () => void;
}

export default function DisplayRecipeInstructions(props: Props) {
  const [confirmDeleteRecipe, setConfirmDeleteRecipe] = useState<boolean>(false);

  return (
    <>
      <ConfirmDeleteRecipeDialog
        isOpened={confirmDeleteRecipe}
        setIsOpened={setConfirmDeleteRecipe}
        deletedRecipe={props.recipe}
        loadDataListRecipes={props.loadDataListRecipes}
      />
      <div className="display-recipe-instructions-container">
        <div style={{ width: '100%', textAlign: 'right' }}>
          <Link to={`/editRecipe/${props.recipe.id}/`} className="budget-button-link">
            <Button size="small" className="budget-warning-button-text">
              Edit
            </Button>
          </Link>
          <Button
            size="small"
            className="budget-error-button-text"
            onClick={() => setConfirmDeleteRecipe(true)}
          >
            Delete
          </Button>
        </div>
        <div className="budget-header-h6">Ingredients:</div>
        {props.recipe.recipeIngredients?.map((ingredient: RecipeIngredients, key: number) => (
          <div className="display-recipe-instructions-list-item-ingredient">
            {`
            ${ingredient.quantity} 
            ${parseUnits(ingredient.ingredient.ingredientUnit)} ${ingredient.ingredient.ingredientName}
            `}
          </div>
        ))}

        <div className="budget-header-h6">Instructions:</div>
        <Editor editorState={props.recipe.recipeInstructions} readOnly onChange={() => { }} />
        <div className="budget-header-h6">Total Recipe Calories:</div>
        <div className="display-recipe-instructions-list-item-ingredient">
          {props.recipe.recipeCalories === undefined
            ? 'No calories defined for this recipe'
            : `${props.recipe.recipeCalories}kCal`}
        </div>
      </div>
    </>
  );
}
