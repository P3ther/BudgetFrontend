import { Button, FormHelperText, Grid, InputLabel, TextField } from '@material-ui/core';
import moment from 'moment';
import { Dispatch, SetStateAction, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { taskApi } from '../../Api/TaskApi';
import { loadTasks } from '../../Common/DataHandling/LoadTasks';
import Task from '../../Interfaces/Task.interface';

import './Styles/CreateTask.scss';

interface Props {
  setAddNewTask: Dispatch<SetStateAction<boolean>>;
  setTasks: Dispatch<SetStateAction<Task[]>>;
}

export default function CreateTask({ setAddNewTask, setTasks }: Props) {
  const [selectedPriority, setSelectedPriority] = useState<number>(0);
  const {
    handleSubmit,
    control,
    errors,
    // formState: { isDirty, isValid },
  } = useForm<Task>({
    mode: 'onChange',
    // resolver: yupResolver(ExpenseValidation),
  });

  const handleClose = () => {
    setAddNewTask(false);
  };

  const onSubmit = async (data: Task) => {
    const newTask: Task = {
      taskName: data.taskName,
      taskDetails: data.taskDetails,
      dateCreated: new Date(),
      dateExpired: new Date(data.dateExpired),
      priority: selectedPriority,
    };

    await taskApi
      .createTask(newTask)
      .then(() => {
        loadTasks(setTasks).then(() => {
          setAddNewTask(false);
        });
      })
      .catch((err) => console.warn(err));
  };

  return (
    <div style={{ width: '100%', height: '240px' }}>
      <form>
        <Grid container spacing={2}>
          <Grid item xs={9}>
            <InputLabel error={!!errors.taskName}>Name of the Task</InputLabel>
            <Controller
              as={<TextField fullWidth size="small" />}
              name="taskName"
              variant="outlined"
              control={control}
              error={errors.taskName}
              helperText={errors.taskName ? errors.taskName.message : ''}
            />
            <InputLabel error={!!errors.taskName}>Details of the Task</InputLabel>
            <Controller
              as={<TextField fullWidth size="small" multiline rowsMax={3} rows={3} />}
              name="taskDetails"
              variant="outlined"
              control={control}
              error={errors.taskName}
              helperText={errors.taskName ? errors.taskName.message : ''}
            />
            <InputLabel style={{ marginTop: '10px' }} error={!!errors.dateExpired}>
              Expiration Date
            </InputLabel>
            <Controller
              as={<TextField fullWidth type="datetime-local" size="small" />}
              name="dateExpired"
              variant="outlined"
              control={control}
              defaultValue={moment(new Date()).format('YYYY-MM-DDTHH:mm')}
              error={errors.dateExpired}
            />
            {errors.dateExpired ? (
              <FormHelperText error style={{ marginLeft: '14px' }}>
                {errors.dateExpired.message}
              </FormHelperText>
            ) : (
              ''
            )}
            <div className="create-task-button-container">
              <Button
                variant="outlined"
                size="small"
                className="create-task-close-button budget-warning-button"
                onClick={handleClose}
              >
                Close
              </Button>
              <Button
                variant="outlined"
                size="small"
                className="budget-confirm-button"
                onClick={handleSubmit(onSubmit)}
              >
                Submit
              </Button>
            </div>
          </Grid>
          <Grid item xs={3}>
            <InputLabel error={!!errors.taskName}>Priority</InputLabel>
            <PrioritySelector selectedPriority={selectedPriority} setSelectedPriority={setSelectedPriority} />
          </Grid>
        </Grid>
      </form>
    </div>
  );
}

interface PrioritySelectorProps {
  selectedPriority: number;
  setSelectedPriority: Dispatch<SetStateAction<number>>;
}

function PrioritySelector({ selectedPriority, setSelectedPriority }: PrioritySelectorProps) {
  const priorityLevels: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  return (
    <div>
      {priorityLevels.map((priority: number, key: number) => (
        <div
          className={
            selectedPriority === priority
              ? 'create-task-priority-selector-item create-task-priority-selector-item-selected'
              : 'create-task-priority-selector-item'
          }
          onClick={() => setSelectedPriority(priority)}
        >
          {priority}
        </div>
      ))}
    </div>
  );
}
