import { Dispatch, SetStateAction, useEffect, useState } from 'react';
import { Button, FormHelperText, Grid, InputLabel, TextField } from '@material-ui/core';
import moment from 'moment';
import { Controller, useForm } from 'react-hook-form';
import { taskApi } from '../../Api/TaskApi';
import ConfirmDeleteTask from './ConfirmDeleteTask';
import Task from '../../Interfaces/Task.interface';

import './Styles/EditTask.scss';
import { loadTasks } from '../../Common/DataHandling/LoadTasks';

interface Props {
  setEditTask: Dispatch<SetStateAction<boolean>>;
  editedTask: Task;
  setTasks: Dispatch<SetStateAction<Task[]>>;
}

export default function EditTask({ setEditTask, editedTask, setTasks }: Props) {
  const [selectedPriority, setSelectedPriority] = useState<number>(0);
  const [confirmDelete, setConfirmDelete] = useState<boolean>(false);
  const {
    handleSubmit,
    control,
    errors,
    // formState: { isDirty, isValid },
  } = useForm<Task>({
    mode: 'onChange',
    // resolver: yupResolver(ExpenseValidation),
  });

  useEffect(() => {
    setSelectedPriority(editedTask.priority);
  }, [editedTask.priority]);

  const handleClose = () => {
    setEditTask(false);
  };

  const onSubmit = async (data: Task) => {
    const editedTaskSubmit: Task = {
      id: editedTask.id,
      taskName: data.taskName,
      taskDetails: data.taskDetails,
      dateCreated: new Date(editedTask.dateCreated),
      dateExpired: new Date(data.dateExpired),
      priority: selectedPriority,
    };

    await taskApi
      .updateTask(editedTaskSubmit)
      .then((res) => {
        loadTasks(setTasks).then(() => {
          setEditTask(false);
        });
      })
      .catch((err) => console.warn(err));
  };

  return (
    <div style={{ width: '100%', height: '240px', position: 'relative' }}>
      <form>
        <Grid container spacing={2}>
          <Grid item xs={9}>
            <InputLabel error={!!errors.taskName}>Name of the Task</InputLabel>
            <Controller
              as={<TextField fullWidth size="small" />}
              name="taskName"
              variant="outlined"
              control={control}
              error={errors.taskName}
              helperText={errors.taskName ? errors.taskName.message : ''}
              defaultValue={editedTask.taskName}
            />
            <InputLabel error={!!errors.taskName}>Details of the Task</InputLabel>
            <Controller
              as={<TextField fullWidth size="small" multiline rowsMax={3} rows={3} />}
              name="taskDetails"
              variant="outlined"
              control={control}
              error={errors.taskName}
              helperText={errors.taskName ? errors.taskName.message : ''}
              defaultValue={editedTask.taskDetails}
            />
            <InputLabel style={{ marginTop: '10px' }} error={!!errors.dateExpired}>
              Expiration Date
            </InputLabel>
            <Controller
              as={<TextField fullWidth type="datetime-local" size="small" />}
              name="dateExpired"
              variant="outlined"
              control={control}
              defaultValue={moment(editedTask.dateExpired).utc(true).local().format('YYYY-MM-DDTHH:mm')}
              error={errors.dateExpired}
            />
            {errors.dateExpired ? (
              <FormHelperText error style={{ marginLeft: '14px' }}>
                {errors.dateExpired.message}
              </FormHelperText>
            ) : (
              ''
            )}
            <div className="create-task-button-container">
              <Button
                variant="outlined"
                size="small"
                className="create-task-close-button budget-error-button"
                onClick={() => setConfirmDelete(true)}
              >
                Delete
              </Button>
              <Button
                variant="outlined"
                size="small"
                className="create-task-close-button budget-warning-button"
                onClick={handleClose}
              >
                Close
              </Button>
              <Button
                variant="outlined"
                size="small"
                className="budget-confirm-button"
                onClick={handleSubmit(onSubmit)}
              >
                Submit
              </Button>
            </div>
          </Grid>
          <Grid item xs={3}>
            <InputLabel error={!!errors.taskName}>Priority</InputLabel>
            <PrioritySelector selectedPriority={selectedPriority} setSelectedPriority={setSelectedPriority} />
          </Grid>
        </Grid>
      </form>
      {confirmDelete && (
        <ConfirmDeleteTask
          setIsOpened={setConfirmDelete}
          taskId={editedTask.id!}
          setEditTask={setEditTask}
          setTasks={setTasks}
        />
      )}
    </div>
  );
}

interface PrioritySelectorProps {
  selectedPriority: number;
  setSelectedPriority: Dispatch<SetStateAction<number>>;
}

function PrioritySelector({ selectedPriority, setSelectedPriority }: PrioritySelectorProps) {
  const priorityLevels: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  return (
    <div>
      {priorityLevels.map((priority: number, key: number) => (
        <div
          className={
            selectedPriority === priority
              ? 'create-task-priority-selector-item create-task-priority-selector-item-selected'
              : 'create-task-priority-selector-item'
          }
          onClick={() => setSelectedPriority(priority)}
        >
          {priority}
        </div>
      ))}
    </div>
  );
}
