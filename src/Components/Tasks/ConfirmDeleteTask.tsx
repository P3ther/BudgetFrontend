import { Dispatch, SetStateAction } from 'react';
import { Button, Card, CardContent, Typography } from '@material-ui/core';
import { taskApi } from '../../Api/TaskApi';
import Task from '../../Interfaces/Task.interface';

import './Styles/ConfirmDeleteTask.scss';
import { loadTasks } from '../../Common/DataHandling/LoadTasks';

interface Props {
  setIsOpened: Dispatch<SetStateAction<boolean>>;
  taskId: number;
  setEditTask: Dispatch<SetStateAction<boolean>>;
  setTasks: Dispatch<SetStateAction<Task[]>>;
}

export default function ConfirmDeleteTask({ setIsOpened, taskId, setEditTask, setTasks }: Props) {
  const confirmDelete = () => {
    taskApi.removeTask(taskId).then(() => {
      loadTasks(setTasks).then(() => {
        setIsOpened(false);
        setEditTask(false);
      });
    });
  };

  return (
    <div className="confirm-delete-task-container">
      <Card
        variant="outlined"
        style={{ width: '75%', marginRight: 'auto', marginLeft: 'auto', textAlign: 'center' }}
      >
        <CardContent>
          <Typography variant="h5">Confirm deletion of the task</Typography>
          <Typography variant="body1">
            Are you sure you want to delete this task? This operation cannot be undone
          </Typography>
          <div style={{ width: '100%', textAlign: 'right' }}>
            <Button
              variant="outlined"
              size="small"
              className="confirm-delete-no-button budget-confirm-button"
              onClick={() => setIsOpened(false)}
            >
              No
            </Button>
            <Button variant="outlined" size="small" className="budget-error-button" onClick={confirmDelete}>
              Yes
            </Button>
          </div>
        </CardContent>
      </Card>
    </div>
  );
}
