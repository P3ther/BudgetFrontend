import React, { useState, useEffect } from 'react';
import {
  Container,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
  Grid,
  Button,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import Moment from 'react-moment';
import PrintIcon from '@material-ui/icons/Print';
import GetAppIcon from '@material-ui/icons/GetApp';
import LoadingOverlay from 'react-loading-overlay-ts';
import MoneyChange from '../Interfaces/MoneyChange.interface';
import { BudgetInterface } from '../Interfaces/Interfaces';

interface Props {
  budget: BudgetInterface;
}
export default function ExportComponent({ budget }: Props) {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [displayedRows, setDisplayedRows] = useState<MoneyChange[]>([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  useEffect(() => {
    const tempArray: MoneyChange[] = [];

    if (budget.incomeList !== undefined) {
      budget.incomeList.forEach((row) => {
        const tempInc = { ...row };

        tempInc.type = 'Income';
        tempArray.push(tempInc);
      });
    }

    if (budget.expensesList !== undefined) {
      budget.expensesList.forEach((row) => {
        const tempExp = { ...row };

        tempExp.type = 'Expense';
        tempArray.push(tempExp);
      });
    }

    setDisplayedRows(tempArray);
    setIsLoading(false);
  }, [budget]);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const handlePrint = () => {
    window.print();
  };

  return (
    <Container maxWidth="lg">
      <Grid container>
        <Grid item xs={6}>
          <Link
            to="/list/"
            style={{
              fontSize: '1.25rem',
              fontWeight: 500,
              lineHeight: 1.6,
              textDecoration: 'none',
              color: '#e4e6eb',
              verticalAlign: 'middle',
              display: 'inline-block',
            }}
          >
            <ArrowBackIosIcon
              style={{
                verticalAlign: 'middle',
                display: 'inline-block',
              }}
            />

            <span
              className="noPrint"
              style={{
                verticalAlign: 'middle',
                display: 'inline-block',
              }}
            >
              Export Data
            </span>
          </Link>
        </Grid>
        <Grid item xs={6} style={{ textAlign: 'right' }}>
          <Button color="primary" onClick={handlePrint}>
            <PrintIcon />
            Print
          </Button>
          <Button color="primary" disabled>
            <GetAppIcon />
            Downlaod
          </Button>
        </Grid>
      </Grid>
      <LoadingOverlay active={isLoading} spinner text="Loading">
        <TableContainer>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell style={{ width: '20%' }}>Date/Time</TableCell>
                <TableCell align="right" style={{ width: '30%' }}>
                  Name
                </TableCell>
                <TableCell align="right" style={{ width: '10%' }}>
                  Price
                </TableCell>
                <TableCell align="right" style={{ width: '10%' }}>
                  Type
                </TableCell>
                <TableCell align="right" style={{ width: '20%' }}>
                  Category
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {displayedRows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row: MoneyChange, key: number) => (
                  <TableRow key={row.id} style={{ height: '50px', overflow: 'hidden' }}>
                    <TableCell align="right">
                      <Moment format="DD.MM.YYYY kk:mm" utc local>
                        {row.date}
                      </Moment>
                    </TableCell>
                    <TableCell align="right">
                      <span
                        style={{
                          display: 'inline-block',
                          width: '20vh',
                          whiteSpace: 'nowrap',
                          overflow: 'hidden',
                          textOverflow: 'ellipsis',
                        }}
                      >
                        {row.name}
                      </span>
                    </TableCell>
                    <TableCell align="right">{`${row.price} ${row.currencySign}`}</TableCell>
                    <TableCell align="right">{row.type}</TableCell>
                    <TableCell align="right">{row.categoryName}</TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25, 100]}
          component="div"
          count={displayedRows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </LoadingOverlay>
    </Container>
  );
}
