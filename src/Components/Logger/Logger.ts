const Logger = {
  logError: (message: string) => {
    console.error(message);
  },
};

export default Logger;
