import React, { useState } from 'react';
import { Tab, Tabs } from '@material-ui/core';
import moment from 'moment';
import DateTimeRange from '../Interfaces/StartEndTime.interface';
import IncomesExpensesGraph from '../Common/Graphs/IncomesExpensesGraph';
import DateRangeSelection from '../Common/Graphs/DateRangeSelection';
import CategoryGraph from '../Common/Graphs/CategoryGraph';
import User from '../Interfaces/User.interface';

interface Props {
  userProperties: User;
}

export default function GraphsComponent({ userProperties }: Props) {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [value, setValue] = useState<number>(0);
  const [graphDateRange, setGraphDateRange] = useState<DateTimeRange>({
    startDateTime: new Date(moment().utc(false).startOf('year').toString()),
    endDateTime: new Date(moment().utc(true).toString()),
  });
  const [dateDiff, setDateDiff] = useState<string>('');

  const handleChange = (id: number) => {
    setValue(id);
  };

  return (
    <div className="budget-main-container">
      <Tabs value={value}>
        <Tab label="Icomes/Expenses" onClick={() => handleChange(0)} disableRipple />
        <Tab label="Category" onClick={() => handleChange(1)} disableRipple />
        <Tab label="Not Implemented" onClick={() => handleChange(2)} disableRipple disabled />
      </Tabs>
      <DateRangeSelection
        isLoading={isLoading}
        setGraphDateRange={setGraphDateRange}
        setDateDiff={setDateDiff}
      />
      {value === 0 && (
        <IncomesExpensesGraph
          defaultCurrency={userProperties?.defaultCurrency!}
          isLoading={isLoading}
          setIsLoading={setIsLoading}
          graphDateRange={graphDateRange}
          setGraphDateRange={setGraphDateRange}
          dateDiff={dateDiff}
        />
      )}
      {value === 1 && (
        <CategoryGraph
          defaultCurrency={userProperties?.defaultCurrency!}
          isLoading={isLoading}
          setIsLoading={setIsLoading}
          graphDateRange={graphDateRange}
          setGraphDateRange={setGraphDateRange}
          dateDiff={dateDiff}
        />
      )}
      {value === 2 && <span>Value is 2</span>}
    </div>
  );
}
