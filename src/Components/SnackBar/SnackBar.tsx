import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
import { getSnackbars } from '../../Redux/App/app.selectors';


const useNotifier = () => {
  // const dispatch = useDispatch();
  const notifications = useSelector(getSnackbars);
  const { enqueueSnackbar } = useSnackbar();
  /*
    const storeDisplayed = (id) => {
      displayed = [...displayed, id];
    };
  
    const removeDisplayed = (id) => {
      displayed = [...displayed.filter(key => id !== key)];
    };
  */

  useEffect(() => {
    console.log(notifications);
    if (notifications.length > 0) {
      enqueueSnackbar(notifications[0].message, {
        variant: notifications[0].variant,
      });
    }

    /*
    notifications.forEach(({ key, message, options = {}, dismissed = false }) => {
      if (dismissed) {
        // dismiss snackbar using notistack
        closeSnackbar(key);
        return;
      }

      // do nothing if snackbar is already displayed
      if (displayed.includes(key)) return;

      // display snackbar using notistack
      enqueueSnackbar(message, {
        key,
        ...options,
        onClose: (event, reason, myKey) => {
          if (options.onClose) {
            options.onClose(event, reason, myKey);
          }
        },
        onExited: (event, myKey) => {
          // remove this snackbar from redux store
          dispatch(removeSnackbar(myKey));
          removeDisplayed(myKey);
        },
      });

      // keep track of snackbars that we've displayed
      storeDisplayed(key);
    });
    */
  }, [notifications]);
};

export default useNotifier;
