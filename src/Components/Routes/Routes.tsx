import React, { FC, useState } from 'react';
import { Route, Switch } from 'react-router-dom';
import { useSelector } from 'react-redux';
import LoginPage from '../../Pages/Login/Login';
import Investments from '../Investments/Investments';
import BudgetInterface from '../../Interfaces/Budget.interface';
import { getBudget, getLoadingState, getUserProperties } from '../../Redux/Selectors';
import BudgetComponent from '../Budget/Budget';
import Task from '../../Interfaces/Task.interface';
import { getAuthenticated } from '../../Redux/Auth/auth.selectors';
import GraphsComponent from '../Graphs';
import ExportComponent from '../Export';
import Targets from '../Targets';
import AddTarget from '../AddTarget';
import ResetPassword from '../ResetPassword/ResetPassword';
import EditCategory from '../Categories/EditCategory';
import AddCategory from '../Categories/AddCategory';
import ListCategoryItems from '../Categories/ListCategoryItems';
import ManageIngredients from '../Ingredient/ManageIngredients';
import MainReports from '../Reports/MainReports';
import Savings from '../Savings/Savings';
import Calendar from '../Calendar/Calendar';
import { Note } from '../../Interfaces/Note.interface';
import DisplayNote from '../Note/DisplayNote';
import CreateNote from '../Note/CreateNote';
import ListNotes from '../Note/ListNotes';
import EditNote from '../Note/EditNote';
import CreateRecipe from '../Recipe/CreateRecipe';
import EditRecipe from '../Recipe/EditRecipe';
import ListRecipes from '../Recipe/ListRecipes';
import Register from '../Register/Register';
import ListComponent from '../List/List';

const Routes: FC = () => {
  const budget: BudgetInterface = useSelector(getBudget);
  const userProperties = useSelector(getUserProperties);
  const loading = useSelector(getLoadingState);
  const isAuth = useSelector(getAuthenticated);

  const [tasks, setTasks] = useState<Task[]>([]);
  const [notes, setNotes] = useState<Note[]>([]);

  if (isAuth === false) {
    return <LoginPage />;
  }

  if (!isAuth) return null;

  return (
    <Switch>
      <Route
        exact
        path="/"
        render={(p) => (
          <BudgetComponent
            setShowAddMoneyChange={() => { }}
            loadingData={loading}
            tasks={tasks}
            setTasks={setTasks}
          />
        )}
      />
      <Route
        exact
        path="/investments/"
        render={(p) => <Investments userProperties={userProperties} budget={budget} />}
      />
      <Route exact path="/list/" render={(p) => <ListComponent />} />
      <Route exact path="/graphs/" render={(p) => <GraphsComponent userProperties={userProperties} />} />
      <Route exact path="/export/" render={(p) => <ExportComponent budget={budget} />} />
      <Route exact path="/targets/" render={(p) => <Targets />} />
      <Route exact path="/addTarget/" render={(p) => <AddTarget />} />
      <Route exact path="/resetPassword/" render={(p) => <ResetPassword />} />
      <Route
        exact
        path="/editCategory/:id/"
        render={(p) => <EditCategory categoryId={p.match.params.id} />}
      />
      <Route exact path="/addCategory/" render={(p) => <AddCategory />} />
      <Route
        exact
        path="/category/:id/items/"
        render={(p) => <ListCategoryItems categoryId={p.match.params.id} />}
      />
      <Route exact path="/ingredients/" render={(p) => <ManageIngredients />} />
      <Route exact path="/reports/" render={() => <MainReports />} />
      <Route exact path="/savings/" render={() => <Savings />} />
      <Route exact path="/calendar/" render={(p) => <Calendar />} />
      <Route
        exact
        path="/displayNote/:id/"
        render={(p) => <DisplayNote notes={notes} setNotes={setNotes} displayedNoteId={p.match.params.id} />}
      />
      <Route exact path="/createNote/" render={(p) => <CreateNote />} />
      <Route
        exact
        path="/editNote/:id/"
        render={(p) => <EditNote notes={notes} setNotes={setNotes} editedNoteId={p.match.params.id} />}
      />
      <Route exact path="/notes/" render={(p) => <ListNotes notes={notes} setNotes={setNotes} />} />
      <Route exact path="/createRecipe/" render={(p) => <CreateRecipe />} />
      <Route
        exact
        path="/editRecipe/:id/"
        render={(p) => <EditRecipe selectedRecipe={p.match.params.id} />}
      />
      <Route exact path="/recipes/" render={(p) => <ListRecipes selectedRecipe={undefined} />} />
      <Route exact path="/recipes/:id/" render={(p) => <ListRecipes selectedRecipe={p.match.params.id} />} />
      <Route exact path="/register/" render={() => <Register />} />
    </Switch>
  );
};

export default Routes;
