import React, { useEffect, useMemo, useState } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { CssBaseline, Grid, StylesProvider } from '@material-ui/core';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import Appbar from './Components/Appbar';
import Profile from './Drawers/Profile/Profile';
import Theme from './Interfaces/Theme.interface';
import AddIncome from './Dialogs/AddIncome';
import LeftMenu from './Components/LeftMenu';
import SuccessSnackbar from './Common/SnackBars/SuccessSnackbar';
import { CategoryInterface } from './Interfaces/Interfaces';
import './Styles/LoginStyle.css';
import './Styles/App.css';
import './Styles/App.scss';
import './Styles/BudgetConfirmButton.scss';
import './Styles/BudgetWarningButton.scss';
import './Styles/BudgetErrorButton.scss';
import './Styles/BaseDialog.scss';
import AddExpense from './Dialogs/AddExpense/AddExpense';
import { getAllBasicData } from './Redux/Actions';
import { getDateTimeRange } from './Redux/Selectors';
import Routes from './Components/Routes/Routes';
import { getAuthenticated } from './Redux/Auth/auth.selectors';
import useNotifier from './Components/SnackBar/SnackBar';
import { isAuthenticated } from './Redux/Auth/auth.actions';
import { getAddExpense, getAddIncome } from './Redux/App/app.selectors';

function App() {
  useNotifier();
  const [isLogedin, setIsLogedin] = useState<boolean>(false);
  const [isProfileDrawer, setIsProfileDrawer] = useState<boolean>(false);
  const dateRange = useSelector(getDateTimeRange);
  const [userTheme, setUserTheme] = useState<Theme>({} as Theme);
  const [allCategories, setAllCategories] = useState<CategoryInterface[]>([]);
  const dispatch = useDispatch();
  const isAuth = useSelector(getAuthenticated);
  const isAddExpense = useSelector(getAddExpense);
  const isAddIncome = useSelector(getAddIncome);
  const state = useSelector((state: RootStateOrAny) => state);
  const gridSize = useMemo(() => {
    if (isAuth) {
      if (window.screen.width <= 480) {
        return 12;
      }

      return 10;
    }

    return 12;
  }, [isAuth]);

  useEffect(() => {
    dispatch(isAuthenticated());
    if (isAuth) {
      dispatch(getAllBasicData(dateRange));
    }
  }, [isAuth]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    console.log('State is:');
    console.log(state);
    console.log(`Add Expense? ${isAddExpense}`);
    console.log(`Add Income? ${isAddIncome}`);
  }, [state, isAddIncome, isAddExpense]);


  return (
    <StylesProvider injectFirst>
      <CssBaseline />
      <BrowserRouter>
        <SuccessSnackbar />
        <Appbar
          isLogedin={isLogedin}
          setIsLogedin={setIsLogedin}
          setIsProfileDrawer={setIsProfileDrawer}
          showAddMoneyChange={false}
          setIsAddIncomeOpened={() => { }}
        />
        <Grid
          container
          style={{ height: '100%', overflow: 'visible' }}
          spacing={0}
          className="budget-app-grid-container"
        >
          {isAuth && <LeftMenu />}
          <Grid item xs={gridSize} className="app-main-content-grid">
            <Routes />
          </Grid>
          <Profile
            isProfileDrawer={isProfileDrawer}
            setIsProfileDrawer={setIsProfileDrawer}
            userTheme={userTheme}
            setUserTheme={setUserTheme}
            setIsLogedin={setIsLogedin}
            setLoadingData={() => { }}
            setTasks={() => { }}
            setAllCategories={setAllCategories}
          />
          <AddIncome allCategories={allCategories} />
          <AddExpense />
        </Grid>
      </BrowserRouter>
    </StylesProvider>
  );
}

export default App;
