import React from 'react';
import ReactDOM from 'react-dom';
import { Provider, ReactReduxContext } from 'react-redux';
import { StyledEngineProvider } from '@mui/material/styles';
import { SnackbarProvider } from 'notistack';
import App from './App';
import { store } from './Redux/Store';

ReactDOM.render(
  <Provider store={store} context={ReactReduxContext}>
    <StyledEngineProvider injectFirst>
      <SnackbarProvider maxSnack={3} anchorOrigin={{ horizontal: 'right', vertical: 'top' }}>
        <React.StrictMode>
          <App />
        </React.StrictMode>
      </SnackbarProvider>
    </StyledEngineProvider>
  </Provider>,
  document.getElementById('root'),
);
