export enum UserApiEndpoints {
  authenticate = 'login',
  forgotPassword = '/user/forgotPassword/',
  resetPassword = '/user/resetPassword/',
  userProperties = '/user/properties/',
  verifyRegistrationCode = '/user/regenerationCode/',
}
