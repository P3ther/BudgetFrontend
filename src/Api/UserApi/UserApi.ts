import User from '../../Interfaces/User.interface';
import { axiosInstance } from '../AxiosInstance';
import { UserApiEndpoints } from './UserApi.types';

export const userApi = {
  authenticate: (user: User) => axiosInstance.post(UserApiEndpoints.authenticate, user),
  generateForgotPasswordCode: (user: User) =>
    axiosInstance.post<User>(UserApiEndpoints.forgotPassword, user).then((res) => res.data),
  getUserProperties: () => axiosInstance.get<User>(UserApiEndpoints.userProperties).then((res) => res.data),
  resetPassword: (user: User) =>
    axiosInstance.post(UserApiEndpoints.resetPassword, user).then((res) => res.data),
  updateUserProperties: (editedUser: User) =>
    axiosInstance.post<User>(UserApiEndpoints.userProperties, editedUser).then((res) => res.data),
  verifyRegistrationCode: (user: User) =>
    axiosInstance.post(UserApiEndpoints.verifyRegistrationCode, user).then((res) => res.data),
};
