import Investment from '../Interfaces/Investment.interface';
import { axiosInstance } from './AxiosInstance';

enum endPoints {
  investment = '/investment/',
}

export const investmentApi = {
  createInvestment: (newInvestment: Investment) =>
    axiosInstance.post<Investment>(endPoints.investment, newInvestment).then((res) => res.data),
};
