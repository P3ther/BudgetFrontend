import { NewNoteBE, NoteBE } from '../Interfaces/Note.interface';
import { axiosInstance } from './AxiosInstance';

enum endPoints {
  note = '/note/',
  notes = '/notes/',
}

export const noteApi = {
  getNote: (noteId: number) =>
    axiosInstance.get<NoteBE>(`${endPoints.note + noteId}/`).then((res) => res.data),
  createNote: (newNote: NewNoteBE) =>
    axiosInstance.post<NoteBE>(endPoints.note, newNote).then((res) => res.data),
  updateNote: (editedNote: NoteBE) =>
    axiosInstance.put<NoteBE>(endPoints.note, editedNote).then((res) => res.data),
  listNotes: () => axiosInstance.get<NoteBE[]>(endPoints.notes).then((res) => res.data),
  deleteNote: (noteId: number) => axiosInstance.delete(`${endPoints.note + noteId}/`),
};
