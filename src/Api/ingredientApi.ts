import { Ingredient, NewIngredient } from '../Interfaces/Food/Ingredient.interface';
import { axiosInstance } from './AxiosInstance';

enum endPoints {
  ingredient = '/ingredient/',
  ingredients = '/ingredients/',
}

export const ingredientApi = {
  getIngredient: (id: number) =>
    axiosInstance.get<Ingredient>(`${endPoints.ingredient + id}/`).then((res) => res.data),
  createIngredient: (ingredient: NewIngredient) =>
    axiosInstance.post<Ingredient>(endPoints.ingredient, ingredient).then((res) => res.data),
  updateIngredient: (ingredient: Ingredient) =>
    axiosInstance.put<Ingredient>(endPoints.ingredient, ingredient).then((res) => res.data),
  listIngredient: () => axiosInstance.get<Ingredient[]>(endPoints.ingredients).then((res) => res.data),
  deleteIngredient: (id: number) => axiosInstance.delete(`${endPoints.ingredient + id}/`),
};
