export interface EkasaResponse {
  receipt: {
    totalPrice: number;
    createDate: string;
    date: Date;
    organization: {
      name: string;
    };
  };
}
