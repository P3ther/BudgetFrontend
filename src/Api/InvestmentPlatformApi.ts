import InvestmentPlatform from '../Interfaces/InvestmentPlatform.interface';
import { axiosInstance } from './AxiosInstance';

enum endPoints {
  investmentPlatform = '/investmentPlatform/',
  investmentPlatforms = '/investmentPlatforms/',
}

export const investmentPlatformApi = {
  getInvestmentPlatform: (id: number) =>
    axiosInstance.get<InvestmentPlatform>(`${endPoints.investmentPlatform + id}/`).then((res) => res.data),
  createInvestmentPlatform: (newPlatform: InvestmentPlatform) =>
    axiosInstance.post<InvestmentPlatform>(endPoints.investmentPlatform, newPlatform).then((res) => res.data),
  updateInvestmentPlatform: (updatedPlatform: InvestmentPlatform) =>
    axiosInstance
      .put<InvestmentPlatform>(endPoints.investmentPlatform, updatedPlatform)
      .then((res) => res.data),
  deleteInvestmentPlatform: (id: number) => axiosInstance.delete(`${endPoints.investmentPlatform + id}/`),
  listInvestmentPlatforms: () =>
    axiosInstance.get<InvestmentPlatform[]>(endPoints.investmentPlatforms).then((res) => res.data),
};
