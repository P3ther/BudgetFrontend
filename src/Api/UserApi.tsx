import User from '../Interfaces/User.interface';
import { axiosInstance } from './AxiosInstance';

enum endPoints {
  forgotPassword = '/user/forgotPassword/',
  resetPassword = '/user/resetPassword/',
  userProperties = '/user/properties/',
  verifyRegistrationCode = '/user/regenerationCode/',
}

export const userApi = {
  generateForgotPasswordCode: (user: User) =>
    axiosInstance.post<User>(endPoints.forgotPassword, user).then((res) => res.data),
  getUserProperties: () => axiosInstance.get<User>(endPoints.userProperties).then((res) => res.data),
  resetPassword: (user: User) => axiosInstance.post(endPoints.resetPassword, user).then((res) => res.data),
  updateUserProperties: (editedUser: User) =>
    axiosInstance.post<User>(endPoints.userProperties, editedUser).then((res) => res.data),
  verifyRegistrationCode: (user: User) =>
    axiosInstance.post(endPoints.verifyRegistrationCode, user).then((res) => res.data),
};
