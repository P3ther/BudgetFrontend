import axios, { AxiosError } from 'axios';
import { Agent } from 'https';
import { backEndAddress } from '../Common/Variables';
import { getToken } from '../Common/Auth';
import { logoutMethod } from '../Redux/Auth/auth.actions';

export const axiosInstance = axios.create({
  baseURL: backEndAddress,
  headers: { Authorization: getToken() },
  httpsAgent: new Agent({
    rejectUnauthorized: false,
  }),
});

export const axiosUpdate = async () => {
  axiosInstance.defaults.headers.Authorization = getToken();
};

axiosInstance.interceptors.response.use(
  (response) => response,
  async (error: AxiosError) => {
    if (error.response?.status === 403 && !error.request.responseURL.includes('login')) {
      logoutMethod();
      window.location.reload();
    }

    return Promise.reject(error);
  },
);
