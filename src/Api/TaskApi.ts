import Task from '../Interfaces/Task.interface';
import { axiosInstance } from './AxiosInstance';

enum endPoints {
  task = '/task/',
  tasks = '/tasks/',
}

export const taskApi = {
  getTask: (taskId: number) => axiosInstance.get<Task>(`${endPoints.task + taskId}/`).then((res) => res.data),
  createTask: (newTask: Task) => axiosInstance.post<Task>(endPoints.task, newTask).then((res) => res.data),
  updateTask: (editedTask: Task) =>
    axiosInstance.put<Task>(endPoints.task, editedTask).then((res) => res.data),
  listTasks: () => axiosInstance.get<Task[]>(endPoints.tasks).then((res) => res.data),
  removeTask: (taskId: number) => axiosInstance.delete(`${endPoints.task + taskId}/`),
};
