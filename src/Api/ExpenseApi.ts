import { AxiosResponse } from 'axios';
import Expense from '../Interfaces/Expense.interface';
import DateTimeRange from '../Interfaces/StartEndTime.interface';
import { axiosInstance } from './AxiosInstance';
import { EkasaResponse } from './Types/Expense.types';

export enum ExpenseEndPoints {
  expensesByTime = 'expense/time/',
  getReceiptQrCodeDetails = '/expense/checkBill/',
}

export const expenseApi = {
  getExpensesByTime: (dateRange: DateTimeRange) =>
    axiosInstance
      .post<Expense[]>(ExpenseEndPoints.expensesByTime, dateRange)
      .then((res: AxiosResponse<Expense[]>) => res.data),
  getReceiptQrCodeDetails: (billId: String) =>
    axiosInstance
      .post(ExpenseEndPoints.getReceiptQrCodeDetails, { receiptId: billId })
      .then((res: AxiosResponse<EkasaResponse>) => res.data),
};
