import { AxiosResponse } from 'axios';
import { NewRecipeBE, RecipeBE } from '../Interfaces/Food/Recipe.interface';
import { axiosInstance } from './AxiosInstance';

enum endPoints {
  recipe = '/recipe/',
  recipes = '/recipes/',
}

export const recipeApi = {
  getRecipe: (recipeId: number) =>
    axiosInstance.get<RecipeBE>(`${endPoints.recipe + recipeId}/`).then((res) => res.data),
  createRecipe: (newRecipe: NewRecipeBE) =>
    axiosInstance.post<RecipeBE>(endPoints.recipe, newRecipe).then((res) => res.data),
  updateRecipe: (editedRecipe: RecipeBE) =>
    axiosInstance.put<RecipeBE>(endPoints.recipe, editedRecipe).then((res) => res.data),
  listRecipes: () =>
    axiosInstance.get<RecipeBE[]>(endPoints.recipes).then((res: AxiosResponse<RecipeBE[]>) => res.data),
  deleteRecipe: (recipeId: number) => axiosInstance.delete(`${endPoints.recipe + recipeId}/`),
};
