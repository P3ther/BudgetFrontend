import CurrencyPairs from '../Interfaces/CurrencyPairs.interface';
import { axiosInstance } from './AxiosInstance';

enum endPoints {
  currencyPairs = 'currencyPairs/',
  exchangeRates = 'exchangeRates/',
}
export const currencyPairsApi = {
  getCurrencyPairs: () => axiosInstance.get<CurrencyPairs[]>(endPoints.currencyPairs).then((res) => res.data),
  updateExchangeRates: () => axiosInstance.get(endPoints.exchangeRates),
};
