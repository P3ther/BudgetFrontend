import { DateInterface, SavingsInterface } from '../Interfaces/Interfaces';
import DateTimeRange from '../Interfaces/StartEndTime.interface';
import { axiosInstance } from './AxiosInstance';

enum endPoints {
  savings = '/savings/',
  savingsForMonth = '/savings/month/',
  savingsForRange = '/savings/range/',
}

export const savingsApi = {
  createSavings: (newSaving: SavingsInterface) =>
    axiosInstance.post<SavingsInterface>(endPoints.savings, newSaving).then((res) => res.data),
  listSavings: () => axiosInstance.get<SavingsInterface[]>(endPoints.savings).then((res) => res.data),
  readSavingsForMonth: (date: DateInterface) =>
    axiosInstance.post<SavingsInterface>(endPoints.savingsForMonth, date).then((res) => res.data),
  readSavingsForRange: (date: DateTimeRange) =>
    axiosInstance.post<SavingsInterface[]>(endPoints.savingsForRange, date).then((res) => res.data),
};
