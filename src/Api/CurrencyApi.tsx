import Currency from '../Interfaces/Currency.interface';
import { axiosInstance } from './AxiosInstance';

enum endPoints {
  currency = 'currency/',
  editCurrency = 'currency/edit/',
}

export const currencyApi = {
  getCurrencies: () => axiosInstance.get<Currency[]>(endPoints.currency).then((res) => res.data),
  addNewCurrency: (newCurrency: Currency) =>
    axiosInstance.post<Currency[]>(endPoints.currency, newCurrency).then((res) => res.data),
  editCurrency: (editedCurrency: Currency) =>
    axiosInstance.post<Currency[]>(endPoints.editCurrency, editedCurrency).then((res) => res.data),
};
