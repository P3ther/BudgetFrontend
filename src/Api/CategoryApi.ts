import { axiosInstance } from './AxiosInstance';
/* Interfaces */
import DateTimeRange from '../Interfaces/StartEndTime.interface';
import { CategoryInterface, NewCategoryInterface } from '../Interfaces/Interfaces';

enum endPoints {
  category = '/category/',
  categories = '/categories/',
  categoriesValuesForTime = '/categories/time/',
  categoriesExpense = '/categories/expense/',
  categoriesIncome = '/categories/income/',
}

export const categoryApi = {
  readCategory: (id: string) =>
    axiosInstance.get<CategoryInterface>(`${endPoints.category + id}/`).then((res) => res.data),
  readCategoryByTime: (id: string, dateRange: DateTimeRange) =>
    axiosInstance
      .post<CategoryInterface>(`${endPoints.category + id}/time/`, dateRange)
      .then((res) => res.data),
  getCategories: () => axiosInstance.get<CategoryInterface[]>(endPoints.categories).then((res) => res.data),
  getCategoriesAndValuesForTime: (dateRange: DateTimeRange) =>
    axiosInstance
      .post<CategoryInterface[]>(endPoints.categoriesValuesForTime, dateRange)
      .then((res) => res.data),
  getExpenseCategories: () =>
    axiosInstance.get<CategoryInterface[]>(endPoints.categoriesExpense).then((res) => res.data),
  getIncomeCategories: () =>
    axiosInstance.get<CategoryInterface[]>(endPoints.categoriesIncome).then((res) => res.data),
  createCategory: (newCategory: NewCategoryInterface) =>
    axiosInstance.post<CategoryInterface>(endPoints.category, newCategory).then((res) => res.data),
  updateCategory: (updatedCategory: CategoryInterface) =>
    axiosInstance.put<CategoryInterface>(endPoints.category, updatedCategory).then((res) => res.data),
  deleteCategory: (id: number) => axiosInstance.delete(`${endPoints.category + id}/`),
  deleteCategoryAndAllItems: (id: number) => axiosInstance.delete(`${endPoints.category + id}/all/`),
};
