import Income from '../Interfaces/Income.interface';
import DateTimeRange from '../Interfaces/StartEndTime.interface';
import { axiosInstance } from './AxiosInstance';

enum endPoints {
  incomesByTime = '/income/time/',
}

export const incomeApi = {
  getIncomesByTime: (dateRange: DateTimeRange) =>
    axiosInstance.post<Income[]>(endPoints.incomesByTime, dateRange).then((res) => res.data),
};
