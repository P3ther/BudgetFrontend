import { createReducer } from '@reduxjs/toolkit';
import { AppState } from '../BudgetState.types';
import { changeAddExpenseDialog, changeAddIncomeDialog, enqueueSnackbar } from './app.action';

const initialState: AppState = {
  snackbars: [],
  dialogs: {
    addExpense: false,
    addIncome: false,
  },
};

export const appReducer = createReducer<AppState>(initialState, (builder) => {
  builder.addCase(enqueueSnackbar.fulfilled, (state: AppState, { payload }) => {
    state.snackbars = [
      ...state.snackbars,
      {
        id: payload.id || 0,
        variant: payload.variant!,
        message: payload.message!,
      },
    ];
  });

  builder.addCase(changeAddExpenseDialog.fulfilled, (state: AppState, { payload }) => {
    state.dialogs.addExpense = payload;
  });

  builder.addCase(changeAddIncomeDialog.fulfilled, (state: AppState, { payload }) => {
    state.dialogs.addIncome = payload;
  });
});
