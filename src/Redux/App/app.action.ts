import { createAsyncThunk } from '@reduxjs/toolkit';
import { SnackbarDetails } from '../BudgetState.types';
import { getAddExpense, getAddIncome } from './app.selectors';
import { RootState } from '../Store';

export const enqueueSnackbar = createAsyncThunk<SnackbarDetails, SnackbarDetails>(
  'ENQUEUE:SNACKBAR',
  async (snackbarDetails) => snackbarDetails,
);

export const changeAddExpenseDialog = createAsyncThunk<boolean, void, { state: RootState }>(
  'ADDEXPENSE:OPEN',
  async (status, { getState }) => !getAddExpense(getState()),
);

export const changeAddIncomeDialog = createAsyncThunk<boolean, void, { state: RootState }>(
  'ADDINCOME:OPEN',
  async (status, { getState }) => !getAddIncome(getState()),
);
