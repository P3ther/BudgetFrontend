import { RootState } from '../Store';

export const getSnackbars = (state: RootState) => state.app.snackbars;

export const getAddExpense = (state: RootState) => state.app.dialogs.addExpense;

export const getAddIncome = (state: RootState) => state.app.dialogs.addIncome;
