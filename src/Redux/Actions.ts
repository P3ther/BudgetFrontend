import { createAction, createAsyncThunk } from '@reduxjs/toolkit';
import Expense, { AddExpense } from '../Interfaces/Expense.interface';
import { api } from '../Common/Api';
import Logger from '../Components/Logger/Logger';
import DateTimeRange from '../Interfaces/StartEndTime.interface';
import { taskApi } from '../Api/TaskApi';
import { userApi } from '../Api/UserApi';
import { categoryApi } from '../Api/CategoryApi';
import { currencyApi } from '../Api/CurrencyApi';
import Currency from '../Interfaces/Currency.interface';
import Budget from '../Interfaces/Budget.interface';
import Task from '../Interfaces/Task.interface';
import User from '../Interfaces/User.interface';
import { BudgetInterface, CategoryInterface } from '../Interfaces/Interfaces';
import Income from '../Interfaces/Income.interface';
import { changeAddIncomeDialog } from './App/app.action';
import { RootState } from './Store';

export const changeVisibilityExpenseDialog = createAction<void>('EXPENSE:VIEW/CLOSE:DIALOG');

export const getLoadingStatus = createAction<void>('GET:LOADING');

export const addExpenseAction = createAsyncThunk<Expense, AddExpense, { state: RootState }>(
  'EXPENSE:ADD',
  async (expense, { getState, dispatch, rejectWithValue }) => {
    try {
      const expenseResponse = await api.addExpense(expense);

      dispatch(getAllBasicData(getState().budget.dateRange));

      return expenseResponse;
    } catch (err) {
      Logger.logError(err);

      return rejectWithValue(err);
    }
  },
);

export const addIncome = createAsyncThunk<Income, Income, { state: RootState }>(
  'ADDINCOME',
  async (income, { rejectWithValue, dispatch }) => {
    try {
      const incomeResponse = await api.addIncome(income);

      dispatch(changeAddIncomeDialog());

      return incomeResponse.data;
    } catch (error) {
      return rejectWithValue(error);
    }
  },
);

export const getAllBasicData = createAsyncThunk<
  [Budget, Task[], User, CategoryInterface[], Currency[]],
  DateTimeRange
>('BUDGET:DATA:GET', async (dateTime, { rejectWithValue }) => {
  try {
    const budget = api.getTimedBudget(dateTime);
    const tasks = taskApi.listTasks();
    const userProperties = userApi.getUserProperties();
    const categories = categoryApi.getCategoriesAndValuesForTime(dateTime);
    const currencies = await currencyApi.getCurrencies();

    return await Promise.all([budget, tasks, userProperties, categories, currencies]);
  } catch (err) {
    Logger.logError(err);

    return rejectWithValue(err);
  }
});

export const getTimedBudget = createAsyncThunk<BudgetInterface, DateTimeRange>(
  'GET_TIMED_BUDGET',
  async (dateTimeRange, { rejectWithValue }) => {
    try {
      return api.getTimedBudget(dateTimeRange);
    } catch (error) {
      return rejectWithValue(error);
    }
  },
);
