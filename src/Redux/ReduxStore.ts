// Libraries
import { createReducer } from '@reduxjs/toolkit';
import moment from 'moment';
import Currency from '../Interfaces/Currency.interface';
// Interfaces
import { BudgetInterface, CategoryInterface } from '../Interfaces/Interfaces';
import DateTimeRange from '../Interfaces/StartEndTime.interface';
import { addExpenseAction, addIncome, changeVisibilityExpenseDialog, getAllBasicData } from './Actions';
import Task from '../Interfaces/Task.interface';
import User from '../Interfaces/User.interface';

export interface BudgetStateOld {
  loading: boolean;
  dateRange: DateTimeRange;
  budget: BudgetInterface;
  successSnackbar: boolean;
  succesSnackbarMessage: string;
  categories: CategoryInterface[];
  currencies: Currency[];
  tasks: Task[];
  addExpense: boolean;
  userProperties?: User;
}

export const initialState: BudgetStateOld = {
  loading: true,
  dateRange: {
    startDateTime: new Date(moment().startOf('month').toString()),
    endDateTime: new Date(),
  } as DateTimeRange,
  budget: {
    id: 0,
    expensesList: [],
    incomeList: [],
    totalBalance: 0,
    totalExpense: 0,
    totalIncome: 0,
    expectedIncome: 0,
    expectedExpense: 0,
    totalInvestmentsValue: 0,
    totalInvestmentsGain: 0,
  },
  successSnackbar: false,
  succesSnackbarMessage: '',
  categories: [],
  currencies: [],
  tasks: [],
  addExpense: false,
};

export enum ReducerActionsEnum {
  dateRange = 'dateRange',
  budget = 'budget',
  successSnackbar = 'successSnackbar',
  succesSnackbarMessage = 'succesSnackbarMessage',
  categories = 'categories',
  currencies = 'currencies',
  addExpense = 'addExpense',
}

export const Reducer = createReducer<BudgetStateOld>(initialState, (builder) => {
  builder
    // every time location changes
    // Inventory
    .addCase(addExpenseAction.pending, (state) => {
      // state.pending = true;
    })
    .addCase(addExpenseAction.fulfilled, (state, { payload }) => {
      state.addExpense = false;
      // state.pending = false;
      // state.inventory = payload;
    })
    .addCase(addExpenseAction.rejected, (state) => {
      // state. = false;
    })
    .addCase(addIncome.fulfilled, (state, { payload }) => {
      state.budget.incomeList = [payload, ...state.budget.incomeList];
    })
    .addCase(changeVisibilityExpenseDialog, (state) => {
      state.addExpense = !state.addExpense;
    })
    /* .addCase(getAllBasicData.pending, (state) => {
      state.loading = true;
    }) */
    .addCase(getAllBasicData.fulfilled, (state, { payload }) => {
      state.budget = payload[0];
      state.tasks = payload[1];
      state.userProperties = payload[2];
      state.categories = payload[3];
      state.currencies = payload[4];
      state.loading = false;
    })
    .addCase(getAllBasicData.rejected, (state) => {
      state.loading = false;
    });
});
