import { BudgetStateOld } from './ReduxStore';

export interface BudgetState {
  authState: AuthState;
  budgetStateOld: BudgetStateOld;
}

export interface AuthState {
  isAuthenticated?: boolean;
  authErrors?: string;
}

export interface AppState {
  snackbars: SnackbarDetails[];
  dialogs: DialogsState;
}

export interface SnackbarDetails {
  id?: number;
  variant: 'default' | 'error' | 'info' | 'success' | 'warning';
  message: string;
}

export interface DialogsState {
  addExpense: boolean;
  addIncome: boolean;
}
