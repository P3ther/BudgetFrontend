import { authReducer } from './Auth/auth.reducer';
import { Reducer } from './ReduxStore';
import { appReducer } from './App/app.reducer';

export const BudgetReducer = {
  budget: Reducer,
  auth: authReducer,
  app: appReducer,
};
