import { configureStore } from '@reduxjs/toolkit';
import { BudgetReducer } from './Reducer';

export const store = configureStore({
  reducer: BudgetReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware(),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
