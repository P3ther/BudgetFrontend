import { CategoryInterface } from '../Interfaces/Interfaces';
import { RootState } from './Store';

export const getBudget = (state: RootState) => state.budget.budget;

export const getExpenseState = (state: RootState) => state.budget.addExpense;

export const getLoadingState = (state: RootState) => state.budget.loading;

export const getCurrencies = (state: RootState) => state.budget.currencies;

export const getUserProperties = (state: RootState) => state.budget.userProperties;

export const getDateTimeRange = (state: RootState) => state.budget.dateRange;

export const getCategories = (state: RootState) => state.budget.categories;

export const getIncomeCategories = (state: RootState) =>
  state.budget.categories.filter((category: CategoryInterface) => category.type === 1);

export const getDefaultCurrency = (state: RootState) => state.budget.userProperties?.defaultCurrency;
