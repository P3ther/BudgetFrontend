import { createAsyncThunk } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';
import jwt from 'jwt-decode';
import { userApi } from '../../Api/UserApi/UserApi';
import User from '../../Interfaces/User.interface';
import { axiosUpdate } from '../../Api/AxiosInstance';

interface Token {
  sub: string;
  exp: number;
}

export const authenticate = createAsyncThunk<string, User, { rejectValue: string }>(
  'AUTHENTICATE',
  async (user, { rejectWithValue, dispatch }) => {
    const token: string = await userApi
      .authenticate(user)
      .then((response) => {
        localStorage.setItem('token', response.headers.authorization);

        return response.headers.authorization;
      })
      .catch((error: AxiosError) => {
        if (error.message === 'Network Error') {
          return rejectWithValue('There was a Network Error, try again later');
        }

        if (error.response?.status === 403) {
          return rejectWithValue('Wrong Username or password');
          // dispatch(enqueueSnackbar({ variant: 'error', message: 'Error' }));
        }

        return rejectWithValue('Unknown Error.');
      });

    await axiosUpdate();

    return token;
  },
);

export const logout = createAsyncThunk<void, void>('LOGOUT', async (user, { rejectWithValue }) => {
  try {
    logoutMethod();
  } catch (error) {
    rejectWithValue(error);
  }
});

export const isAuthenticated = createAsyncThunk<boolean, void>('AUTHENTICATE:CHECK', async () => {
  try {
    const token: Token | null = jwt(localStorage.getItem('token') || '');
    const date: number = Date.now() / 1000;

    if (token?.exp! > date && token !== null) return true;

    logout();

    return false;
  } catch (error) {
    return false;
  }
});

export const logoutMethod = () => {
  localStorage.removeItem('theme');
  localStorage.removeItem('token');
  localStorage.removeItem('user');
};
