import { createReducer } from '@reduxjs/toolkit';
import { AuthState } from '../BudgetState.types';
import { authenticate, isAuthenticated, logout } from './auth.actions';

const initialState: AuthState = {
  authErrors: '',
};

export enum AuthReducerEnum {
  authenticate = 'authenticate',
  isAuthenticated = 'isAuthenticated',
}

export const authReducer = createReducer<AuthState>(initialState, (builder) => {
  builder.addCase(authenticate.fulfilled, (state) => {
    state.isAuthenticated = true;
  });
  builder.addCase(authenticate.rejected, (state, { payload }) => {
    state.isAuthenticated = false;
    state.authErrors = payload;
  });

  builder.addCase(isAuthenticated.fulfilled, (state, { payload }) => {
    state.isAuthenticated = payload;
  });

  builder.addCase(isAuthenticated.rejected, (state) => {
    state.isAuthenticated = false;
  });

  builder.addCase(logout.fulfilled, (state) => {
    state.isAuthenticated = false;
  });
  builder.addCase(logout.rejected, (state) => {
    state.isAuthenticated = false;
  });
});
