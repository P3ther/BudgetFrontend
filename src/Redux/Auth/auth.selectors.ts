import { RootState } from '../Store';

export const getAuthenticated = (state: RootState) => state.auth.isAuthenticated;

export const getAuthenticationError = (state: RootState) => state.auth.authErrors;
