import React, { Dispatch, SetStateAction, useState } from 'react';
import {
  Typography,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  InputLabel,
  TextField,
  Button,
  MenuItem,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Controller, useForm } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { api } from '../../../Common/Api';

import { userApi } from '../../../Api/UserApi';
/* Enums */
import { ProfileDrawerEnums } from '../../../Enums/ProfileDrawerEnums';
/* Interfaces */
import User from '../../../Interfaces/User.interface';
import Theme from '../../../Interfaces/Theme.interface';
/* Styles */
import './SelectTheme.scss';
import { getUserProperties } from '../../../Redux/Selectors';

interface Props {
  userTheme: Theme;
  setUserTheme: Dispatch<SetStateAction<Theme>>;
  openedAccordion: ProfileDrawerEnums;
  closeAccordions: Function;
}

export default function SelectTheme({ userTheme, setUserTheme, openedAccordion, closeAccordions }: Props) {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [themes, setThemes] = useState<Theme[]>([]);
  const userProperties = useSelector(getUserProperties);
  const {
    handleSubmit,
    control,
    formState: { isDirty, isSubmitting },
    reset,
  } = useForm<Theme>({
    defaultValues: {
      id: userProperties?.defaultTheme?.id,
    },
  });

  const onSelectThemeEnter = () => {
    api
      .getThemes()
      .then((res) => {
        setThemes(res);
        setIsLoading(false);
      })
      .catch((err) => console.warn(err));
  };

  const onSelectThemeExit = () => {
    setThemes([]);
  };

  const onSubmit = (data: Theme) => {
    const updatedUserAndProperties: User = {
      username: userProperties?.username!,
      defaultTheme: themes.find((theme: Theme) => (theme.id = data.id)),
    };

    userApi
      .updateUserProperties(updatedUserAndProperties)
      .then((res: User) => {
        reset(userProperties?.defaultTheme);
      })
      .catch((err) => console.warn(err));
  };

  const handleOpen = () => {
    closeAccordions(ProfileDrawerEnums.selectYourTheme);
    if (isDirty) {
      reset();
    }
  };

  const handleClose = () => {
    if (isDirty) {
      reset();
      closeAccordions(ProfileDrawerEnums.selectYourTheme);
    } else {
      closeAccordions(ProfileDrawerEnums.none);
    }
  };

  return (
    <Accordion
      expanded={openedAccordion === ProfileDrawerEnums.selectYourTheme}
      TransitionProps={{
        onEnter: () => onSelectThemeEnter(),
        onExit: () => onSelectThemeExit(),
      }}
    >
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        onClick={openedAccordion === ProfileDrawerEnums.selectYourTheme ? handleClose : handleOpen}
      >
        <Typography>Select Your Theme:</Typography>
      </AccordionSummary>
      <AccordionDetails className="select-theme-accordion-details ">
        <InputLabel style={{ marginTop: '1vh' }}>Theme:</InputLabel>
        <Controller
          as={
            <TextField fullWidth size="small" select>
              {themes.map((theme: Theme, key: number) => (
                <MenuItem key={key} value={theme.id}>
                  {theme.themeName}
                </MenuItem>
              ))}
            </TextField>
          }
          name="id"
          variant="outlined"
          control={control}
          disabled={isLoading}
        />
        <div className="select-theme-button-container">
          <Button color="secondary" className="DefaultButton DrawerMarginRight1" onClick={handleClose}>
            Close
          </Button>
          <Button
            variant="contained"
            color="primary"
            className="DefaultButton"
            disabled={!isDirty || isSubmitting}
            onClick={handleSubmit(onSubmit)}
          >
            {isSubmitting ? 'Sending...' : 'Submit'}
          </Button>
        </div>
      </AccordionDetails>
    </Accordion>
  );
}
