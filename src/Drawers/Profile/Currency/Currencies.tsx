import React, { Dispatch, SetStateAction, useEffect, useState } from 'react';
import {
  Typography,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Button,
  TextField,
  InputLabel,
  MenuItem,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Controller, useForm } from 'react-hook-form';
import { useSelector, useDispatch } from 'react-redux';
import AddCurrency from './AddCurrency';
import { currencyApi } from '../../../Api/CurrencyApi';
import EditCurrency from './EditCurrency';
import UnsavedChanges from '../../../Dialogs/UnsavedChanges';
import { userApi } from '../../../Api/UserApi';
import { ProfileDrawerEnums } from '../../../Enums/ProfileDrawerEnums';
import { CategoryInterface } from '../../../Interfaces/Interfaces';
import Currency from '../../../Interfaces/Currency.interface';
import Task from '../../../Interfaces/Task.interface';
import User from '../../../Interfaces/User.interface';
import './Currencies.scss';
import { getDateTimeRange } from '../../../Redux/Selectors';
import { getAllBasicData } from '../../../Redux/Actions';

interface Props {
  openedAccordion: ProfileDrawerEnums;
  closeAccordions: (leftOpened: ProfileDrawerEnums) => void;
  hasUnsavedChanges: boolean;
  setHasUnsavedChanges: Dispatch<SetStateAction<boolean>>;
  userProperties?: User;
  setLoadingData: Dispatch<SetStateAction<boolean>>;
  setTasks: Dispatch<SetStateAction<Task[]>>;
  setAllCategories: Dispatch<SetStateAction<CategoryInterface[]>>;
}

export default function Currencies({
  openedAccordion,
  closeAccordions,
  hasUnsavedChanges,
  setHasUnsavedChanges,
  userProperties,
  setLoadingData,
  setTasks,
  setAllCategories,
}: Props) {
  const [addCurrency, setAddCurrency] = useState<boolean>(false);
  const [editCurrency, setEditCurrency] = useState<boolean>(false);
  const [currencies, setCurrencies] = useState<Currency[]>([]);
  const [selectedSubSection, setSelectedSubSection] = useState<string>('');
  const [isUnsavedChangesOpened, setIsUnsavedChangesOpened] = useState<boolean>(false);
  const dateRange = useSelector(getDateTimeRange);
  const dispatch = useDispatch();
  // All things that need to loaded when displaying currencies in profile drawer.

  useEffect(() => {
    currencyApi
      .getCurrencies()
      .then((res) => setCurrencies(res))
      .catch((err) => console.warn(err));
  }, []);

  const {
    handleSubmit,
    control,
    formState: { isDirty, isValid },
    reset,
  } = useForm<Currency>();

  const onSubmit = async (data: any) => {
    const editedUserAndCurrency: User = {
      username: userProperties?.username!,
      defaultCurrency: currencies.find((curr: Currency) => curr.id === data.newCurrency),
    };

    await userApi
      .updateUserProperties(editedUserAndCurrency)
      .then((res) => {
        // setUserProperties(res);
        dispatch(getAllBasicData(dateRange));
        reset(userProperties?.defaultCurrency);
      })
      .catch((err) => console.warn(err));
  };

  const handleOpen = () => {
    closeAccordions(ProfileDrawerEnums.currencies);
    if (isDirty) {
      reset();
    }
  };

  const handleClose = () => {
    if (isDirty) {
      reset();
    }
  };

  const handleSubSection = (subSection: string) => {
    setSelectedSubSection(subSection);
    if (hasUnsavedChanges) {
      setIsUnsavedChangesOpened(true);
    } else if (subSection === 'Add') {
      setEditCurrency(false);
      setAddCurrency(true);
    } else if (subSection === 'Edit') {
      setEditCurrency(true);
      setAddCurrency(false);
    }
  };

  const manageSubSection = () => {
    if (selectedSubSection === 'Add') {
      setEditCurrency(false);
      setAddCurrency(true);
    } else if (selectedSubSection === 'Edit') {
      setEditCurrency(true);
      setAddCurrency(false);
    }
  };

  return (
    <>
      <Accordion expanded={openedAccordion === ProfileDrawerEnums.currencies}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          onClick={openedAccordion === ProfileDrawerEnums.currencies ? handleClose : handleOpen}
        >
          <Typography>Currencies</Typography>
        </AccordionSummary>
        <AccordionDetails className="currencies-accordion-details">
          <form>
            <InputLabel>Select Default Currency:</InputLabel>
            <Controller
              as={
                <TextField select fullWidth size="small" disabled={addCurrency || editCurrency}>
                  {currencies.map((currency: Currency) => (
                    <MenuItem key={currency.id} value={currency.id}>
                      {currency.name}
                    </MenuItem>
                  ))}
                </TextField>
              }
              name="newCurrency"
              variant="outlined"
              control={control}
              defaultValue={userProperties?.defaultCurrency?.id}
            />
            <div className="currencies-button-container">
              <Button className="budget-confirm-button-text" onClick={() => handleSubSection('Add')}>
                Add Curency
              </Button>
              <Button className="budget-error-button-text" onClick={() => handleSubSection('Edit')}>
                Edit Curency
              </Button>
              <Button
                variant="outlined"
                color="primary"
                className="budget-confirm-button"
                onClick={handleSubmit(onSubmit)}
                disabled={!isDirty || !isValid || addCurrency || editCurrency}
              >
                Submit
              </Button>
            </div>
          </form>
          {addCurrency && (
            <AddCurrency
              setAddCurrency={setAddCurrency}
              setCurrencies={setCurrencies}
              setHasUnsavedChanges={setHasUnsavedChanges}
            />
          )}
          {editCurrency && (
            <EditCurrency
              setEditCurrency={setEditCurrency}
              currencies={currencies}
              setCurrencies={setCurrencies}
              setHasUnsavedChanges={setHasUnsavedChanges}
            />
          )}
        </AccordionDetails>
      </Accordion>
      <UnsavedChanges
        isOpened={isUnsavedChangesOpened}
        setIsOpened={setIsUnsavedChangesOpened}
        handleYes={manageSubSection}
      />
    </>
  );
}
