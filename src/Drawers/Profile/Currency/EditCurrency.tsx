import React, { Dispatch, SetStateAction, useState, useEffect } from 'react';
import { Button, TextField, InputLabel, MenuItem } from '@material-ui/core';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import Currency from '../../../Interfaces/Currency.interface';
import UnsavedChanges from '../../../Dialogs/UnsavedChanges';
import { CurrencyValidation } from '../../../Common/Validation/CurrencyValidation';

import { currencyApi } from '../../../Api/CurrencyApi';

import './EditCurrency.scss';

interface Props {
  setEditCurrency: Dispatch<SetStateAction<boolean>>;
  currencies: Currency[];
  setCurrencies: Dispatch<SetStateAction<Currency[]>>;
  setHasUnsavedChanges: Dispatch<SetStateAction<boolean>>;
}

export default function EditCurrency({
  setEditCurrency,
  currencies,
  setCurrencies,
  setHasUnsavedChanges,
}: Props) {
  const [selectedCurrency, setSelectedCurrency] = useState<Currency>({} as Currency);
  const [isUnsavedChangesOpened, setIsUnsavedChangesOpened] = useState<boolean>(false);
  const {
    handleSubmit,
    control,
    formState: { isDirty, isValid },
    reset,
  } = useForm<Currency>({
    mode: 'onChange',
    resolver: yupResolver(CurrencyValidation),
    defaultValues: {
      name: selectedCurrency.name,
      currencyCode: selectedCurrency.currencyCode,
      sign: selectedCurrency.sign,
    },
  });

  useEffect(() => {
    reset({
      name: selectedCurrency.name,
      currencyCode: selectedCurrency.currencyCode,
      sign: selectedCurrency.sign,
    });
  }, [selectedCurrency, reset]);

  const handleChangeSelectedCurrency = (evt: React.ChangeEvent<any>) => {
    const selectedCurrencies: Currency[] = currencies.filter(
      (curr: Currency) => curr.id === evt.target.value,
    );

    setSelectedCurrency(selectedCurrencies[0]);
  };

  const onSubmit = async (data: Currency) => {
    const editedCurrency: Currency = {
      id: selectedCurrency.id,
      name: data.name,
      currencyCode: data.currencyCode,
      sign: data.sign,
    };

    currencyApi
      .editCurrency(editedCurrency)
      .then((res) => {
        setCurrencies(res);
        manageClose();
      })
      .catch((err) => console.warn(err));
  };

  const handleClose = () => {
    if (isDirty) {
      setIsUnsavedChangesOpened(true);
    } else {
      manageClose();
    }
  };

  const manageClose = () => {
    setHasUnsavedChanges(false);
    setEditCurrency(false);
  };

  return (
    <>
      <InputLabel>Select Currency:</InputLabel>
      <TextField
        fullWidth
        variant="outlined"
        size="small"
        select
        onChange={(evt) => handleChangeSelectedCurrency(evt)}
      >
        {currencies.map((currency: Currency, key: number) => (
          <MenuItem key={key} value={currency.id}>
            {currency.name}
          </MenuItem>
        ))}
      </TextField>
      <form>
        <InputLabel
          className="profile-drawer-form-input-label"
          disabled={selectedCurrency.currencyCode === undefined}
        >
          Name of Currency:
        </InputLabel>
        <Controller
          as={<TextField fullWidth size="small" />}
          name="name"
          variant="outlined"
          control={control}
          disabled={selectedCurrency.name === undefined}
        />
        <InputLabel
          className="profile-drawer-form-input-label"
          disabled={selectedCurrency.currencyCode === undefined}
        >
          Sign of Currency:
        </InputLabel>
        <Controller
          as={<TextField fullWidth size="small" />}
          name="sign"
          variant="outlined"
          control={control}
          disabled={selectedCurrency.sign === undefined}
        />
        <InputLabel
          className="profile-drawer-form-input-label"
          disabled={selectedCurrency.currencyCode === undefined}
        >
          Code of Currency:
        </InputLabel>
        <Controller
          as={<TextField fullWidth size="small" />}
          name="currencyCode"
          variant="outlined"
          control={control}
          disabled={selectedCurrency.currencyCode === undefined}
        />
        <div className="edit-currency-button-container">
          <Button className="budget-error-button-text" onClick={handleClose}>
            Close
          </Button>
          <Button
            variant="outlined"
            className="budget-confirm-button"
            onClick={handleSubmit(onSubmit)}
            disabled={!isDirty || !isValid}
          >
            Submit
          </Button>
        </div>
      </form>
      <UnsavedChanges
        isOpened={isUnsavedChangesOpened}
        setIsOpened={setIsUnsavedChangesOpened}
        handleYes={manageClose}
      />
    </>
  );
}
