import { Dispatch, SetStateAction, useState, useEffect } from 'react';
import { Button, TextField, InputLabel } from '@material-ui/core';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import Currency from '../../../Interfaces/Currency.interface';
import { currencyApi } from '../../../Api/CurrencyApi';
import { CurrencyValidation } from '../../../Common/Validation/CurrencyValidation';

import UnsavedChanges from '../../../Dialogs/UnsavedChanges';

import './AddCurrency.scss';

interface Props {
  setAddCurrency: Dispatch<SetStateAction<boolean>>;
  setCurrencies: Dispatch<SetStateAction<Currency[]>>;
  setHasUnsavedChanges: Dispatch<SetStateAction<boolean>>;
}

export default function AddCurrency({ setAddCurrency, setCurrencies, setHasUnsavedChanges }: Props) {
  const [isUnsavedChangesOpened, setIsUnsavedChangesOpened] = useState<boolean>(false);
  const {
    handleSubmit,
    control,
    formState: { isDirty, isValid },
  } = useForm<Currency>({
    mode: 'onChange',
    resolver: yupResolver(CurrencyValidation),
    defaultValues: { name: '', currencyCode: '', sign: '' },
  });

  const onSubmit = async (data: Currency) => {
    const newCurrency: Currency = {
      name: data.name,
      currencyCode: data.currencyCode,
      sign: data.sign,
    };

    await currencyApi
      .addNewCurrency(newCurrency)
      .then((res) => setCurrencies(res))
      .catch((err) => console.warn(err));
  };

  const handleClose = () => {
    if (isDirty) {
      setIsUnsavedChangesOpened(true);
    } else {
      manageClose();
    }
  };

  const manageClose = () => {
    setHasUnsavedChanges(false);
    setAddCurrency(false);
  };

  useEffect(() => {
    // handle Unsaved Changes Dialog.
    if (isDirty) {
      setHasUnsavedChanges(true);
    } else {
      setHasUnsavedChanges(false);
    }
  }, [isDirty]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <>
      <form>
        <div style={{ width: '100%' }}>
          <InputLabel className="profile-drawer-form-input-label">Name of Currency:</InputLabel>
          <Controller
            as={<TextField fullWidth size="small" />}
            name="name"
            variant="outlined"
            control={control}
          />
          <InputLabel className="profile-drawer-form-input-label">Code of Currency:</InputLabel>
          <Controller
            as={<TextField fullWidth size="small" />}
            name="currencyCode"
            variant="outlined"
            control={control}
          />
          <InputLabel className="profile-drawer-form-input-label">Sign of Currency:</InputLabel>
          <Controller
            as={<TextField fullWidth size="small" />}
            name="sign"
            variant="outlined"
            control={control}
          />
          <div className="add-currency-button-container ">
            <Button className="budget-error-button-text" onClick={handleClose}>
              Close
            </Button>
            <Button
              variant="outlined"
              className="budget-confirm-button"
              onClick={handleSubmit(onSubmit)}
              disabled={!isDirty || !isValid}
            >
              Submit
            </Button>
          </div>
        </div>
      </form>
      <UnsavedChanges
        isOpened={isUnsavedChangesOpened}
        setIsOpened={setIsUnsavedChangesOpened}
        handleYes={manageClose}
      />
    </>
  );
}
