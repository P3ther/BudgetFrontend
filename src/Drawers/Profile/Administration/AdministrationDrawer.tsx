import { Typography, Accordion, AccordionSummary, AccordionDetails, Button } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { ProfileDrawerEnums } from '../../../Enums/ProfileDrawerEnums';

/* Styles */
// import './SelectTheme.scss';

interface Props {
  openedAccordion: ProfileDrawerEnums;
  closeAccordions: (leftOpened: ProfileDrawerEnums) => void;
}

export default function AdministrationDrawer(props: Props) {
  const handleOpen = () => {
    props.closeAccordions(ProfileDrawerEnums.administrationSection);
  };

  const handleClose = () => {
    props.closeAccordions(ProfileDrawerEnums.none);
  };

  return (
    <Accordion expanded={props.openedAccordion === ProfileDrawerEnums.administrationSection}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        onClick={
          props.openedAccordion === ProfileDrawerEnums.administrationSection ? handleClose : handleOpen
        }
      >
        <Typography>Administration Section:</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Button fullWidth className="budget-confirm-button-text">
          Open Administration Section
        </Button>
      </AccordionDetails>
    </Accordion>
  );
}
