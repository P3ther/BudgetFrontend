import { Dispatch, SetStateAction, useState, useEffect } from 'react';
import {
  Typography,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  InputLabel,
  TextField,
  InputAdornment,
  IconButton,
  Button,
} from '@material-ui/core';
import { yupResolver } from '@hookform/resolvers/yup';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Controller, useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { ChangePasswordValidation } from '../../../Common/Validation/ChangePasswordValidation';
import { api } from '../../../Common/Api';
import { getUser, removeUserSession } from '../../../Common/Auth';
import ChangePassword from '../../../Interfaces/ChangePassword.interface';

import { ProfileDrawerEnums } from '../../../Enums/ProfileDrawerEnums';

import './EditPassword.scss';

interface Props {
  openedAccordion: ProfileDrawerEnums;
  closeAccordions: (leftOpened: ProfileDrawerEnums) => void;
  setIsProfileDrawer: Dispatch<SetStateAction<boolean>>;
  setIsLogedin: Dispatch<SetStateAction<boolean>>;
  setHasUnsavedChanges: Dispatch<SetStateAction<boolean>>;
}

export default function EditPassword({
  openedAccordion,
  closeAccordions,
  setIsProfileDrawer,
  setIsLogedin,
  setHasUnsavedChanges,
}: Props) {
  const history = useHistory();
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [showNewPassword, setShowNewPassword] = useState<boolean>(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState<boolean>(false);

  const {
    handleSubmit,
    control,
    errors,
    formState: { isDirty, isValid },
  } = useForm<ChangePassword>({
    mode: 'onChange',
    resolver: yupResolver(ChangePasswordValidation),
    defaultValues: { password: '', newPassword: '', confirmNewPassword: '' },
  });

  const onSubmit = async (data: ChangePassword) => {
    const newPassword: ChangePassword = {
      username: getUser(),
      password: data.password,
      newPassword: data.newPassword,
    };

    await api.changePassword(newPassword).then(() => {
      removeUserSession();
      closeAccordions(ProfileDrawerEnums.none);
      setIsLogedin(false);
      handleClose();
      setIsProfileDrawer(false);
      history.push('/login/');
    });
  };

  const handleOpen = () => {
    closeAccordions(ProfileDrawerEnums.changePassword);
  };

  const handleClose = () => {
    closeAccordions(ProfileDrawerEnums.none);
  };

  useEffect(() => {
    // handle Unsaved Changes Dialog.
    if (isDirty) {
      setHasUnsavedChanges(true);
    } else {
      setHasUnsavedChanges(false);
    }
  }, [isDirty]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <>
      <Accordion expanded={openedAccordion === ProfileDrawerEnums.changePassword}>
        <AccordionSummary
          onClick={openedAccordion === ProfileDrawerEnums.changePassword ? handleClose : handleOpen}
          expandIcon={<ExpandMoreIcon />}
        >
          <Typography>Change Password</Typography>
        </AccordionSummary>
        <AccordionDetails className="edit-password-accordion-details">
          <form>
            <InputLabel>Current Password:</InputLabel>
            <Controller
              as={
                <TextField
                  fullWidth
                  size="small"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton onClick={() => setShowPassword(!showPassword)}>
                          {showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  type={showPassword ? 'text' : 'password'}
                />
              }
              name="password"
              variant="outlined"
              control={control}
              error={errors.password}
              helperText={errors.password ? errors.password.message : ''}
            />
            <InputLabel className="MarginBetweenTextFields">New Password:</InputLabel>
            <Controller
              as={
                <TextField
                  fullWidth
                  size="small"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton onClick={() => setShowNewPassword(!showNewPassword)}>
                          {showNewPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  type={showNewPassword ? 'text' : 'password'}
                />
              }
              name="newPassword"
              variant="outlined"
              control={control}
              error={errors.newPassword}
              helperText={errors.newPassword ? errors.newPassword.message : ''}
            />
            <InputLabel className="MarginBetweenTextFields">Confirm New Password:</InputLabel>
            <Controller
              as={
                <TextField
                  fullWidth
                  size="small"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton onClick={() => setShowConfirmPassword(!showConfirmPassword)}>
                          {showConfirmPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                  type={showConfirmPassword ? 'text' : 'password'}
                />
              }
              name="confirmNewPassword"
              variant="outlined"
              control={control}
              error={errors.confirmNewPassword}
              helperText={errors.confirmNewPassword ? errors.confirmNewPassword.message : ''}
            />
            <div className="edit-password-button-container">
              <Button
                variant="outlined"
                className="budget-confirm-button"
                onClick={handleSubmit(onSubmit)}
                disabled={!isDirty || !isValid}
              >
                Submit
              </Button>
            </div>
          </form>
        </AccordionDetails>
      </Accordion>
    </>
  );
}
