import React, { Dispatch, SetStateAction, useState } from 'react';
import { Typography, Drawer, Button, Grid } from '@material-ui/core';
import { useSelector } from 'react-redux';
import Currencies from './Currency/Currencies';
import ExchangeRates from './ExchangeRates/ExchangeRates';
import SelectTheme from './Theme/SelectTheme';
import Theme from '../../Interfaces/Theme.interface';
import EditPassword from './Password/EditPassword';
import UnsavedChanges from '../../Dialogs/UnsavedChanges';
import Task from '../../Interfaces/Task.interface';

import './Profile.scss';
import { UserRole } from '../../Enums/UserRole';
import AdministrationDrawer from './Administration/AdministrationDrawer';
import { ProfileDrawerEnums } from '../../Enums/ProfileDrawerEnums';
import { CategoryInterface } from '../../Interfaces/Interfaces';
import { getUserProperties } from '../../Redux/Selectors';

interface Props {
  isProfileDrawer: boolean;
  setIsProfileDrawer: Dispatch<SetStateAction<boolean>>;
  userTheme: Theme;
  setUserTheme: Dispatch<SetStateAction<Theme>>;
  setIsLogedin: Dispatch<SetStateAction<boolean>>;
  setLoadingData: Dispatch<SetStateAction<boolean>>;
  setTasks: Dispatch<SetStateAction<Task[]>>;
  setAllCategories: Dispatch<SetStateAction<CategoryInterface[]>>;
}

export default function Profile({
  isProfileDrawer,
  setIsProfileDrawer,
  userTheme,
  setUserTheme,
  setIsLogedin,
  setLoadingData,
  setTasks,
  setAllCategories,
}: Props) {
  const [hasUnsavedChanges, setHasUnsavedChanges] = useState<boolean>(false);
  const [isUnsavedChangesOpened, setIsUnsavedChangesOpened] = useState<boolean>(false);
  const [openedAccordion, setOpenedAccordion] = useState<ProfileDrawerEnums>(ProfileDrawerEnums.none);
  const userProperties = useSelector(getUserProperties);
  const closeAccordions = (leftOpened: ProfileDrawerEnums) => {
    setOpenedAccordion(leftOpened);
  };

  const handleClose = () => {
    if (hasUnsavedChanges) {
      setIsUnsavedChangesOpened(true);
    } else {
      setOpenedAccordion(ProfileDrawerEnums.none);
      setIsProfileDrawer(false);
    }
  };

  const handleCloseUnsavedChanges = () => {
    setOpenedAccordion(ProfileDrawerEnums.none);
    setIsProfileDrawer(false);
  };

  return (
    <Drawer
      variant="temporary"
      anchor="right"
      open={isProfileDrawer}
      onClose={handleClose}
      PaperProps={{ className: 'profile-drawer' }}
    >
      <Grid container>
        <Grid item xs={8}>
          <Typography variant="h4" className="budget-header-h4">
            User Profile
          </Typography>
          <Typography
            variant="h6"
            style={{
              width: '70%',
              margin: '1vh',
              marginTop: '0px',
              fontSize: '1rem',
            }}
          >
            <div>Logged In: {userProperties?.username}</div>
            <div>Default Currency: {userProperties?.defaultCurrency?.name}</div>
          </Typography >
        </Grid >
        <Grid item xs={4} className="profile-close-button-container">
          <Button
            variant="outlined"
            className="budget-confirm-button profile-close-button"
            onClick={handleClose}
          >
            Close
          </Button>
        </Grid>
      </Grid >
      <EditPassword
        openedAccordion={openedAccordion}
        closeAccordions={closeAccordions}
        setIsProfileDrawer={setIsProfileDrawer}
        setIsLogedin={setIsLogedin}
        setHasUnsavedChanges={setHasUnsavedChanges}
      />
      <Currencies
        openedAccordion={openedAccordion}
        closeAccordions={closeAccordions}
        hasUnsavedChanges={hasUnsavedChanges}
        setHasUnsavedChanges={setHasUnsavedChanges}
        userProperties={userProperties}
        setLoadingData={setLoadingData}
        setTasks={setTasks}
        setAllCategories={setAllCategories}
      />
      <ExchangeRates openedAccordion={openedAccordion} closeAccordions={closeAccordions} />
      <SelectTheme
        userTheme={userTheme}
        setUserTheme={setUserTheme}
        openedAccordion={openedAccordion}
        closeAccordions={closeAccordions}
      />
      {
        userProperties?.userRole === UserRole.Admin && (
          <AdministrationDrawer openedAccordion={openedAccordion} closeAccordions={closeAccordions} />
        )
      }
      <UnsavedChanges
        isOpened={isUnsavedChangesOpened}
        setIsOpened={setIsUnsavedChangesOpened}
        handleYes={handleCloseUnsavedChanges}
      />
    </Drawer >
  );
}
