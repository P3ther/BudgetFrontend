import { useEffect, useState } from 'react';
import {
  Typography,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Tooltip,
  Button,
} from '@material-ui/core';
import PriorityHighIcon from '@material-ui/icons/PriorityHigh';
import DoneIcon from '@material-ui/icons/Done';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { currencyPairsApi } from '../../../Api/CurrencyPairsApi';
import CurrencyPairs from '../../../Interfaces/CurrencyPairs.interface';
import { ProfileDrawerEnums } from '../../../Enums/ProfileDrawerEnums';

interface Props {
  openedAccordion: ProfileDrawerEnums;
  closeAccordions: (leftOpened: ProfileDrawerEnums) => void;
}

export default function ExchangeRates(props: Props) {
  const currentDate: Date = new Date();
  const [currencyPairs, setCurrencyPairs] = useState<CurrencyPairs[]>([]);

  useEffect(() => {
    currencyPairsApi
      .getCurrencyPairs()
      .then((res) => setCurrencyPairs(res))
      .catch((err) => console.warn(err));
  }, []);

  interface ListItemSuccessfulProps {
    text: string;
    lastChanged: Date;
  }

  const getLatestExchangeRates = () => {
    currencyPairsApi.updateExchangeRates().catch((err) => console.warn(err));
  };

  const ListItemSuccessful = ({ text, lastChanged }: ListItemSuccessfulProps) => (
    <ListItem>
      <Tooltip arrow title={`Last changed: ${lastChanged}`}>
        <ListItemIcon style={{ minWidth: '25px' }}>
          <DoneIcon style={{ color: '#90be6d' }} />
        </ListItemIcon>
      </Tooltip>
      <ListItemText primary={text} style={{ color: '#90be6d' }} />
    </ListItem>
  );

  interface ListItemWithErrorProps {
    text: string;
    lastChanged: Date;
  }

  const ListItemWithError = ({ text, lastChanged }: ListItemWithErrorProps) => (
    <ListItem>
      <Tooltip arrow title={`Last changed: ${lastChanged}`}>
        <ListItemIcon style={{ minWidth: '25px' }}>
          <PriorityHighIcon style={{ color: '#f50057' }} />
        </ListItemIcon>
      </Tooltip>
      <ListItemText primary={text} style={{ color: '#f50057' }} />
    </ListItem>
  );

  const handleOpen = () => {
    props.closeAccordions(ProfileDrawerEnums.exchangeRates);
  };

  const handleClose = () => {
    props.closeAccordions(ProfileDrawerEnums.none);
  };

  return (
    <Accordion expanded={props.openedAccordion === ProfileDrawerEnums.exchangeRates}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        onClick={
          (props.openedAccordion === ProfileDrawerEnums.exchangeRates) === true ? handleClose : handleOpen
        }
      >
        <Typography>Exchange Rates:</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <List className="exchange-rates-list">
          <div>
            {currencyPairs.map((pair: CurrencyPairs, key: number) =>
              pair.lastChange > currentDate ? (
                <ListItemWithError
                  text={`${pair.firstCurrencyCode} ${String.fromCharCode(8594)} ${pair.secondCurrencyCode
                    } is: ${pair.exchangeRate}`}
                  lastChanged={pair.lastChange}
                />
              ) : (
                <ListItemSuccessful
                  text={`${pair.firstCurrencyCode} ${String.fromCharCode(8594)} ${pair.secondCurrencyCode
                    } is: ${pair.exchangeRate}`}
                  lastChanged={pair.lastChange}
                />
              ),
            )}
          </div>
          <Button onClick={getLatestExchangeRates} fullWidth className="budget-confirm-button-text">
            Get Latest Exchange Rates.
          </Button>
        </List>
      </AccordionDetails>
    </Accordion>
  );
}
