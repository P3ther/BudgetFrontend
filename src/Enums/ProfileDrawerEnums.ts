export enum ProfileDrawerEnums {
  none = 'none',
  changePassword = 'changePassword',
  currencies = 'currencies',
  exchangeRates = 'exchangeRates',
  selectYourTheme = 'selectYourTheme',
  administrationSection = 'administrationSection',
}
