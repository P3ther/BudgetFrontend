export enum IngredientUnit {
  Kilogram = 'Kilogram',
  Gram = 'Gram',
  Liter = 'Liter',
  Pcs = 'Pcs',
  Mililiters = 'Mililiters',
}

export const ingredientUnits: string[] = ['Kilogram', 'Gram', 'Liter', 'Pcs', 'Mililiters'];
