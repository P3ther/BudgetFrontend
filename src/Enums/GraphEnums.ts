export enum GraphTimeValues {
  YTD = 'YTD',
  Month1 = 'Month1',
  Months3 = 'Months3',
  Months6 = 'Months6',
  Year = 'Year',
}
