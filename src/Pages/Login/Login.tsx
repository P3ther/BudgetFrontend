import React, { FC, useState } from 'react';
import { TextField, InputLabel, Button, InputAdornment, IconButton, FormHelperText } from '@material-ui/core';
import { useForm, Controller } from 'react-hook-form';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';
import User from '../../Interfaces/User.interface';
import { UserValidation } from '../../Common/Validation/UserValidation';
import AuthContainer from '../../Components/AuthContainer/AuthContainer';
import { authenticate } from '../../Redux/Auth/auth.actions';
import { getAuthenticationError } from '../../Redux/Auth/auth.selectors';
import './Login.scss';

export const LoginPageComponent: FC = () => {
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const history = useHistory();
  const dispatch = useDispatch();
  const authError = useSelector(getAuthenticationError);
  const {
    handleSubmit,
    control,
    errors,

    formState: { isDirty, isValid, isSubmitting },
  } = useForm<User>({
    mode: 'onBlur',
    resolver: yupResolver(UserValidation),
  });

  const doLogin = async (data: User) => {
    dispatch(
      authenticate({
        username: data.username,
        password: data.password,
      }),
    );
    history.push('/');
  };

  return (
    <>
      <form onSubmit={handleSubmit(doLogin)}>
        <InputLabel className="login-input-label" error={!!errors.username || !!authError}>
          Username
        </InputLabel>
        <Controller
          as={
            <TextField
              size="small"
              className="login-text-field"
              InputProps={{ className: 'login-text-field-input' }}
              data-testid="username-input-field"
            />
          }
          name="username"
          variant="outlined"
          control={control}
          disabled={isSubmitting}
          defaultValue=""
          error={!!authError}
        />
        {authError && (
          <FormHelperText
            className="username-input-error"
            error={!!authError}
            data-testid="username-input-error"
          >
            {authError}
          </FormHelperText>
        )}
        <InputLabel className="login-input-label" error={!!errors.password || !!authError}>
          Password
        </InputLabel>
        <Controller
          as={
            <TextField
              size="small"
              className="login-text-field"
              InputProps={{
                className: 'login-text-field-input',
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton onClick={() => setShowPassword(!showPassword)} disabled={isSubmitting}>
                      {showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              type={showPassword ? 'text' : 'password'}
              data-testid="password-input-field"
            />
          }
          name="password"
          variant="outlined"
          control={control}
          disabled={isSubmitting}
          defaultValue=""
          error={!!authError}
        />
        <p className="login-button-container">
          <Button
            variant="outlined"
            type="submit"
            className="budget-confirm-button"
            disabled={!isDirty || !isValid || isSubmitting}
            data-testid="login-button"
          >
            {isSubmitting ? 'Signing in' : 'Login'}
          </Button>
        </p>
      </form>
    </>
  );
};

export default () => (
  <AuthContainer headerText="Log in to use the app">
    <LoginPageComponent />
  </AuthContainer>
);
