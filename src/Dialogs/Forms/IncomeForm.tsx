import { InputLabel, TextField, Select, MenuItem, FormHelperText, Grid } from '@material-ui/core';
import { Controller, Control, DeepMap, FieldError } from 'react-hook-form';
import moment from 'moment';
import { useMemo } from 'react';
import Income from '../../Interfaces/Income.interface';
import Currency from '../../Interfaces/Currency.interface';
import { CategoryInterface } from '../../Interfaces/Interfaces';

interface Props {
  control: Control<Income>;
  errors: DeepMap<Income, FieldError>;
  categories: CategoryInterface[];
  income?: Income;
  currencies: Currency[];
}
export default function IncomeForm({ control, errors, categories, income, currencies }: Props) {
  const gridSize = useMemo(() => {
    if (window.screen.width <= 480) return 12;

    return 6;
  }, []);

  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={gridSize}>
          <InputLabel error={!!errors.name}>Name of the Income</InputLabel>
          <Controller
            as={<TextField fullWidth size="small" />}
            name="name"
            variant="outlined"
            control={control}
            defaultValue={income?.name}
            error={errors.name}
            helperText={errors.name ? errors.name.message : ''}
          />
        </Grid>
        <Grid item xs={gridSize}>
          <InputLabel error={!!errors.category}>Category</InputLabel>
          <Controller
            as={
              <Select fullWidth style={{ height: '40px' }}>
                {categories?.map((option: CategoryInterface) => (
                  <MenuItem value={option.id}>{option.name}</MenuItem>
                ))}
              </Select>
            }
            name="category"
            error={errors.category}
            control={control}
            variant="outlined"
            defaultValue={income?.category}
          />
          {errors.category ? (
            <FormHelperText error style={{ marginLeft: '14px' }}>
              {errors.category.message}
            </FormHelperText>
          ) : (
            ''
          )}
        </Grid>
      </Grid>
      <InputLabel style={{ marginTop: '10px' }} error={!!errors.date}>
        Date of the Expense
      </InputLabel>
      <Controller
        as={<TextField fullWidth type="datetime-local" size="small" />}
        name="date"
        variant="outlined"
        control={control}
        defaultValue={
          income?.date
            ? moment(income.date).utc(true).local().format('YYYY-MM-DDTHH:mm')
            : moment(new Date()).format('YYYY-MM-DDTHH:mm')
        }
        error={errors.date}
      />
      {errors.date ? (
        <FormHelperText error style={{ marginLeft: '14px' }}>
          {errors.date.message}
        </FormHelperText>
      ) : (
        ''
      )}
      <Grid container spacing={2}>
        <Grid item xs={gridSize}>
          <InputLabel style={{ marginTop: '10px' }} error={!!errors.price}>
            Price of the Income
          </InputLabel>
          <Controller
            as={<TextField fullWidth size="small" />}
            name="price"
            variant="outlined"
            control={control}
            type="number"
            error={errors.price}
            helperText={errors.price ? errors.price.message : ''}
            defaultValue={income?.price}
          />
        </Grid>
        <Grid item xs={gridSize}>
          <InputLabel style={{ marginTop: '10px' }} error={!!errors.currencyId}>
            Currency of the Income
          </InputLabel>
          <Controller
            as={
              <TextField fullWidth select size="small">
                {currencies.map((currency: Currency, key: number) => (
                  <MenuItem key={key} value={currency.id}>
                    {`${currency.name} (${currency.sign})`}
                  </MenuItem>
                ))}
              </TextField>
            }
            name="currencyId"
            variant="outlined"
            control={control}
            type="number"
            error={errors.currencyId}
            helperText={errors.currencyId ? errors.currencyId.message : ''}
            defaultValue={income?.currencyId}
          />
        </Grid>
      </Grid>
    </>
  );
}
