import React, { FC } from 'react';
import { Button } from '@material-ui/core';
import './BudgetDialog.scss';

enum DateOptions {
  thirtyDays = 'thirtyDays',
  month = 'month',
  threeMonths = 'threeMonths',
  sixMonths = 'sixMonths',
  year = 'year',
  ytd = 'ytd',
  custom = 'custom',
}

interface Props {
  isMobile: () => boolean;
  handleSelectDateRange: (arg0: DateOptions) => void;
  selectedDateRange: DateOptions;
}
const BudgetDialogButtons: FC<Props> = ({ isMobile, handleSelectDateRange, selectedDateRange }) => {
  const fullWidth = isMobile();

  return (
    <>
      <Button
        variant="outlined"
        onClick={() => handleSelectDateRange(DateOptions.month)}
        fullWidth={fullWidth}
        className={
          selectedDateRange === DateOptions.month ? 'budget-confirm-button' : 'budget-confirm-button-outlined'
        }
      >
        1 Month
      </Button>
      <Button
        variant="outlined"
        onClick={() => handleSelectDateRange(DateOptions.thirtyDays)}
        fullWidth={fullWidth}
        className={
          selectedDateRange === DateOptions.thirtyDays
            ? 'budget-confirm-button'
            : 'budget-confirm-button-outlined'
        }
      >
        30 Days
      </Button>
      <Button
        variant="outlined"
        onClick={() => handleSelectDateRange(DateOptions.threeMonths)}
        fullWidth={fullWidth}
        className={
          selectedDateRange === DateOptions.threeMonths
            ? 'budget-confirm-button'
            : 'budget-confirm-button-outlined'
        }
      >
        3 Months
      </Button>
      <Button
        variant="outlined"
        onClick={() => handleSelectDateRange(DateOptions.sixMonths)}
        fullWidth={fullWidth}
        className={
          selectedDateRange === DateOptions.sixMonths
            ? 'budget-confirm-button'
            : 'budget-confirm-button-outlined'
        }
      >
        6 Months
      </Button>
      <Button
        variant="outlined"
        onClick={() => handleSelectDateRange(DateOptions.year)}
        fullWidth={fullWidth}
        className={
          selectedDateRange === DateOptions.year ? 'budget-confirm-button' : 'budget-confirm-button-outlined'
        }
      >
        1 Year
      </Button>
      <Button
        variant="outlined"
        onClick={() => handleSelectDateRange(DateOptions.ytd)}
        fullWidth={fullWidth}
        className={
          selectedDateRange === DateOptions.ytd ? 'budget-confirm-button' : 'budget-confirm-button-outlined'
        }
      >
        YTD
      </Button>
      <Button
        disabled
        variant="outlined"
        onClick={() => handleSelectDateRange(DateOptions.ytd)}
        fullWidth={fullWidth}
        className={
          selectedDateRange === DateOptions.custom
            ? 'budget-confirm-button'
            : 'budget-confirm-button-outlined'
        }
      >
        Custom
      </Button>
    </>
  );
};

export default BudgetDialogButtons;
