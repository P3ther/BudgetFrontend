import React, { Dispatch, SetStateAction, useState } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  FormControlLabel,
  Checkbox,
} from '@material-ui/core';

import './Styles/ConfirmDeleteCategory.scss';
import { categoryApi } from '../Api/CategoryApi';
/* InterFaces */
import { CategoryInterface } from '../Interfaces/Interfaces';

interface Props {
  isOpened: boolean;
  setIsOpened: Dispatch<SetStateAction<boolean>>;
  deletedCategory: CategoryInterface;
}

export default function ConfirmDeleteCategory({ isOpened, setIsOpened, deletedCategory }: Props) {
  const [deleteAll, setDeleteAll] = useState<boolean>(false);
  const handleDeleteCheckbox = (evt: React.ChangeEvent<any>) => {
    setDeleteAll(evt.target.checked);
  };

  const handleClose = () => {
    setIsOpened(false);
    setDeleteAll(false);
  };

  const handleDelete = () => {
    if (deleteAll) {
      categoryApi.deleteCategoryAndAllItems(deletedCategory.id).catch((err) => console.warn(err));
    } else {
      categoryApi.deleteCategory(deletedCategory.id!).catch((err) => console.warn(err));
    }
  };

  return (
    <Dialog open={isOpened} fullWidth maxWidth="xs">
      <DialogTitle>Confirm deletion of {deletedCategory.name} category</DialogTitle>
      <DialogContent>
        <DialogContentText>
          <div className="dialog-content-text-div">
            Are you sure you want to delete this category? This operation cannot be undone
          </div>
          <FormControlLabel
            control={<Checkbox checked={deleteAll} />}
            label="Delete all items assigned to this category"
            onChange={(evt) => handleDeleteCheckbox(evt)}
          />
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button variant="outlined" onClick={handleClose} className="budget-confirm-button">
          No
        </Button>
        <Button variant="outlined" onClick={() => handleDelete()} className="budget-error-button">
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  );
}
