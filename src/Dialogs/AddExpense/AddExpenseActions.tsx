import React, { Dispatch, FC, SetStateAction } from 'react';
import { DialogActions, Button } from '@mui/material';
import './AddExpense.scss';
import QrCodeScannerIcon from '@mui/icons-material/QrCodeScanner';
import IconButton from '@mui/material/IconButton';
import { UseFormMethods } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { addExpenseAction } from '../../Redux/Actions';
import Expense, { AddExpense } from '../../Interfaces/Expense.interface';
import { changeAddExpenseDialog } from '../../Redux/App/app.action';

interface AddExpenseActionsProps {
  isQrCodeScannerVisible: boolean;
  setIsQrCodeScannerVisible: Dispatch<SetStateAction<boolean>>;
  formState: UseFormMethods<Expense>;
}

const AddExpenseActions: FC<AddExpenseActionsProps> = ({
  isQrCodeScannerVisible,
  setIsQrCodeScannerVisible,
  formState,
}: AddExpenseActionsProps) => {
  const dispatch = useDispatch();

  const handleClose = () => {
    if (isQrCodeScannerVisible) {
      setIsQrCodeScannerVisible(false);
    } else {
      dispatch(changeAddExpenseDialog());
    }
  };

  const onSubmit = async (data: Expense) => {
    const newExpense: AddExpense = {
      name: data.name,
      date: new Date(data.date),
      price: data.price,
      addressToReceipt: '',
      category: data.category,
      categoryName: '',
      currencyId: data.currencyId,
      currencySign: '',
    };

    dispatch(addExpenseAction(newExpense));
  };

  return (
    <DialogActions className="base-dialog-actions add-expense-actions">
      <span>
        {!isQrCodeScannerVisible && (
          <IconButton onClick={() => setIsQrCodeScannerVisible(true)}>
            <QrCodeScannerIcon />
          </IconButton>
        )}
      </span>
      <span>
        <Button onClick={handleClose} color="error">
          Cancel
        </Button>
        {!isQrCodeScannerVisible && (
          <Button onClick={formState.handleSubmit(onSubmit)} color="success">
            Submit
          </Button>
        )}
      </span>
    </DialogActions>
  );
};

export default AddExpenseActions;
