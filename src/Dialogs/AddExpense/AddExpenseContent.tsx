import React, { Dispatch, FC, SetStateAction, useMemo, useState } from 'react';
import { Grid } from '@mui/material';
import './AddExpense.scss';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import { FormProvider, UseFormMethods } from 'react-hook-form';
import { useSelector } from 'react-redux';
import Currency from '../../Interfaces/Currency.interface';
import Expense, { AddExpense } from '../../Interfaces/Expense.interface';
import QrCodeScanner from '../QrCodeScanner/QrCodeScanner';
import BudgetTextField from '../../Common/Inputs/BudgetTextField/BudgetTextField';
import BudgetSelect from '../../Common/Inputs/BudgetSelect/BudgetSelect';
import { getCategories, getCurrencies, getUserProperties } from '../../Redux/Selectors';
import { Category } from '../../Interfaces/Category.interface';
import BudgetDateTimePicker from '../../Common/Inputs/BudgetDateTimePicker/BudgetDateTimePicker';

interface AddExpenseContentProps {
  isQrCodeScannerVisible: boolean;
  setIsQrCodeScannerVisible: Dispatch<SetStateAction<boolean>>;
  formState: UseFormMethods<Expense>;
}

const AddExpenseContent: FC<AddExpenseContentProps> = ({
  isQrCodeScannerVisible,
  setIsQrCodeScannerVisible,
  formState,
}: AddExpenseContentProps) => {
  const [expense, setExpense] = useState<AddExpense>();
  const categories = useSelector(getCategories);
  const currencies = useSelector(getCurrencies);
  const userProperties = useSelector(getUserProperties);
  const options = categories.map((category: Category) => ({
    id: category.id.toString(),
    name: category.name,
  }));
  const gridSize = useMemo(() => {
    if (window.screen.width <= 480) return 12;

    return 6;
  }, []);

  if (isQrCodeScannerVisible) {
    return <QrCodeScanner setIsQrCodeScannerVisible={setIsQrCodeScannerVisible} setExpense={setExpense} />;
  }

  return (
    <DialogContent className="base-dialog-body">
      <DialogContentText>
        <FormProvider {...formState}>
          <form>
            <Grid container spacing={2}>
              <Grid item xs={gridSize}>
                <BudgetTextField elementName="name" title="Name of the Expense" fullWidth defaultValue={expense?.name} />
              </Grid>
              <Grid item xs={gridSize}>
                <BudgetSelect
                  options={options}
                  defaultValue={categories[0].id.toString()}
                  elementName="category"
                  fullWidth
                  title="Category"
                />
              </Grid>
              <Grid item xs={12}>
                <BudgetDateTimePicker
                  title="Date of the Expense"
                  elementName="date"
                  defaultValue={expense?.date ? expense?.date : new Date()}
                  fullWidth
                />
              </Grid>
              <Grid item xs={gridSize}>
                <BudgetTextField elementName="price" title="Price" fullWidth type="number" defaultValue={expense?.price.toString()} />
              </Grid>
              <Grid item xs={gridSize}>
                <BudgetSelect
                  options={currencies.map((currency: Currency) => ({
                    id: currency.id?.toString() || '',
                    name: `${currency.name} (${currency.sign})`,
                  }))}
                  defaultValue={userProperties?.defaultCurrency?.id?.toString()}
                  fullWidth
                  elementName="currencyId"
                  title="Currency"
                />
              </Grid>
            </Grid>
          </form>
        </FormProvider>
      </DialogContentText>
    </DialogContent>
  );
};

export default AddExpenseContent;
