import { number, object, string, date } from 'yup';

const parseToDate = (date: string) => new Date(date);

export const ExpenseValidation = object().shape({
  name: string().required('Name is Required').max(50, 'String is too long.'),
  date: date().transform(parseToDate).typeError('Date is Required').required('Date is Required'),
  category: number().required('Category is Required'),
  price: number()
    .required('Price is Required')
    .typeError('Price has to be a number')
    .positive('Price cannot be negative'),
  currencyId: number().required('Currency is Required'),
});
