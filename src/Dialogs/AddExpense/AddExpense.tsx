import React, { FC, useState } from 'react';
import { Dialog, DialogTitle } from '@mui/material';
import './AddExpense.scss';
import IconButton from '@mui/material/IconButton';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { yupResolver } from '@hookform/resolvers/yup';
import CloseIcon from '@mui/icons-material/Close';
import { ExpenseValidation } from './AddExpense.validation';
import Expense from '../../Interfaces/Expense.interface';
import AddExpenseContent from './AddExpenseContent';
import AddExpenseActions from './AddExpenseActions';
import { getAddExpense } from '../../Redux/App/app.selectors';
import { changeAddExpenseDialog } from '../../Redux/App/app.action';

const AddExpense: FC = () => {
  const dispatch = useDispatch();
  const [isQrCodeScannerVisible, setIsQrCodeScannerVisible] = useState(false);
  const isOpened = useSelector(getAddExpense);

  const formState = useForm<Expense>({
    mode: 'onChange',
    resolver: yupResolver(ExpenseValidation),
  });

  const handleClose = () => {
    setIsQrCodeScannerVisible(false);
    dispatch(changeAddExpenseDialog());
  };

  return (
    <Dialog open={isOpened} onClose={() => { }} fullWidth>
      <DialogTitle sx={{ m: 0, p: 2 }} className="base-dialog-title">
        Add Expense
        <IconButton onClick={handleClose} className="base-dialog-title-close-button">
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <AddExpenseContent
        isQrCodeScannerVisible={isQrCodeScannerVisible}
        setIsQrCodeScannerVisible={setIsQrCodeScannerVisible}
        formState={formState}
      />
      <AddExpenseActions
        isQrCodeScannerVisible={isQrCodeScannerVisible}
        setIsQrCodeScannerVisible={setIsQrCodeScannerVisible}
        formState={formState}
      />
    </Dialog>
  );
};

export default AddExpense;
