import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@material-ui/core';

interface Props {
  isConfirmDeleteDialogOpened: boolean;
  setIsConfirmDeleteDialogOpened: Function;
  doDelete: Function;
  deletedName: string;
}

export default function ConfirmDelete({
  isConfirmDeleteDialogOpened,
  setIsConfirmDeleteDialogOpened,
  doDelete,
  deletedName,
}: Props) {
  return (
    <Dialog open={isConfirmDeleteDialogOpened} fullWidth maxWidth="xs">
      <DialogTitle>Delete {deletedName}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Are you sure you want to delete this {deletedName}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          color="secondary"
          onClick={() => setIsConfirmDeleteDialogOpened(false)}
          style={{ backgroundColor: '#90be6d' }}
        >
          No
        </Button>
        <Button
          variant="contained"
          color="secondary"
          onClick={() => doDelete()}
          style={{ backgroundColor: '#f94144' }}
        >
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  );
}
