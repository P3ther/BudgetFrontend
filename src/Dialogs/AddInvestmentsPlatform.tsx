import { Dispatch, SetStateAction, useState } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  InputLabel,
  TextField,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
} from '@material-ui/core';
import { useForm, Controller } from 'react-hook-form';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import InvestmentPlatform from '../Interfaces/InvestmentPlatform.interface';
import { investmentPlatformApi } from '../Api/InvestmentPlatformApi';

interface Props {
  isOpened: boolean;
  setIsOpened: Dispatch<SetStateAction<boolean>>;
  handleLoadInvestmentPlatforms: () => void;
  platforms: InvestmentPlatform[];
}

export default function AddInvestmentsPlatform({
  isOpened,
  setIsOpened,
  handleLoadInvestmentPlatforms,
  platforms,
}: Props) {
  const [isEdited, setIsEdited] = useState<boolean>(false);
  const [isAdding, setIsAdding] = useState<boolean>(false);
  const [editedInvestmentPlatform, setEditedInvestmentPlatform] = useState<number>(-1);

  const handleClose = () => {
    setIsOpened(false);
  };

  const handleEdit = (id: number) => {
    setEditedInvestmentPlatform(id);
    setIsEdited(true);
  };
  const handleExit = () => {
    setIsAdding(false);
    setIsEdited(false);
    setEditedInvestmentPlatform(-1);
  };

  const handleDelete = async (id: number) => {
    await investmentPlatformApi.deleteInvestmentPlatform(platforms[id].id);
    setIsOpened(false);
  };

  const { handleSubmit, control, errors } = useForm<InvestmentPlatform>();

  const onSubmit = async (data: InvestmentPlatform) => {
    const newPlatform: InvestmentPlatform = { id: -1, name: data.name, investmentsOnPlatform: [] };

    await investmentPlatformApi.createInvestmentPlatform(newPlatform).then((res) => {
      handleLoadInvestmentPlatforms();
      setIsOpened(false);
    });
  };

  const onEdit = async (data: InvestmentPlatform) => {
    const newPlatform: InvestmentPlatform = {
      id: platforms[editedInvestmentPlatform].id,
      name: data.name,
      investmentsOnPlatform: [],
    };

    await investmentPlatformApi.updateInvestmentPlatform(newPlatform).then((res) => {
      handleLoadInvestmentPlatforms();
      setIsOpened(false);
    });
  };

  return (
    <Dialog
      open={isOpened}
      onClose={handleClose}
      onExited={handleExit}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      fullWidth
      maxWidth="sm"
    >
      <DialogTitle>Add investment Platform</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description" />
        <List>
          {platforms.map((category: InvestmentPlatform, key: number) => (
            <ListItem style={{ borderStyle: 'solid', border: '1px', borderColor: '#000000' }}>
              <ListItemText primary={category.name} />
              <ListItemSecondaryAction>
                <IconButton style={{ padding: '5px' }}>
                  <EditIcon onClick={() => handleEdit(key)} />
                </IconButton>
                <IconButton style={{ padding: '5px' }}>
                  <DeleteIcon onClick={() => handleDelete(key)} />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="secondary">
          Close
        </Button>
        <Button variant="contained" color="primary" title="Submit" onClick={() => setIsAdding(true)}>
          Add investment
        </Button>
      </DialogActions>
      {isEdited && (
        <>
          <DialogContent>
            <form>
              <InputLabel style={{ marginTop: '10px' }}>Name of platform</InputLabel>
              <Controller
                as={<TextField fullWidth size="small" />}
                name="name"
                variant="outlined"
                control={control}
                defaultValue={platforms[editedInvestmentPlatform].name}
                error={errors.name}
                helperText={errors.name ? errors.name.message : ''}
              />
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => setIsEdited(false)} color="secondary">
              Back
            </Button>
            <Button
              variant="contained"
              color="primary"
              title="Submit"
              onClick={handleSubmit(onEdit)}
              style={{ backgroundColor: '#90be6d' }}
            >
              Submit
            </Button>
          </DialogActions>
        </>
      )}
      {isAdding && (
        <>
          <DialogContent>
            <form>
              <InputLabel style={{ marginTop: '10px' }}>Name of new platform</InputLabel>
              <Controller
                as={<TextField fullWidth size="small" />}
                name="name"
                variant="outlined"
                control={control}
                error={errors.name}
                helperText={errors.name ? errors.name.message : ''}
              />
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => setIsAdding(false)} color="secondary">
              Back
            </Button>
            <Button
              variant="contained"
              color="primary"
              title="Submit"
              onClick={handleSubmit(onSubmit)}
              style={{ backgroundColor: '#90be6d' }}
            >
              Submit
            </Button>
          </DialogActions>
        </>
      )}
    </Dialog>
  );
}
