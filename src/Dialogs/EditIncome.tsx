import { useEffect, useState } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  CircularProgress,
} from '@material-ui/core';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { api } from '../Common/Api';
import Income from '../Interfaces/Income.interface';
import { IncomeValidation } from '../Common/Validation/IncomeValidation';
import IncomeForm from './Forms/IncomeForm';
import DateTimeRange from '../Interfaces/StartEndTime.interface';
import ConfirmDelete from './ConfirmDelete';
import Currency from '../Interfaces/Currency.interface';
import { CategoryInterface } from '../Interfaces/Interfaces';

interface Props {
  isEditIncomeDialogOpened: boolean;
  setIsEditIncomeDialogOpened: Function;
  income: Income;
  defaultTimes: DateTimeRange;
  allCategories: CategoryInterface[];
  currencies: Currency[];
}

export default function EditIncome({
  isEditIncomeDialogOpened,
  setIsEditIncomeDialogOpened,
  income,
  defaultTimes,
  allCategories,
  currencies,
}: Props) {
  const [isConfirmDeleteDialogOpened, setIsConfirmDeleteDialogOpened] = useState<boolean>(false);
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
  const [incomeCategories, setIncomeCategories] = useState<CategoryInterface[]>([]);

  useEffect(() => {
    setIncomeCategories(allCategories.filter((category: CategoryInterface) => category.type === 1));
  }, [allCategories]);

  const { handleSubmit, control, errors } = useForm<Income>({
    mode: 'onChange',
    resolver: yupResolver(IncomeValidation),
  });

  const onSubmit = async (data: Income) => {
    const newDate: Date = new Date(data.date);

    setIsSubmitting(true);
    const editedIncome: Income = {
      id: income.id,
      name: data.name,
      date: newDate,
      price: data.price,
      addressToReceipt: '',
      category: data.category,
      categoryName: '',
      currencyId: data.currencyId,
      currencySign: '',
    };

    await api
      .updateIncome(editedIncome)
      .then(() => {
        setIsEditIncomeDialogOpened(false);
      })
      .catch((err) => console.warn(err));
    setIsSubmitting(false);
  };
  const doDelete = async () => {
    await api.deleteIncome(income);
    setIsConfirmDeleteDialogOpened(false);
    setIsEditIncomeDialogOpened(false);
  };

  return (
    <>
      <ConfirmDelete
        isConfirmDeleteDialogOpened={isConfirmDeleteDialogOpened}
        setIsConfirmDeleteDialogOpened={setIsConfirmDeleteDialogOpened}
        doDelete={doDelete}
        deletedName="Income"
      />
      <Dialog open={isEditIncomeDialogOpened} fullWidth maxWidth="sm">
        <DialogTitle>Edit Income</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <form>
              <IncomeForm
                control={control}
                errors={errors}
                categories={incomeCategories}
                income={income}
                currencies={currencies}
              />
            </form>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setIsEditIncomeDialogOpened(false)} className="budget-error-button-text">
            Close
          </Button>
          <Button
            variant="outlined"
            className="budget-error-button"
            onClick={() => setIsConfirmDeleteDialogOpened(true)}
          >
            Delete
          </Button>
          <Button variant="outlined" className="budget-confirm-button" onClick={handleSubmit(onSubmit)}>
            {isSubmitting ? <CircularProgress size={25} /> : 'Submit'}
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
