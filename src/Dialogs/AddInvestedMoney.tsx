import React, { Dispatch, SetStateAction, useEffect, useState } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  InputLabel,
  TextField,
  Select,
  MenuItem,
  FormHelperText,
  FormControlLabel,
  Checkbox,
  Grid,
} from '@material-ui/core';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import moment from 'moment';
import InvestmentPlatform from '../Interfaces/InvestmentPlatform.interface';
import Investment from '../Interfaces/Investment.interface';
import Expense from '../Interfaces/Expense.interface';
import { InvestedMoneyValidation } from '../Common/Validation/InvestedMoneyValidation';
import Income from '../Interfaces/Income.interface';
import { currencyApi } from '../Api/CurrencyApi';
import Currency from '../Interfaces/Currency.interface';
import { investmentApi } from '../Api/investmentApi';
import { api } from '../Common/Api';

interface Props {
  isOpened: boolean;
  setIsOpened: Dispatch<SetStateAction<boolean>>;
  handleLoadInvestmentPlatforms: () => void;
  platforms: InvestmentPlatform[];
}

export default function AddInvestedMoney({
  isOpened,
  setIsOpened,
  handleLoadInvestmentPlatforms,
  platforms,
}: Props) {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [usingOwnMoney, setUsingOwnMoney] = useState<boolean>(true);
  const [currencies, setCurrencies] = useState<Currency[]>([]);

  useEffect(() => {
    currencyApi
      .getCurrencies()
      .then((res) => setCurrencies(res))
      .catch((err) => console.warn(err));
  }, []);

  const handleClose = () => {
    setIsOpened(false);
  };

  const { handleSubmit, control, errors } = useForm<Investment>({
    mode: 'onChange',
    resolver: yupResolver(InvestedMoneyValidation),
  });

  const onSubmit = async (data: Investment) => {
    setIsLoading(true);
    const newDate: Date = new Date(data.dateTime);
    const newInvestment: Investment = {
      dateTime: newDate,
      investment: data.investment,
      investmentPlatformId: data.investmentPlatformId,
      currencyId: data.currencyId,
    };

    await investmentApi.createInvestment(newInvestment).then((res: Investment) => {
      handleLoadInvestmentPlatforms();
      if (usingOwnMoney) {
        const newExpense: Expense = {
          id: 0,
          name: 'Investment',
          date: newDate,
          price: data.investment!,
          addressToReceipt: '',
          investmentId: res.id,
          currencyId: data.currencyId,
        };

        api.addExpense(newExpense).then(() => { });
      } else {
        const newIncome: Income = {
          id: 0,
          name: `Money invested on platform:${res.investmentPlatformName}`,
          date: newDate,
          price: data.investment!,
          addressToReceipt: '',
          investmentId: res.id,
          currencyId: data.currencyId,
        };

        api.addIncome(newIncome).then((axiosResponse) => console.warn(axiosResponse));
      }

      setIsOpened(false);
    });
    setIsLoading(false);
  };

  const handleChangeCheckbox = (evt: React.ChangeEvent<any>) => {
    setUsingOwnMoney(evt.target.checked);
  };

  return (
    <Dialog
      open={isOpened}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      fullWidth
      maxWidth="sm"
    >
      <DialogTitle>Add invested money</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description" />
        <form>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <InputLabel style={{ marginTop: '10px' }}>Date of the Expense</InputLabel>
              <Controller
                as={<TextField fullWidth type="datetime-local" size="small" />}
                name="dateTime"
                variant="outlined"
                control={control}
                defaultValue={moment(new Date()).format('YYYY-MM-DDTHH:mm')}
                error={errors.dateTime}
                helperText={errors.dateTime ? errors.dateTime.message : ''}
                disabled={isLoading}
              />
            </Grid>
            <Grid item xs={6}>
              <InputLabel style={{ marginTop: '10px' }} error={!!errors.investmentPlatformId}>
                Select platform
              </InputLabel>
              <Controller
                as={
                  <Select fullWidth style={{ height: '40px' }}>
                    {platforms?.map((option: InvestmentPlatform) => (
                      <MenuItem value={option.id}>{option.name}</MenuItem>
                    ))}
                  </Select>
                }
                name="investmentPlatformId"
                error={errors.investmentPlatformId}
                control={control}
                variant="outlined"
                defaultValue={platforms[0]?.id}
                disabled={isLoading}
              />

              {errors.investmentPlatformId && (
                <FormHelperText error style={{ marginLeft: '14px' }}>
                  {errors.investmentPlatformId.message}
                </FormHelperText>
              )}
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <InputLabel style={{ marginTop: '10px' }} error={!!errors.investment}>
                Value of investment
              </InputLabel>
              <Controller
                as={<TextField fullWidth size="small" />}
                name="investment"
                variant="outlined"
                control={control}
                type="number"
                error={errors.investment}
                helperText={errors.investment ? errors.investment.message : ''}
                disabled={isLoading}
              />
            </Grid>
            <Grid item xs={6}>
              <InputLabel style={{ marginTop: '10px' }} error={!!errors.investmentPlatformId}>
                Currency
              </InputLabel>
              <Controller
                as={
                  <Select fullWidth style={{ height: '40px' }}>
                    {currencies?.map((option: Currency) => (
                      <MenuItem value={option.id}>{option.name}</MenuItem>
                    ))}
                  </Select>
                }
                name="currencyId"
                error={errors.investmentPlatformId}
                control={control}
                variant="outlined"
                defaultValue={platforms[0]?.id}
                disabled={isLoading}
              />

              {errors.investmentPlatformId && (
                <FormHelperText error style={{ marginLeft: '14px' }}>
                  {errors.investmentPlatformId.message}
                </FormHelperText>
              )}
            </Grid>
          </Grid>

          <FormControlLabel
            control={<Checkbox checked={usingOwnMoney} name="gilad" />}
            label="Im using money from my savings"
            onChange={(evt) => handleChangeCheckbox(evt)}
            disabled={isLoading}
          />
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="secondary" disabled={isLoading}>
          Close
        </Button>
        <Button
          variant="contained"
          color="primary"
          title="Submit"
          onClick={handleSubmit(onSubmit)}
          // disabled={!isDirty || !isValid}
          disabled={isLoading}
        >
          {isLoading ? 'Sending' : 'Submit'}
        </Button>
      </DialogActions>
    </Dialog>
  );
}
