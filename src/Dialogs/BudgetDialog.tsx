import { useState, ChangeEvent } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  TextField,
  ButtonGroup,
} from '@material-ui/core';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
/* Interfaces */
import DateTimeRange from '../Interfaces/StartEndTime.interface';
/* Styles */
import './BudgetDialog.scss';
import { ReducerActionsEnum } from '../Redux/ReduxStore';
import BudgetDialogButtons from './BudgetDialogButtons';

interface Props {
  isBudgetDialogOpened: boolean;
  setIsBudgetDialogOpened: Function;
}

enum DateOptions {
  thirtyDays = 'thirtyDays',
  month = 'month',
  threeMonths = 'threeMonths',
  sixMonths = 'sixMonths',
  year = 'year',
  ytd = 'ytd',
  custom = 'custom',
}

export default function BudgetDialog({ isBudgetDialogOpened, setIsBudgetDialogOpened }: Props) {
  const [selectedDateRange, setSelectedDateRange] = useState<DateOptions>(DateOptions.month);
  const dateRange = useSelector((state: RootStateOrAny) => state.dateRange);
  const dispatch = useDispatch();

  const handleSelectDateRange = (methodDateRange: DateOptions) => {
    if (methodDateRange === DateOptions.month) {
      dispatch({
        type: ReducerActionsEnum.dateRange,
        payload: {
          startDateTime: new Date(moment().startOf('month').toString()),
          endDateTime: new Date(),
        } as DateTimeRange,
      });
    } else if (methodDateRange === DateOptions.thirtyDays) {
      dispatch({
        type: ReducerActionsEnum.dateRange,
        payload: {
          startDateTime: new Date(moment(new Date()).subtract('30', 'days').startOf('day').toString()),
          endDateTime: new Date(),
        } as DateTimeRange,
      });
    } else if (methodDateRange === DateOptions.threeMonths) {
      dispatch({
        type: ReducerActionsEnum.dateRange,
        payload: {
          startDateTime: new Date(moment(new Date()).subtract('3', 'months').startOf('day').toString()),
          endDateTime: new Date(),
        } as DateTimeRange,
      });
    } else if (methodDateRange === DateOptions.sixMonths) {
      dispatch({
        type: ReducerActionsEnum.dateRange,
        payload: {
          startDateTime: new Date(moment(new Date()).subtract('6', 'months').startOf('day').toString()),
          endDateTime: new Date(),
        } as DateTimeRange,
      });
    } else if (methodDateRange === DateOptions.year) {
      dispatch({
        type: ReducerActionsEnum.dateRange,
        payload: {
          startDateTime: new Date(moment(new Date()).subtract('1', 'year').startOf('day').toString()),
          endDateTime: new Date(),
        } as DateTimeRange,
      });
    } else if (methodDateRange === DateOptions.ytd) {
      dispatch({
        type: ReducerActionsEnum.dateRange,
        payload: {
          startDateTime: new Date(moment().startOf('year').toString()),
          endDateTime: new Date(),
        } as DateTimeRange,
      });
    }

    setSelectedDateRange(dateRange);
  };

  const handleSelectStartDate = (evt: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    dispatch({
      type: ReducerActionsEnum.dateRange,
      payload: {
        startDateTime: new Date(evt.target.value),
        endDateTime: dateRange.endDateTime,
      } as DateTimeRange,
    });
    setSelectedDateRange(DateOptions.custom);
  };

  const handleSelectEndDate = (evt: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    dispatch({
      type: ReducerActionsEnum.dateRange,
      payload: {
        startDateTime: dateRange.startDateTime,
        endDateTime: new Date(evt.target.value),
      } as DateTimeRange,
    });
    setSelectedDateRange(DateOptions.custom);
  };

  const isMobile = (): boolean => {
    if (window.screen.width <= 480) {
      return true;
    }

    return false;
  };

  return (
    <Dialog open={isBudgetDialogOpened} fullWidth PaperProps={{ className: 'budget-dialog' }}>
      <DialogTitle>Choose a time range for budget to be displayed </DialogTitle>
      <DialogContent>
        <div className="budget-dialog-button-group-container">
          {isMobile() ? (
            <BudgetDialogButtons
              isBudgetDialogOpened={isBudgetDialogOpened}
              setIsBudgetDialogOpened={setIsBudgetDialogOpened}
              isMobile={isMobile}
              handleSelectDateRange={handleSelectDateRange}
              selectedDateRange={selectedDateRange}
            />
          ) : (
            <ButtonGroup disableRipple disableElevation color="primary">
              <BudgetDialogButtons
                isBudgetDialogOpened={isBudgetDialogOpened}
                setIsBudgetDialogOpened={setIsBudgetDialogOpened}
                isMobile={isMobile}
                handleSelectDateRange={handleSelectDateRange}
                selectedDateRange={selectedDateRange}
              />
            </ButtonGroup>
          )}
        </div>
        <DialogContentText id="alert-dialog-description">
          <form>
            <div className="budget-dialog-date-time-picker-container budget-dialog-date-time-picker-left">
              <TextField
                type="datetime-local"
                fullWidth
                size="small"
                label="From:"
                required
                variant="outlined"
                value={moment(new Date(dateRange?.startDateTime)).format('YYYY-MM-DDTHH:mm')}
                onChange={(evt) => handleSelectStartDate(evt)}
              />
            </div>
            <div className="budget-dialog-date-time-picker-container budget-dialog-date-time-picker-right">
              <TextField
                type="datetime-local"
                fullWidth
                size="small"
                label="To:"
                required
                variant="outlined"
                value={moment(new Date(dateRange?.endDateTime)).format('YYYY-MM-DDTHH:mm')}
                onChange={(evt) => handleSelectEndDate(evt)}
              />
            </div>
          </form>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => setIsBudgetDialogOpened(false)} className="budget-error-button-text">
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
}
