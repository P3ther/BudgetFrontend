import React, { Dispatch, FC, SetStateAction, useState } from 'react';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import { QrReader } from 'react-qr-reader';
import { RootStateOrAny, useSelector } from 'react-redux';
import { useSnackbar } from 'notistack';
import { expenseApi } from '../../Api/ExpenseApi';
import { AddExpense } from '../../Interfaces/Expense.interface';
import Currency from '../../Interfaces/Currency.interface';
import './QrCodeScanner.scss';
import Logger from '../../Components/Logger/Logger';

interface QrCodeScannerProps {
  setIsQrCodeScannerVisible: Dispatch<SetStateAction<boolean>>;
  setExpense: Dispatch<SetStateAction<AddExpense | undefined>>;
}

const QrCodeScanner: FC<QrCodeScannerProps> = ({
  setIsQrCodeScannerVisible,
  setExpense,
}: QrCodeScannerProps) => {
  const { enqueueSnackbar } = useSnackbar();
  const currencies = useSelector((state: RootStateOrAny) => state.currencies);
  const [qrCodeValue, setQrCodeValue] = useState<string>();
  const [processing, setProcessing] = useState<boolean>(false);

  const checkBillInEkasa = async (billId: string) => {
    try {
      const response = await expenseApi.getReceiptQrCodeDetails(billId);

      return response;
    } catch (err) {
      Logger.logError(err);
    }
  };

  const handleSuccess = async (result: any) => {
    setProcessing(true);
    setQrCodeValue(result.text);
    const ekasaResponse = await checkBillInEkasa(result.text);

    if (ekasaResponse) {
      setExpense({
        name: ekasaResponse.receipt.organization.name,
        date: new Date(ekasaResponse.receipt.date),
        currencyId: currencies.find((currency: Currency) => currency.currencyCode === 'EUR').id,
        price: ekasaResponse.receipt.totalPrice,
      });
      setIsQrCodeScannerVisible(false);
      enqueueSnackbar('Qr code successfully read, details were added to this dialog.', {
        variant: 'success',
      });
    }
  };

  const handleRead = async (result: any, error: any) => {
    if (result) {
      await handleSuccess(result);
    }

    if (error) {
      console.error(error);
    }
  };

  return (
    <DialogContent>
      <DialogContentText>
        {!qrCodeValue && (
          <QrReader
            onResult={async (result: any, error: any) =>
              handleRead(result, error)
            }
            // eslint-disable-next-line no-undef
            constraints={{ facingMode: 'environment' } as MediaTrackConstraints}
          />
        )}
        {processing && (
          <div className="scanner-processing">Qr code read please wait until it gets processed.</div>
        )}
      </DialogContentText>
    </DialogContent>
  );
};

export default QrCodeScanner;
