import { Dispatch, SetStateAction } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  Typography,
} from '@material-ui/core';

interface Props {
  isOpened: boolean;
  setIsOpened: Dispatch<SetStateAction<boolean>>;
  handleYes: Function;
}

export default function UnsavedChanges({ isOpened, setIsOpened, handleYes }: Props) {
  const handleYesOption = (handleYes: Function) => {
    setIsOpened(false);
    handleYes();
  };

  return (
    <Dialog open={isOpened} fullWidth maxWidth="xs">
      <DialogTitle>Unsaved Changes</DialogTitle>
      <DialogContent>
        <DialogContentText>
          <Typography style={{ textAlign: 'center' }}>You have made changes.</Typography>
          <Typography style={{ textAlign: 'center' }}>Do you want to discard them?</Typography>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => setIsOpened(false)}
          color="secondary"
          size="small"
          className="budget-error-button-text"
        >
          No
        </Button>
        <Button
          variant="outlined"
          size="small"
          color="primary"
          className="budget-confirm-button"
          onClick={() => handleYesOption(handleYes)}
        >
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  );
}
