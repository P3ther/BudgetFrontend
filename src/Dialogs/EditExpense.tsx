import { useEffect, useState } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@material-ui/core';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { api } from '../Common/Api';
import Income from '../Interfaces/Income.interface';
import Expense from '../Interfaces/Expense.interface';
import ExpenseForm from './Forms/ExpenseForm';
import ConfirmDelete from './ConfirmDelete';
import { ExpenseValidation } from '../Common/Validation/ExpenseValidation';
import DateTimeRange from '../Interfaces/StartEndTime.interface';
import Currency from '../Interfaces/Currency.interface';
import { CategoryInterface } from '../Interfaces/Interfaces';

interface Props {
  isEditExpenseialogOpened: boolean;
  setIsEditExpenseDialogOpened: Function;
  expense: Expense;
  defaultTimes: DateTimeRange;
  allCategories: CategoryInterface[];
  currencies: Currency[];
}

export default function EditExpense({
  isEditExpenseialogOpened,
  setIsEditExpenseDialogOpened,
  expense,
  defaultTimes,
  allCategories,
  currencies,
}: Props) {
  const [isConfirmDeleteDialogOpened, setIsConfirmDeleteDialogOpened] = useState<boolean>(false);
  const [expenseCategories, setExpenseCategories] = useState<CategoryInterface[]>([]);

  useEffect(() => {
    setExpenseCategories(allCategories.filter((category: CategoryInterface) => category.type === 0));
  }, [allCategories]);

  const { handleSubmit, control, errors } = useForm<Income>({
    mode: 'onChange',
    resolver: yupResolver(ExpenseValidation),
  });
  const onSubmit = async (data: Expense) => {
    const newDate: Date = new Date(data.date);
    const editedExpense: Expense = {
      id: expense.id,
      name: data.name,
      date: newDate,
      price: data.price,
      addressToReceipt: '',
      category: data.category,
      categoryName: '',
      currencyId: data.currencyId,
      currencySign: '',
    };

    await api.updateExpense(editedExpense).then((res) => { });
    setIsEditExpenseDialogOpened(false);
  };

  const doDelete = async () => {
    await api.deleteExpense(expense);
    setIsConfirmDeleteDialogOpened(false);
    setIsEditExpenseDialogOpened(false);
  };

  return (
    <>
      <ConfirmDelete
        isConfirmDeleteDialogOpened={isConfirmDeleteDialogOpened}
        setIsConfirmDeleteDialogOpened={setIsConfirmDeleteDialogOpened}
        doDelete={doDelete}
        deletedName="Expense"
      />
      <Dialog open={isEditExpenseialogOpened} fullWidth maxWidth="sm">
        <DialogTitle>Edit Expense</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <form>
              <ExpenseForm
                control={control}
                errors={errors}
                categories={expenseCategories}
                expense={expense}
                currencies={currencies}
              />
            </form>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setIsEditExpenseDialogOpened(false)} className="budget-error-button-text">
            Close
          </Button>
          <Button
            variant="outlined"
            className="budget-error-button"
            onClick={() => setIsConfirmDeleteDialogOpened(true)}
          >
            Delete
          </Button>
          <Button variant="outlined" className="budget-confirm-button" onClick={handleSubmit(onSubmit)}>
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
