import React, { useState } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@material-ui/core';
import { useForm } from 'react-hook-form';
import { useSelector, useDispatch } from 'react-redux';
import { yupResolver } from '@hookform/resolvers/yup';
import Income from '../Interfaces/Income.interface';
import { IncomeValidation } from '../Common/Validation/IncomeValidation';
import IncomeForm from './Forms/IncomeForm';
import UnsavedChanges from './UnsavedChanges';
import { CategoryInterface } from '../Interfaces/Interfaces';
import { getCurrencies, getIncomeCategories } from '../Redux/Selectors';
import { getAddIncome } from '../Redux/App/app.selectors';
import { changeAddIncomeDialog } from '../Redux/App/app.action';
import { addIncome } from '../Redux/Actions';

interface Props {
  allCategories: CategoryInterface[];
}

export default function AddIncome({ allCategories }: Props) {
  const dispatch = useDispatch();
  const [hasUnsavedChanges, setHasUnsavedChanges] = useState<boolean>(false);
  const currencies = useSelector(getCurrencies);
  const isOpened = useSelector(getAddIncome);
  const incomeCategories = useSelector(getIncomeCategories);

  const handleClose = () => {
    if (isDirty) {
      setHasUnsavedChanges(true);
    } else {
      dispatch(changeAddIncomeDialog());
    }
  };

  const {
    handleSubmit,
    control,
    errors,
    formState: { isDirty, isValid },
  } = useForm<Income>({
    mode: 'onChange',
    resolver: yupResolver(IncomeValidation),
  });

  const onSubmit = async (data: Income) => {
    const newDate: Date = new Date(data.date);
    const newIncome: Income = {
      id: 0,
      name: data.name,
      date: newDate,
      price: data.price,
      addressToReceipt: '',
      category: data.category,
      categoryName: '',
      currencyId: data.currencyId,
      currencySign: '',
    };

    dispatch(addIncome(newIncome));
  };

  return (
    <Dialog open={isOpened} onClose={handleClose} fullWidth maxWidth="sm">
      <UnsavedChanges
        isOpened={hasUnsavedChanges}
        setIsOpened={setHasUnsavedChanges}
        handleYes={() => dispatch(changeAddIncomeDialog())}
      />
      <form onSubmit={handleSubmit(onSubmit)}>
        <DialogTitle>Add new Income</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <IncomeForm
              control={control}
              categories={incomeCategories}
              errors={errors}
              currencies={currencies}
            />
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} className="budget-error-button-text">
            Close
          </Button>
          <Button
            variant="outlined"
            className="budget-confirm-button"
            disabled={!isValid || !isDirty}
            type="submit"
          >
            Submit
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
}
