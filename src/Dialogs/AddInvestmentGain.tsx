import React, { Dispatch, SetStateAction, useState } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  InputLabel,
  TextField,
  Select,
  MenuItem,
  FormHelperText,
  Grid,
} from '@material-ui/core';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import moment from 'moment';
import { api } from '../Common/Api';
import InvestmentPlatform from '../Interfaces/InvestmentPlatform.interface';
import Investment from '../Interfaces/Investment.interface';
import { InvestedGainValidation } from '../Common/Validation/InvestedGainValidation';
import Expense from '../Interfaces/Expense.interface';
import Income from '../Interfaces/Income.interface';
import { currencyApi } from '../Api/CurrencyApi';
import Currency from '../Interfaces/Currency.interface';
import { investmentApi } from '../Api/investmentApi';

interface Props {
  isOpened: boolean;
  setIsOpened: Dispatch<SetStateAction<boolean>>;
  handleLoadInvestmentPlatforms: () => void;
  platforms: InvestmentPlatform[];
}

export default function AddInvestmentGain({
  isOpened,
  setIsOpened,
  handleLoadInvestmentPlatforms,
  platforms,
}: Props) {
  const [currencies, setCurrencies] = useState<Currency[]>([]);
  const handleEnter = () => {
    currencyApi
      .getCurrencies()
      .then((res) => setCurrencies(res))
      .catch((err) => console.warn(err));
  };
  const handleClose = () => {
    setIsOpened(false);
  };

  const { handleSubmit, control, errors } = useForm<Investment>({
    mode: 'onChange',
    resolver: yupResolver(InvestedGainValidation),
  });

  const onSubmit = async (data: Investment) => {
    const newDate: Date = new Date(data.dateTime);
    const newInvestment: Investment = {
      dateTime: newDate,
      gain: data.investment,
      investmentPlatformId: data.investmentPlatformId,
      currencyId: data.currencyId,
    };

    await investmentApi.createInvestment(newInvestment).then((res: Investment) => {
      handleLoadInvestmentPlatforms();
      setIsOpened(false);
    });
    const investmentPlatformName: InvestmentPlatform[] = platforms.filter(
      (b) => b.id === data.investmentPlatformId,
    );

    if (data.investment! >= 0) {
      const newIncome: Income = {
        id: 0,
        name: `Gain on investment platform ${investmentPlatformName[0].name}`,
        date: newDate,
        price: data.investment!,
        addressToReceipt: '',
        currencyId: data.currencyId,
      };

      api.addIncome(newIncome);
    } else {
      const newExpense: Expense = {
        id: 0,
        name: `Loss on investment platform ${investmentPlatformName[0].name}`,
        date: newDate,
        price: Math.abs(data.investment!),
        addressToReceipt: '',
        currencyId: data.currencyId,
      };

      api.addExpense(newExpense);
    }
  };

  return (
    <Dialog open={isOpened} onClose={handleClose} onEnter={handleEnter} fullWidth maxWidth="sm">
      <DialogTitle>Add investment gain</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description" />
        <form>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <InputLabel style={{ marginTop: '10px' }} error={!!errors.investment}>
                Gain of investment
              </InputLabel>
              <Controller
                as={<TextField fullWidth size="small" />}
                name="investment"
                variant="outlined"
                control={control}
                type="number"
                error={errors.investment}
                helperText={errors.investment ? errors.investment.message : ''}
              />
            </Grid>

            <Grid item xs={6}>
              <InputLabel style={{ marginTop: '10px' }}>
                Currency
              </InputLabel>
              <Controller
                as={
                  <TextField fullWidth select size="small">
                    {currencies.map((currency: Currency, key: number) => (
                      <MenuItem value={currency.id}>{`${currency.name} (${currency.sign})`}</MenuItem>
                    ))}
                  </TextField>
                }
                name="currencyId"
                variant="outlined"
                control={control}
                type="number"
              // error={errors.currencyId}
              // helperText={errors.currencyId ? errors.currencyId.message : ''}
              />
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <InputLabel style={{ marginTop: '10px' }}>Date of the Expense</InputLabel>
              <Controller
                as={<TextField fullWidth type="datetime-local" size="small" />}
                name="dateTime"
                variant="outlined"
                control={control}
                defaultValue={moment(new Date()).format('YYYY-MM-DDTHH:mm')}
                error={errors.dateTime}
                helperText={errors.dateTime ? errors.dateTime.message : ''}
              />
            </Grid>
            <Grid item xs={6}>
              <InputLabel style={{ marginTop: '10px' }} error={!!errors.investmentPlatformId}>
                Select platform
              </InputLabel>
              <Controller
                as={
                  <Select fullWidth style={{ height: '40px' }}>
                    {platforms?.map((option: InvestmentPlatform) => (
                      <MenuItem value={option.id}>{option.name}</MenuItem>
                    ))}
                  </Select>
                }
                name="investmentPlatformId"
                error={errors.investmentPlatformId}
                control={control}
                variant="outlined"
                defaultValue={platforms[0]?.id}
              />

              {errors.investmentPlatformId && (
                <FormHelperText error style={{ marginLeft: '14px' }}>
                  {errors.investmentPlatformId.message}
                </FormHelperText>
              )}
            </Grid>
          </Grid>
        </form>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="secondary">
          Close
        </Button>
        <Button
          variant="contained"
          color="primary"
          title="Submit"
          onClick={handleSubmit(onSubmit)}
        // disabled={!isDirty || !isValid}
        >
          Submit
        </Button>
      </DialogActions>
    </Dialog>
  );
}
