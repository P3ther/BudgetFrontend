import { EditorState } from 'draft-js';

export interface Note extends NewNote {
  id: number;
}

export interface NoteBE extends Omit<Note, 'noteDetails'> {
  noteDetails: string;
}

export interface NewNote {
  noteName: string;
  noteDetails: EditorState;
  dateCreated: Date;
  lastUpdated: Date;
}

export interface NewNoteBE extends Omit<NewNote, 'noteDetails'> {
  noteDetails: string;
}
