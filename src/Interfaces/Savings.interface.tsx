export default interface Savings {
  id: number;
  value: number;
  savedValue: number;
  date: Date;
}
