export default interface MoneyChange {
  id: number;
  name: string;
  date: Date;
  price: number;
  addressToReceipt?: string;
  investmentId?: number;
  type?: string;
  category?: number;
  categoryName?: string;
  currencyId: number;
  currencySign?: string;
}
