import Budget from './Budget.interface';
import { Category, NewCategory } from './Category.interface';
import DateIntf from './Date.interface';
import Registration from './Registration.interface';
import ResetPassword from './ResetPassword.interface';
import Savings from './Savings.interface';

export interface BudgetInterface extends Budget {}
export interface CategoryInterface extends Category {}
export interface NewCategoryInterface extends NewCategory {}
export interface RegistrationInterface extends Registration {}
export interface ResetPasswordInterface extends ResetPassword {}
export interface SavingsInterface extends Savings {}
export interface DateInterface extends DateIntf {}
