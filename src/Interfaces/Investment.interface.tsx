export default interface Investment {
  id?: number;
  dateTime: Date;
  investment?: number;
  gain?: number;
  investmentPlatformId: number;
  investmentPlatformName?: string;
  currencyId: number;
  currencySign?: string;
}
