export default interface ChangePassword {
	username: string;
	password: string;
	newPassword: string;
	confirmNewPassword?: string;
}
