import Expense from "./Expense.interface";
import Income from "./Income.interface";

export interface Category extends NewCategory {
  id: number;
}

export interface NewCategory {
  name: string;
  limit: number;
  value?: number;
  type: number;
  expenseList?: Expense[];
  incomeList?: Income[];
  currencyId: number;
  currencySign?: string;
}
