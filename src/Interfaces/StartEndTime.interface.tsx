export default interface DateTimeRange {
  startDateTime: Date;
  endDateTime: Date;
}
