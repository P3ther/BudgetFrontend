import { UserRole } from "../Enums/UserRole";
import Currency from "./Currency.interface";
import Theme from "./Theme.interface";

export default interface User {
  username: string;
  password?: string;
  registrationCode?: number;
  userRole?: UserRole;
  userEmail?: string;
  forgotPasswordCode?: string;
  defaultCurrency?: Currency;
  defaultTheme?: Theme;
}
