export default interface Expense {
  id: number;
  name: string;
  date: Date;
  price: number;
  addressToReceipt?: string;
  investmentId?: number;
  type?: string;
  category?: number;
  categoryName?: string;
  currencyId: number;
  currencySign?: string;
};

export type AddExpense = Omit<Expense, 'id'>;
