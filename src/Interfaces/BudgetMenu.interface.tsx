export default interface BudgetMenuInterface {
  name: string;
  onClickFunction?: () => void;
  isLink: boolean;
  linkAddres?: string;
}
