export default interface CurrencyPairs {
    id?: number;
    firstCurrencyId: number;
    firstCurrencyCode: string;
    firstCurrencyName: string;
    firstCurrencySign: string;
    secondCurrencyId: number;
    secondCurrencyCode: string;
    secondCurrencyName: string;
    secondCurrencySign: string;
    exchangeRate: number;
    lastChange: Date;
}