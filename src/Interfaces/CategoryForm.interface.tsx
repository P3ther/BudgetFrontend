export default interface CategoryForm {
	id?: number;
	name: string;
	limit: number;
	type: number;
	visible: number;
}
