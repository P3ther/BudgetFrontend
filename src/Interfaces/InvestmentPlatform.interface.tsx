import Investment from './Investment.interface';

export default interface InvestmentPlatform {
	id: number;
	name: string;
	investmentsOnPlatform: Investment[];
	totalValue?: number;
	totalGain?: number;
}
