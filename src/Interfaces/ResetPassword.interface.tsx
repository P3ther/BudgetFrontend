export default interface ResetPassword {
	newPassword: string;
	confirmNewPassword?: string;
}
