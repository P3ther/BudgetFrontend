export default interface Theme {
    id: number;
    themeName: string;
    themeFilename: string;
}
