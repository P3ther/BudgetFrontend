export default interface GraphValueDateNumber {
  x: Date | string;
  y: number;
}
