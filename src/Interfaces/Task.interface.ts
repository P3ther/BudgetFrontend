export default interface Task {
  id?: number;
  taskName: string;
  taskDetails: string;
  dateCreated: Date;
  dateExpired: Date;
  priority: number;
}
