export default interface InvestmentData {
    platformName: string;
    value: number;
    dateTime: Date;
}
