export default interface Registration {
  username: string;
  email: string;
  newPassword: string;
  confirmNewPassword: string;
}
