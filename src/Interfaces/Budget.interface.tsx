import Expense from './Expense.interface';
import Income from './Income.interface';

export default interface Budget {
  id: number;
  expensesList: Expense[];
  incomeList: Income[];
  totalBalance: number;
  totalExpense: number;
  totalIncome: number;
  expectedIncome: number;
  expectedExpense: number;
  totalInvestmentsValue: number;
  totalInvestmentsGain: number;
}
