export default interface Currency {
    id?: number;
    name: string;
    currencyCode: string;
    sign: string;
    exchangeRate?: number;
}