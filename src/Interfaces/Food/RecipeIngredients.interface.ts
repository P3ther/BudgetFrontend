import { Ingredient } from './Ingredient.interface';
import { Recipe } from './Recipe.interface';

export interface RecipeIngredients {
  id?: number;
  recipe?: Recipe;
  ingredient: Ingredient;
  quantity: number;
}
