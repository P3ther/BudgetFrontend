import { IngredientUnit } from '../../Enums/IngredientUnit';
import { RecipeIngredients } from './RecipeIngredients.interface';

export interface NewIngredient {
  ingredientName: string;
  ingredientCalories: number;
  ingredientUnit: IngredientUnit;
  recipeIngredients?: RecipeIngredients[];
}

export interface Ingredient extends NewIngredient {
  id: number;
}
