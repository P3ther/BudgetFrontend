import { EditorState } from 'draft-js';
import { RecipeIngredients } from './RecipeIngredients.interface';

export interface Recipe extends NewRecipe {
  id: number;
}

export interface RecipeBE extends Omit<Recipe, 'recipeInstructions'> {
  recipeInstructions: string;
}

export interface NewRecipe {
  recipeName: string;
  recipeInstructions: EditorState;
  recipeIngredients?: RecipeIngredients[];
  recipeCalories?: number;
}

export interface NewRecipeBE extends Omit<NewRecipe, 'recipeInstructions'> {
  recipeInstructions: string;
}
