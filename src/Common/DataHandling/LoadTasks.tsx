import { Dispatch, SetStateAction } from 'react';
import { taskApi } from '../../Api/TaskApi';
import Task from '../../Interfaces/Task.interface';

export const loadTasks = async (setTasks: Dispatch<SetStateAction<Task[]>>) => {
  await taskApi.listTasks().then((res) => {
    res.sort((a: Task, b: Task) => new Date(b.dateExpired).getTime() - new Date(a.dateExpired).getTime());
    setTasks(res);
  });
};
