import { Dispatch, SetStateAction } from 'react';
import { ingredientApi } from '../../Api/ingredientApi';
import { Ingredient } from '../../Interfaces/Food/Ingredient.interface';
import { RecipeIngredients } from '../../Interfaces/Food/RecipeIngredients.interface';

export const LoadIngredients = async (
  setIngredients: Dispatch<SetStateAction<Ingredient[]>>,
  selectedIngredients: RecipeIngredients[] | null,
) => {
  await ingredientApi.listIngredient().then((res) => {
    if (selectedIngredients !== null) {
      // eslint-disable-next-line no-param-reassign
      res = res.filter(
        (ingredient: Ingredient, key: number) =>
          !selectedIngredients.find(
            (selectedIngredient: RecipeIngredients) => ingredient.id === selectedIngredient.ingredient.id,
          ),
      );
      setIngredients(res);
    } else {
      setIngredients(res);
    }
  });
};
