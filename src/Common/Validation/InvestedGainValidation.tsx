import { date, number, object } from 'yup';

export const InvestedGainValidation = object().shape({
	dateTime: date(),
	investment: number()
		.required('Investment value is Required')
		.typeError('Investment value has to be a number'),
	investmentPlatformId: number().required('Investment platform is Required'),
});
