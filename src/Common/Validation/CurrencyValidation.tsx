import { object, string } from 'yup';
import { char_limit } from '../CommonVariables';

export const CurrencyValidation = object().shape({
  name: string().required('Name is Required').max(char_limit, 'String is too long.'),
  currencyCode: string().required('Name is Required').max(char_limit, 'String is too long.'),
  sign: string().required('Sign is Required').max(char_limit, 'String is too long.'),
});
