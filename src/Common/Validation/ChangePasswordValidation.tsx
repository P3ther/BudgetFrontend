import { object, string, ref } from 'yup';

const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

export const ChangePasswordValidation = object().shape({
  password: string().required('Old password is required'),
  newPassword: string()
    .required('New password is required')
    .matches(
      passwordRegex,
      'Password has to contain at least 8 characters, one special character and number',
    ),
  confirmNewPassword: string()
    .required('New password is required')
    .oneOf([ref('newPassword')], 'Passwords do not match!'),
});
