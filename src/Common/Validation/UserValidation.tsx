import { string, object } from 'yup';

export const UserValidation = object().shape({
    username: string().required('Username is Required'),
    password: string().required('Password is Required'),
});
