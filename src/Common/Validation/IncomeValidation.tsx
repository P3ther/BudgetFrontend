import { date, number, object, string } from 'yup';
import { char_limit } from '../CommonVariables';

const parseToDate = (date: string) => new Date(date);

export const IncomeValidation = object().shape({
  name: string().required('Name is Required').max(char_limit, 'String is too long.'),
  date: date().transform(parseToDate).typeError('Date is Required').required('Date is Required'),
  category: number().required('Category is Required'),
  price: number()
    .required('Price is Required')
    .typeError('Price has to be a number')
    .positive('Price cannot be negative'),
  currencyId: number().required('Currency is Required'),
});
