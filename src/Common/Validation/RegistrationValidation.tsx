import { object, string, ref } from 'yup';

const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

const RegistrationValidation = object().shape({
  username: string().min(3, 'Username Has to have at least three characters'),
  email: string().required('Email is required').email('Email addres has to be valid email'),
  newPassword: string()
    .required('New password is required')
    .matches(
      passwordRegex,
      'Password has to contain at least 8 characters, one special character and number',
    ),
  confirmNewPassword: string()
    .required('New password is required')
    .oneOf([ref('newPassword')], 'Passwords do not match!'),
});

export default RegistrationValidation;
