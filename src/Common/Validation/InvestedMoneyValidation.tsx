import { date, number, object } from 'yup';

export const InvestedMoneyValidation = object().shape({
	dateTime: date(),
	investment: number()
		.required('Investment value is Required')
		.typeError('Investment value has to be a number')
		.positive('Investment value cannot be negative'),
	investmentPlatformId: number().required('Investment platform is Required'),
});
