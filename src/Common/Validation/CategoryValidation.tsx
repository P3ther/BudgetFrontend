import { number, object, string } from 'yup';
import { char_limit } from '../CommonVariables';

export const CategoryValidation = object().shape({
  name: string().required('Name is Required').max(char_limit, 'String is too long.'),
  limit: number()
    .required('Limit is Required')
    .typeError('Limit has to be a number')
    .positive('Limit cannot be negative'),
  currencyId: number().required('Currency is Required'),
  type: number().required('Category is Required'),
});
