interface Props {
  height: number;
  percentage: number;
  barColor: string;
  color: string;
}

export default function VerticalBarGraph({ height, percentage, barColor, color }: Props) {
  return (
    <div
      style={{
        width: '40px',
        height: height + 'px',
        backgroundColor: color,
        borderRadius: '1vh',
        position: 'relative',
        marginLeft: 'auto',
        marginRight: 'auto',
      }}
    >
      <div
        style={{
          width: '100%',
          position: 'absolute',
          bottom: '0px',
          height: percentage.toFixed(2) + '%',
          maxHeight: '100%',
          backgroundColor: barColor,
          borderRadius: '1vh',
        }}
      ></div>
    </div>
  );
}
