export const getDatesForInvestmentData = (startDate: Date, dateDifference: number) => {
  let dateTimeArray: Date[] = [];
  dateTimeArray.push(new Date(startDate));
  let todayDate: Date = new Date();
  todayDate.setDate(todayDate.getDate() - dateDifference);
  while (startDate < todayDate) {
    startDate.setDate(startDate.getDate() + dateDifference);
    dateTimeArray.push(new Date(startDate));
  }
  dateTimeArray.push(new Date());
  return dateTimeArray;
};
