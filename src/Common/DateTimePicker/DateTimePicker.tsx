import { Dispatch, SetStateAction, useEffect, useRef, useState } from 'react';
import moment from 'moment';
import Moment from 'react-moment';
import { Grid, IconButton } from '@material-ui/core';
import './DateTimePicker.scss';

/* Icons */
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import DateTimeRange from '../../Interfaces/StartEndTime.interface';

interface Props {
  element: HTMLElement | null;
  setElement: Dispatch<SetStateAction<HTMLElement | null>>;
  selectedDate: Date;
  setSelectedDate: (newDateTime: Date) => Date;
  setDateRange?: DateTimeRange;
}

interface Position {
  xAxis: number;
  yAxis: number;
}

export default function DateTimePicker({ element, setElement, selectedDate }: Props) {
  const [displayedMonth, setDisplayedMonth] = useState<Date>(new Date());
  const [attachement, setAttachement] = useState<Position | null>();
  const [daysOfMonth, setDaysOfMonth] = useState<number[]>([]);
  const [weeksInMonth, setWeeksInMonth] = useState<number>(0);
  const wrapperRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (wrapperRef.current && !wrapperRef.current.contains(event.target as Node)) {
        setElement(null);
      }
    };
    document.addEventListener('mousedown', (evt) => handleClickOutside(evt));
    return () => {
      document.removeEventListener('mousedown', (evt) => handleClickOutside(evt));
    };
  }, [wrapperRef]); // eslint-disable-line react-hooks/exhaustive-deps

  const handleNextMonth = () => {
    setDisplayedMonth(new Date(moment(displayedMonth).add('1', 'month').toString()));
  };
  const handlePreviousMonth = () => {
    setDisplayedMonth(new Date(moment(displayedMonth).subtract('1', 'month').toString()));
  };
  useEffect(() => {
    let temDaysInMonth: number[] = [];
    for (
      let i =
        moment(displayedMonth).subtract('1', 'month').daysInMonth() -
        (moment(displayedMonth).subtract('1', 'month').endOf('month').day() - 1);
      i <= moment(displayedMonth).subtract('1', 'month').daysInMonth();
      i++
    ) {
      temDaysInMonth.push(i);
    }
    for (let i = 1; i <= moment(displayedMonth).daysInMonth(); i++) {
      temDaysInMonth.push(i);
    }
    for (let i = 1; i <= 7 - moment(displayedMonth).endOf('month').day(); i++) {
      temDaysInMonth.push(i);
    }
    setWeeksInMonth(temDaysInMonth.length / 7);
    setDaysOfMonth(temDaysInMonth);
    if (element !== null) {
      setAttachement({
        xAxis: element!.getBoundingClientRect().bottom,
        yAxis: element!.getBoundingClientRect().left + 24,
      });
    }
  }, [element, displayedMonth]);

  const addHour = () => {
    //let empDateTime: Date = selectedDate;
    selectedDate = moment(selectedDate).add('1', 'hour').toDate();
    //selectedDate = tempDateTime;
  };

  if (!!element) {
    return (
      <div
        ref={wrapperRef}
        className="date-time-picker-container"
        style={{
          top: attachement?.xAxis,
          left: attachement?.yAxis,
        }}
      >
        <div style={{ width: '100%', textAlign: 'center' }}>
          <div style={{ height: '20px', width: '20px', display: 'inline', float: 'left' }}>
            <IconButton className="date-time-picker-month-button" onClick={handlePreviousMonth}>
              <ArrowLeftIcon style={{ fontSize: '20px' }} />
            </IconButton>
          </div>
          <div style={{ height: '20px', width: '20px', display: 'inline', float: 'left' }}>
            <IconButton className="date-time-picker-month-button" onClick={handleNextMonth}>
              <ArrowRightIcon style={{ fontSize: '20px' }} />
            </IconButton>
          </div>
          <Moment format="MMMM YYYY">{displayedMonth}</Moment>
        </div>
        <Grid container>
          <Grid item xs={10}>
            <div>
              {daysOfMonth.slice(0, 7).map((day: number) => (
                <div className="date-time-picker-date-day">{day}</div>
              ))}
            </div>
            <div>
              {daysOfMonth.slice(7, 14).map((day: number) => (
                <div className="date-time-picker-date-day">{day + '  '}</div>
              ))}
            </div>
            <div>
              {daysOfMonth.slice(14, 21).map((day: number) => (
                <div className="date-time-picker-date-day">{day}</div>
              ))}
            </div>
            <div>
              {daysOfMonth.slice(21, 28).map((day: number) => (
                <div className="date-time-picker-date-day">{day}</div>
              ))}
            </div>
            {weeksInMonth > 4 && (
              <div>
                {daysOfMonth.slice(28, 35).map((day: number) => (
                  <div className="date-time-picker-date-day">{day}</div>
                ))}
              </div>
            )}
            {weeksInMonth > 5 && (
              <div>
                {daysOfMonth.slice(28, 35).map((day: number) => (
                  <div className="date-time-picker-date-day">{day}</div>
                ))}
              </div>
            )}
          </Grid>
          <Grid item xs={2}>
            <IconButton className="date-time-time-button">
              <ArrowDropUpIcon onClick={addHour} />
            </IconButton>
            <div className="date-time-time-container">{moment(new Date(selectedDate)).format('HH')}</div>
            <IconButton className="date-time-time-button">
              <ArrowDropDownIcon />
            </IconButton>
            <IconButton className="date-time-time-button">
              <ArrowDropUpIcon />
            </IconButton>
            <div className="date-time-time-container">
              <Moment format="mm">{selectedDate}</Moment>
            </div>
            <IconButton className="date-time-time-button">
              <ArrowDropDownIcon />
            </IconButton>
          </Grid>
        </Grid>
      </div>
    );
  } else {
    return <div></div>;
  }
}
