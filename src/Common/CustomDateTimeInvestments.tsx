import { InputLabel, TextField, Button, Grid } from '@material-ui/core';
import { Controller, Control } from 'react-hook-form';
import DateTimeRange from '../Interfaces/StartEndTime.interface';
import moment from 'moment';

interface Props {
  control: Control<DateTimeRange>;
}

export default function CustomDateTimeInvestments({ control }: Props) {
  return (
    <form>
      <Grid container spacing={2}>
        <Grid item md={6}>
          <InputLabel>From:</InputLabel>
          <Controller
            as={<TextField fullWidth type="datetime-local" size="small" />}
            name="date"
            variant="outlined"
            control={control}
            defaultValue={moment(new Date(new Date().getFullYear(), new Date().getMonth(), 1, 0, 0)).format(
              'YYYY-MM-DDTHH:mm',
            )}
          />
        </Grid>
        <Grid item md={6}>
          <InputLabel>To:</InputLabel>
          <Controller
            as={<TextField fullWidth type="datetime-local" size="small" />}
            name="date"
            variant="outlined"
            control={control}
            defaultValue={moment(new Date()).format('YYYY-MM-DDTHH:mm')}
          />
        </Grid>
      </Grid>
      <div style={{ width: '100%', textAlign: 'center' }}>
        <Button
          variant="contained"
          color="primary"
          title="Submit"
          style={{ backgroundColor: '#90be6d', marginTop: '1vh' }}
        >
          Submit
        </Button>
      </div>
    </form>
  );
}
