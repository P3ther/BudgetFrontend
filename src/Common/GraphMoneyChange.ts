import moment from 'moment';
import GraphValueDateNumber from '../Interfaces/GraphValueDateNumber.interface';
import MoneyChange from '../Interfaces/MoneyChange.interface';
import DateTimeRange from '../Interfaces/StartEndTime.interface';

export const parseMoneyChangeForMonthlyGraph = (
  values: MoneyChange[],
  dateRange: DateTimeRange,
): GraphValueDateNumber[] => {
  const returnedValues: GraphValueDateNumber[] = [];
  const tempDateTime: Date = new Date(dateRange.startDateTime);

  for (
    let i: Date = tempDateTime;
    i < dateRange.endDateTime;
    i = new Date(moment(i).add(1, 'month').toDate())
  ) {
    let tempPrice: number = 0;
    const filteredValues: MoneyChange[] = values.filter(
      (value) =>
        new Date(value.date) >= new Date(moment(i).startOf('month').toDate()) &&
        new Date(value.date) <= new Date(moment(i).endOf('month').toDate()),
    );

    filteredValues.forEach((value) => (tempPrice += value.price));
    const tempReturn: GraphValueDateNumber = {
      y: tempPrice,
      x: moment(i).format('DD MMM YYYY'),
    };

    returnedValues.push(tempReturn);
  }

  return returnedValues;
};

export const parseMoneyChangeForDailyGraph = (
  values: MoneyChange[],
  dateRange: DateTimeRange,
): GraphValueDateNumber[] => {
  const returnedValues: GraphValueDateNumber[] = [];
  const tempDateTime: Date = new Date(dateRange.startDateTime);
  let tempPrice: number = 0;

  for (
    let i: Date = tempDateTime;
    i < dateRange.endDateTime;
    i = new Date(moment(i).add(1, 'day').toDate())
  ) {
    let tempForPrice: number = tempPrice;
    const filteredValues: MoneyChange[] = values.filter(
      (value) =>
        new Date(value.date) >= new Date(moment(i).startOf('day').toDate()) &&
        new Date(value.date) <= new Date(moment(i).endOf('day').toDate()),
    );

    filteredValues.forEach((value) => (tempForPrice += value.price));
    const tempReturn: GraphValueDateNumber = {
      y: tempPrice,
      x: moment(i).format('DD MMM YYYY'),
    };

    returnedValues.push(tempReturn);
    tempPrice = tempForPrice;
  }

  tempPrice = 0;

  return returnedValues;
};

export const parseMoneyChangeForWeeklyGraph = (
  values: MoneyChange[],
  dateRange: DateTimeRange,
): GraphValueDateNumber[] => {
  const returnedValues: GraphValueDateNumber[] = [];
  const tempDateTime: Date = new Date(dateRange.startDateTime);
  let tempMonth: number = dateRange.startDateTime.getMonth();
  let tempPrice: number = 0;

  for (
    let i: Date = tempDateTime;
    i < dateRange.endDateTime;
    i = new Date(moment(i).add(1, 'week').toDate())
  ) {
    let tempForPrice: number = tempPrice;
    const filteredValues: MoneyChange[] = values.filter(
      (value) =>
        new Date(value.date) >= new Date(moment(i).startOf('week').toDate()) &&
        new Date(value.date) <= new Date(moment(i).endOf('week').toDate()),
    );

    filteredValues.forEach((value) => (tempForPrice += value.price));
    const tempReturn: GraphValueDateNumber = {
      y: tempPrice,
      x: moment(i).format('DD MMM YYYY'),
    };

    returnedValues.push(tempReturn);
    tempPrice = tempForPrice;
    if (tempMonth < i.getMonth()) {
      tempPrice = 0;
      tempMonth++;
    }
  }

  return returnedValues;
};
