import { useEffect, useState } from 'react';
import { IconButton } from '@material-ui/core';

import './Styles/BudgetPagination.css';

interface Props {
  numberOfRows: number;
  rowsPerPage: number;
  nextPage: () => void;
  previousPage: () => void;
  typesOfPagination: string;
}

export default function BudgetPagination({
  numberOfRows,
  rowsPerPage,
  nextPage,
  previousPage,
  typesOfPagination,
}: Props) {
  const [selectedPage, setSelectedPage] = useState<number>(0);
  const [isNextButtonDisabled, setIsNextButtonDisabled] = useState<boolean>(false);
  const [isPreviousButtonDisabled, setIsPreviousButtonDisabled] = useState<boolean>(true);
  const [typeOfPagination, setTypeOfPagination] = useState<string>('');

  useEffect(() => {
    if (typesOfPagination === 'category') {
      if (numberOfRows < 2) {
        setTypeOfPagination('Category');
      } else {
        setTypeOfPagination('Categories');
      }
    } else if (typesOfPagination === 'list') {
      if (numberOfRows < 2) {
        setTypeOfPagination('Item');
      } else {
        setTypeOfPagination('Items');
      }
    }
  }, [typesOfPagination, numberOfRows]);

  const handlePreviousPage = () => {
    if (selectedPage > 0) {
      setSelectedPage(selectedPage - 1);
    }

    previousPage();
    if (selectedPage * rowsPerPage <= numberOfRows) setIsNextButtonDisabled(false);

    if (selectedPage * rowsPerPage <= rowsPerPage) setIsPreviousButtonDisabled(true);
  };

  const handleNextPage = () => {
    if (selectedPage < numberOfRows / rowsPerPage - 1) {
      setSelectedPage(selectedPage + 1);
    }

    nextPage();
    if ((selectedPage + 2) * rowsPerPage >= numberOfRows) setIsNextButtonDisabled(true);

    if ((selectedPage + 1) * rowsPerPage >= rowsPerPage) setIsPreviousButtonDisabled(false);
  };

  return (
    <div>
      {`${(selectedPage === 0 ? '1 - ' : `${rowsPerPage * selectedPage + 1} - `) +
        ((selectedPage + 1) * rowsPerPage <= numberOfRows ? (selectedPage + 1) * rowsPerPage : numberOfRows)
        } of ${numberOfRows} ${typeOfPagination}`}
      <IconButton
        onClick={handlePreviousPage}
        disabled={isPreviousButtonDisabled}
        style={{ padding: '0.5rem' }}
      >
        {'<'}
      </IconButton>
      <IconButton onClick={handleNextPage} disabled={isNextButtonDisabled}>
        {'>'}
      </IconButton>
    </div>
  );
}
