import { IngredientUnit } from '../../Enums/IngredientUnit';

const parseUnits = (unit: IngredientUnit, value?: number): string => {
  if (unit === IngredientUnit.Gram) {
    return 'g';
  }

  if (unit === IngredientUnit.Kilogram) {
    return 'Kg';
  }

  if (unit === IngredientUnit.Pcs) {
    return 'Pcs';
  }

  if (unit === IngredientUnit.Mililiters) {
    return 'ml';
  }

  return 'L';
};

export default parseUnits;
