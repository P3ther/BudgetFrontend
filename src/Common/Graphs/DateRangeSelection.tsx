import { Dispatch, SetStateAction, useState } from 'react';
import { Button, ButtonGroup } from '@material-ui/core';
import './dateRangeSelection.css';
import moment from 'moment';
import DateTimeRange from '../../Interfaces/StartEndTime.interface';
import { GraphTimeValues } from '../../Enums/GraphEnums';

interface Props {
  isLoading: boolean;
  setGraphDateRange: Dispatch<SetStateAction<DateTimeRange>>;
  setDateDiff: Dispatch<SetStateAction<string>>;
}

export default function DateRangeSelection({ isLoading, setGraphDateRange, setDateDiff }: Props) {
  const [selectedValue, setSelectedValue] = useState<string>(GraphTimeValues.YTD);
  const handleChange = (newValue: string) => {
    setSelectedValue(newValue);
    if (newValue === GraphTimeValues.YTD) {
      setGraphDateRange({
        startDateTime: new Date(moment().utc(false).startOf('year').toString()),
        endDateTime: new Date(moment().utc(true).toString()),
      });
      setDateDiff(GraphTimeValues.YTD);
    } else if (newValue === GraphTimeValues.Month1) {
      setGraphDateRange({
        startDateTime: new Date(moment().utc(false).subtract(1, 'month').toString()),
        endDateTime: new Date(moment().utc(true).toString()),
      });
      setDateDiff(GraphTimeValues.Month1);
    } else if (newValue === GraphTimeValues.Months3) {
      setGraphDateRange({
        startDateTime: new Date(moment().utc(false).subtract(3, 'month').toString()),
        endDateTime: new Date(moment().utc(true).toString()),
      });
      setDateDiff(GraphTimeValues.Months3);
    } else if (newValue === GraphTimeValues.Months6) {
      setGraphDateRange({
        startDateTime: new Date(moment().utc(false).subtract(6, 'month').toString()),
        endDateTime: new Date(moment().utc(true).toString()),
      });
      setDateDiff(GraphTimeValues.Months6);
    } else if (newValue === GraphTimeValues.Year) {
      setGraphDateRange({
        startDateTime: new Date(moment().utc(false).subtract(1, 'year').toString()),
        endDateTime: new Date(moment().utc(true).toString()),
      });
      setDateDiff(GraphTimeValues.Year);
    }
  };

  return (
    <div className="ButtonGroupContainer">
      <ButtonGroup disableRipple disableElevation color="primary" disabled={isLoading}>
        <Button
          variant={selectedValue === GraphTimeValues.YTD ? 'contained' : 'outlined'}
          onClick={() => handleChange(GraphTimeValues.YTD)}
        >
          YTD
        </Button>
        <Button
          variant={selectedValue === GraphTimeValues.Month1 ? 'contained' : 'outlined'}
          onClick={() => handleChange(GraphTimeValues.Month1)}
        >
          1 Month
        </Button>
        <Button
          variant={selectedValue === GraphTimeValues.Months3 ? 'contained' : 'outlined'}
          onClick={() => handleChange(GraphTimeValues.Months3)}
        >
          3 Months
        </Button>
        <Button
          variant={selectedValue === GraphTimeValues.Months6 ? 'contained' : 'outlined'}
          onClick={() => handleChange(GraphTimeValues.Months6)}
        >
          6 Months
        </Button>
        <Button
          variant={selectedValue === GraphTimeValues.Year ? 'contained' : 'outlined'}
          onClick={() => handleChange(GraphTimeValues.Year)}
        >
          1 Year
        </Button>
      </ButtonGroup>
    </div>
  );
}
