/* Libraries */
import { Grid, Paper } from '@material-ui/core';
import moment from 'moment';
import { Dispatch, SetStateAction, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { DiscreteColorLegend, FlexibleWidthXYPlot, LineSeries, XAxis, YAxis } from 'react-vis';
/* Components */
import { GraphColors } from './GraphColors';
import { GraphTimeValues } from '../../Enums/GraphEnums';
/* Functions */
import { currencyPairsApi } from '../../Api/CurrencyPairsApi';
import { transferToDefaultCurrency } from '../Currency';
import { categoryApi } from '../../Api/CategoryApi';
import { parseMoneyChangeForDailyGraph, parseMoneyChangeForMonthlyGraph } from '../GraphMoneyChange';
/* Interfaces */
import Currency from '../../Interfaces/Currency.interface';
import DateTimeRange from '../../Interfaces/StartEndTime.interface';
import CurrencyPairs from '../../Interfaces/CurrencyPairs.interface';
import MoneyChange from '../../Interfaces/MoneyChange.interface';
import { CategoryInterface } from '../../Interfaces/Interfaces';
import Expense from '../../Interfaces/Expense.interface';
import { getDateTimeRange } from '../../Redux/Selectors';

interface Props {
  defaultCurrency: Currency;
  isLoading: boolean;
  setIsLoading: Dispatch<SetStateAction<boolean>>;
  graphDateRange: DateTimeRange;
  setGraphDateRange: Dispatch<SetStateAction<DateTimeRange>>;
  dateDiff: string;
}

interface GraphValue {
  values: any[];
}

interface Legend {
  title: string;
  color: string;
}

export default function CategoryGraph({
  defaultCurrency,
  isLoading,
  setIsLoading,
  graphDateRange,
  setGraphDateRange,
  dateDiff,
}: Props) {
  const [legendValues, setLegendValues] = useState<Legend[]>([]);
  const [graphValues, setGraphValues] = useState<GraphValue[]>([]);
  const dateRange = useSelector(getDateTimeRange);

  useEffect(() => {
    // Get categories for this graph after defaultCurrency is recieved.
    if (Object.keys(defaultCurrency).length !== 0) {
      getValues();
    }
  }, [defaultCurrency, dateRange]); // eslint-disable-line react-hooks/exhaustive-deps

  const getValues = async () => {
    setIsLoading(true);
    let currencyPairsTemp: CurrencyPairs[] = [];
    let categoriesTemp: CategoryInterface[] = [];
    const tempLegends: Legend[] = [];
    const tempGraphs: GraphValue[] = [];
    let tempGraphsValues: any[] = [];

    await categoryApi.getCategories().then((res) => {
      res.sort((a, b) => (a.id! < b.id! ? -1 : 1));
      categoriesTemp = res;
    });
    await currencyPairsApi.getCurrencyPairs().then((res) => (currencyPairsTemp = res));
    for (let i: number = 0; i < categoriesTemp.length; i++) {
      if (categoriesTemp[i].incomeList!.length > 0 || categoriesTemp[i].expenseList!.length > 0) {
        const tempLegendValue: Legend = {
          title: categoriesTemp[i].name,
          color: GraphColors[i],
        };

        tempLegends.push(tempLegendValue);
      }

      if (categoriesTemp[i].expenseList!.length > 0) {
        const defaultCurrencyExpenses: Expense[] = transferToDefaultCurrency(
          defaultCurrency,
          currencyPairsTemp,
          categoriesTemp[i].expenseList!,
        );

        if (dateDiff === GraphTimeValues.Month1) {
          tempGraphsValues = parseMoneyChangeForDailyGraph(defaultCurrencyExpenses, dateRange);
        } else {
          tempGraphsValues = parseMoneyChangeForMonthlyGraph(defaultCurrencyExpenses, dateRange);
        }

        tempGraphsValues.forEach((value) => {
          value.x = moment(value.x).format('DD MMM YYYY');
        });
        tempGraphs.push({ values: tempGraphsValues });
      } else if (categoriesTemp[i].incomeList!.length > 0) {
        const defaultCurrencyIncomes: MoneyChange[] = transferToDefaultCurrency(
          defaultCurrency,
          currencyPairsTemp,
          categoriesTemp[i].incomeList!,
        );

        if (dateDiff === GraphTimeValues.Month1) {
          tempGraphsValues = parseMoneyChangeForDailyGraph(defaultCurrencyIncomes, dateRange);
        } else {
          tempGraphsValues = parseMoneyChangeForMonthlyGraph(defaultCurrencyIncomes, dateRange);
        }

        tempGraphsValues.forEach((value) => {
          value.x = moment(value.x).format('DD MMM YYYY');
        });
        tempGraphs.push({ values: tempGraphsValues });
      }
    }

    setLegendValues(tempLegends);
    setGraphValues(tempGraphs);
    setIsLoading(false);
  };

  return (
    <Paper style={{ marginTop: '20px', width: '70vw' }}>
      <Grid container>
        <Grid item xs={10}>
          <FlexibleWidthXYPlot margin={{ left: 75, bottom: 50 }} height={500} xType="ordinal">
            <XAxis tickLabelAngle={-30} />
            <YAxis orientation="left" />
            {graphValues.map((value: GraphValue, key: number) => (
              <LineSeries
                curve="curveMonotoneX"
                stroke={legendValues[key].color}
                data={value.values}
                opacity={1}
                strokeStyle="solid"
              />
            ))}
          </FlexibleWidthXYPlot>
        </Grid>
        <Grid item xs={2} style={{ margin: 'auto', color: '#FFF' }}>
          <DiscreteColorLegend items={legendValues} />
        </Grid>
      </Grid>
    </Paper>
  );
}
