import { Dispatch, SetStateAction, useEffect, useState } from 'react';
import { Grid, Paper } from '@material-ui/core';
import { DiscreteColorLegend, FlexibleWidthXYPlot, LineSeries, XAxis, YAxis } from 'react-vis';

import Currency from '../../Interfaces/Currency.interface';
import DateTimeRange from '../../Interfaces/StartEndTime.interface';
import CurrencyPairs from '../../Interfaces/CurrencyPairs.interface';
import MoneyChange from '../../Interfaces/MoneyChange.interface';
import { expenseApi } from '../../Api/ExpenseApi';
import { currencyPairsApi } from '../../Api/CurrencyPairsApi';
import { transferToDefaultCurrency } from '../Currency';
import { incomeApi } from '../../Api/IncomeApi';
import {
  parseMoneyChangeForDailyGraph,
  parseMoneyChangeForMonthlyGraph,
  parseMoneyChangeForWeeklyGraph,
} from '../GraphMoneyChange';
import { GraphTimeValues } from '../../Enums/GraphEnums';

interface Props {
  defaultCurrency: Currency;
  isLoading: boolean;
  setIsLoading: Dispatch<SetStateAction<boolean>>;
  graphDateRange: DateTimeRange;
  setGraphDateRange: Dispatch<SetStateAction<DateTimeRange>>;
  dateDiff: string;
}

export default function IncomesExpensesGraph({
  defaultCurrency,
  isLoading,
  setIsLoading,
  graphDateRange,
  setGraphDateRange,
  dateDiff,
}: Props) {
  // const [currencyPairs, setCurrencyPairs] = useState<CurrencyPairs[]>([]);
  // const [graphExpenses, setGraphExpenses] = useState<MoneyChange[]>([]);
  // const [graphIncomes, setGraphIncomes] = useState<MoneyChange[]>([]);
  const [graphExpensesValues, setGraphExpensesValues] = useState<any[] | undefined>(undefined);
  const [graphIncomesValues, setGraphIncomesValues] = useState<any[] | undefined>(undefined);

  useEffect(() => {
    // Get Incomes/Expenses values for this graph after defaultCurrency is recieved.
    if (Object.keys(defaultCurrency).length !== 0) {
      getValues();
    }
  }, [defaultCurrency, graphDateRange]); // eslint-disable-line react-hooks/exhaustive-deps

  const getValues = async () => {
    setIsLoading(true);
    let currencyPairsTemp: CurrencyPairs[] = [];
    let expensesTemp: MoneyChange[] = [];
    let incomesTemp: MoneyChange[] = [];

    await expenseApi.getExpensesByTime(graphDateRange).then((res) => (expensesTemp = res));
    await incomeApi.getIncomesByTime(graphDateRange).then((res) => (incomesTemp = res));
    await currencyPairsApi.getCurrencyPairs().then((res) => (currencyPairsTemp = res));
    const defaultCurrencyExpensesTemp: MoneyChange[] = transferToDefaultCurrency(
      defaultCurrency,
      currencyPairsTemp,
      expensesTemp,
    );
    const defaultCurrencyIncomesTemp: MoneyChange[] = transferToDefaultCurrency(
      defaultCurrency,
      currencyPairsTemp,
      incomesTemp,
    );

    // setGraphExpenses(defaultCurrencyExpensesTemp);
    // setGraphIncomes(defaultCurrencyIncomesTemp);
    if (dateDiff === GraphTimeValues.Month1) {
      setGraphExpensesValues(parseMoneyChangeForDailyGraph(defaultCurrencyExpensesTemp, graphDateRange));
      setGraphIncomesValues(parseMoneyChangeForDailyGraph(defaultCurrencyIncomesTemp, graphDateRange));
    } else if (dateDiff === GraphTimeValues.Months3) {
      setGraphExpensesValues(parseMoneyChangeForWeeklyGraph(defaultCurrencyExpensesTemp, graphDateRange));
      setGraphIncomesValues(parseMoneyChangeForWeeklyGraph(defaultCurrencyIncomesTemp, graphDateRange));
    } else {
      setGraphExpensesValues(parseMoneyChangeForMonthlyGraph(defaultCurrencyExpensesTemp, graphDateRange));
      setGraphIncomesValues(parseMoneyChangeForMonthlyGraph(defaultCurrencyIncomesTemp, graphDateRange));
    }

    setIsLoading(false);
  };

  return (
    <Paper style={{ marginTop: '20px', width: '70vw' }}>
      <Grid container>
        <Grid item xs={10}>
          <FlexibleWidthXYPlot margin={{ left: 75, bottom: 50 }} height={500} xType="ordinal">
            <XAxis tickLabelAngle={-30} />
            <YAxis orientation="left" />
            <LineSeries
              curve="curveMonotoneX"
              stroke="#43aa8b"
              data={graphIncomesValues}
              opacity={1}
              strokeStyle="solid"
            />
            <LineSeries
              curve="curveMonotoneX"
              stroke="#f9c74f"
              data={graphExpensesValues}
              opacity={1}
              strokeStyle="solid"
            />
          </FlexibleWidthXYPlot>
        </Grid>
        <Grid item xs={2} style={{ margin: 'auto', color: '#FFF' }}>
          <DiscreteColorLegend
            items={[
              { title: 'Incomes', color: '#43aa8b' },
              { title: 'Expenses', color: '#f9c74f' },
            ]}
          />
        </Grid>
      </Grid>
    </Paper>
  );
}
