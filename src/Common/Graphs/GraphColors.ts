export const GraphColors: string[] = [
  '#ffd500',
  '#00296b',
  '#e36414',
  '#c1121f',
  '#540d6e',
  '#119da4',
  '#8ac926',
  '#815ac0',
];
