import Currency from '../Interfaces/Currency.interface';
import CurrencyPairs from '../Interfaces/CurrencyPairs.interface';
import MoneyChange from '../Interfaces/MoneyChange.interface';

export const transferToDefaultCurrency = (
  defaultCurrency: Currency,
  currencyPairs: CurrencyPairs[],
  values: MoneyChange[],
) /*: MoneyChange[] */ => {
  const returnedValues: MoneyChange[] = [];

  values.forEach((value) => {
    if (value.currencyId !== defaultCurrency.id) {
      currencyPairs.forEach((pair) => {
        if (pair.firstCurrencyId === value.currencyId && pair.secondCurrencyId === defaultCurrency.id) {
          // if we are transfering from Eur to Czk 25 rate price * rate
          let tempValue: MoneyChange = {} as MoneyChange;

          tempValue = { ...value };
          tempValue.price = Math.round(pair.exchangeRate * value.price * 100) / 100;
          tempValue.currencyId = defaultCurrency.id;
          tempValue.currencySign = defaultCurrency.sign;
          returnedValues.push(tempValue);
        } else if (
          pair.secondCurrencyId === value.currencyId &&
          pair.firstCurrencyId === defaultCurrency.id
        ) {
          // if we are transfering from Czk to Eur 25 rate price * (1 / rate)
          let tempValue: MoneyChange = {} as MoneyChange;

          tempValue = { ...value };
          tempValue.price = Math.round((1 / pair.exchangeRate) * value.price * 100) / 100;
          tempValue.currencyId = defaultCurrency.id;
          tempValue.currencySign = defaultCurrency.sign;
          returnedValues.push(tempValue);
        }
      });
    } else {
      // currency is same to default no need to calculate
      let tempValue: MoneyChange = {} as MoneyChange;

      tempValue = { ...value };
      returnedValues.push(tempValue);
    }
  });

  return returnedValues;
};
