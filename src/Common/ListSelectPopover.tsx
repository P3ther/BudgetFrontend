import { useState } from 'react';
import { Typography, Popover, CircularProgress } from '@material-ui/core';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import { categoryApi } from '../Api/CategoryApi';
import { CategoryInterface } from '../Interfaces/Interfaces';

interface Props {
  anchorEl: HTMLElement | null;
  handleClose: Function;
  open: boolean;
  type: string;
  filterByCategory: Function;
  selectedCategories: number[];
  clearFilterCategory: () => void;
}
export default function ListSelectPopover({
  anchorEl,
  handleClose,
  open,
  type,
  filterByCategory,
  selectedCategories,
  clearFilterCategory,
}: Props) {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [categories, setCategories] = useState<CategoryInterface[]>([]);

  const handleEnter = () => {
    if (type === 'Income') {
      categoryApi
        .getIncomeCategories()
        .then((res) => {
          setCategories(res);
          setIsLoading(false);
        })
        .catch((err) => console.warn(err));
    } else if (type === 'Expense') {
      categoryApi
        .getExpenseCategories()
        .then((res) => {
          setCategories(res);
          setIsLoading(false);
        })
        .catch((err) => console.warn(err));
    }
  };

  return (
    <>
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={() => handleClose()}
        onEnter={handleEnter}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <div style={{ width: '17vh', minHeight: '4vh', textAlign: 'center' }}>
          {categories.map((row: CategoryInterface) => (
            <Typography onClick={() => filterByCategory(row.id!)}>
              {selectedCategories.filter((category) => category === row.id).length > 0 ? (
                <CheckBoxIcon
                  style={{ verticalAlign: 'middle', display: 'inline-block', marginRight: '10px' }}
                />
              ) : (
                <CheckBoxOutlineBlankIcon
                  style={{ verticalAlign: 'middle', display: 'inline-block', marginRight: '10px' }}
                />
              )}

              <span style={{ verticalAlign: 'middle', display: 'inline-block' }}>{row.name}</span>
            </Typography>
          ))}
          {isLoading ? (
            <CircularProgress style={{ width: '3vh', height: '3vh', margin: 'auto' }} />
          ) : (
            <Typography onClick={() => clearFilterCategory()}>Clear</Typography>
          )}
        </div>
      </Popover>
    </>
  );
}
