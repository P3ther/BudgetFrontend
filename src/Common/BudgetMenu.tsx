import { Dispatch, SetStateAction } from 'react';
import { Menu, MenuItem } from '@material-ui/core';
import { Link } from 'react-router-dom';
import BudgetMenuInterface from '../Interfaces/BudgetMenu.interface';
import './Styles/BudgetMenu.css';

interface Props {
  menuAnchor: HTMLElement | null;
  setMenuAnchor: Dispatch<SetStateAction<HTMLElement | null>>;
  options: BudgetMenuInterface[];
}

export default function BudgetMenu({ menuAnchor, setMenuAnchor, options }: Props) {
  const isOpened = Boolean(menuAnchor);
  const handleClose = () => {
    setMenuAnchor(null);
  };

  return (
    <>
      <Menu
        anchorEl={menuAnchor}
        open={isOpened}
        onClose={handleClose}
        transformOrigin={{ horizontal: 'center', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'center', vertical: 'center' }}
      >
        {options.map((value: BudgetMenuInterface, key: number) =>
          value.isLink ? (
            <Link to={value.linkAddres!} className="budget-menu-link">
              <MenuItem className="budget-menu-item">{value.name}</MenuItem>
            </Link>
          ) : (
            <MenuItem className="budget-menu-item" onClick={value.onClickFunction!}>
              {value.name}
            </MenuItem>
          ),
        )}
      </Menu>
    </>
  );
}
