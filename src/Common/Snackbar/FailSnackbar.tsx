import { Snackbar, Slide } from '@material-ui/core';
import './Snackbar.css';
import { SnackbarMessage } from './SnackbarMessage';

interface Props {
	isOpened: boolean;
	setIsOpened: Function;
	messageText: string;
}
export default function FailSnackbar({ isOpened, setIsOpened, messageText }: Props) {
	return (
		<Snackbar
			anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
			open={isOpened}
			autoHideDuration={5000}
			disableWindowBlurListener
			onClose={() => setIsOpened(false)}
			message={< SnackbarMessage messageText={messageText} type={"fail"} />}
			TransitionComponent={Slide}
			ClickAwayListenerProps={{ mouseEvent: false }}
			ContentProps={{ className: 'fail-Snackbar' }}
		/>
	);
}
