//

import { Snackbar, Slide } from '@material-ui/core';
import { SnackbarMessage } from './SnackbarMessage';

import './Snackbar.css';

interface Props {
    isOpened: boolean;
    setIsOpened: Function;
    messageText: string;
}

export default function WarningSnackbar({ isOpened, setIsOpened, messageText }: Props) {

    return (
        <Snackbar
            anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
            open={isOpened}
            autoHideDuration={5000}
            disableWindowBlurListener
            onClose={() => setIsOpened(false)}
            message={<SnackbarMessage messageText={messageText} type={"warning"} />}
            TransitionComponent={Slide}
            ClickAwayListenerProps={{ mouseEvent: false }}
            ContentProps={{ className: 'warning-Snackbar' }}
        />
    );
}
