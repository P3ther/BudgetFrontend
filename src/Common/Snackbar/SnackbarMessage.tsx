import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';

interface snackBarMessageProps {
  messageText: string;
  type: string;
}

export const SnackbarMessage = ({ messageText, type }: snackBarMessageProps) => (
  <>
    {type === 'success' && <CheckCircleOutlineIcon className="BudgetIcon" />}
    {type === 'fail' && <HighlightOffIcon className="BudgetIcon" />}
    {type === 'warning' && <ErrorOutlineIcon className="BudgetIcon" />}
    <div style={{ display: 'inline', height: '100%', fontSize: '1.5vh', color: '#e4e6eb' }}>
      {messageText}
    </div>
  </>
);
