import { Snackbar, Slide } from '@material-ui/core';
import { Dispatch, SetStateAction } from 'react';
import { SnackbarMessage } from './SnackbarMessage';
/* Styles */
import './SuccessSnackbar.scss';

interface Props {
  isOpened: boolean;
  setIsOpened: Dispatch<SetStateAction<boolean>>;
  messageText: string;
}

export default function SuccessSnackbar({ isOpened, setIsOpened, messageText }: Props) {
  return (
    <Snackbar
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      open={isOpened}
      autoHideDuration={5000}
      disableWindowBlurListener
      onClose={() => setIsOpened(false)}
      message={<SnackbarMessage messageText={messageText} type={'success'} />}
      TransitionComponent={Slide}
      ClickAwayListenerProps={{ mouseEvent: false }}
      ContentProps={{ className: 'success-snackbar-container' }}
    />
  );
}
