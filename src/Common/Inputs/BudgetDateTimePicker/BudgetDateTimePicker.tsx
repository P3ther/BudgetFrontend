import React, { FC } from 'react';
import { InputLabel, TextField, FormHelperText } from '@mui/material';
import { Controller, useFormContext } from 'react-hook-form';
import moment from 'moment';
import BudgetDateTimePickerProps from './BudgetDateTimePicker.types';

const BudgetDateTimePicker: FC<BudgetDateTimePickerProps> = ({
  elementName,
  fullWidth,
  defaultValue,
  title,
}) => {
  const { register, errors } = useFormContext();
  const error = errors[elementName];

  return (
    <>
      <InputLabel error={!!error}>{title}</InputLabel>
      <Controller
        as={<TextField fullWidth={fullWidth} type="datetime-local" size="small" />}
        ref={register}
        name={elementName}
        variant="outlined"
        defaultValue={moment(defaultValue).format('YYYY-MM-DDTHH:mm')}
        error={error}
      />
      {error && <FormHelperText error>{error.message}</FormHelperText>}
    </>
  );
};

export default BudgetDateTimePicker;
