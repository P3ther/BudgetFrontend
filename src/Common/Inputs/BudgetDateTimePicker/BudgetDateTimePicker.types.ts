interface BudgetDateTimePickerProps {
    elementName: string;
    fullWidth?: boolean;
    defaultValue?: Date;
    title: string;
}

export default BudgetDateTimePickerProps;
