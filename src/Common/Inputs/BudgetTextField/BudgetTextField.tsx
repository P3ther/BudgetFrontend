import React, { FC } from 'react';
import { InputLabel, TextField } from '@mui/material';
import { Controller, useFormContext } from 'react-hook-form';

interface Props {
  elementName: string;
  fullWidth?: boolean;
  defaultValue?: string;
  title: string;
  type?: 'text' | 'number';
}

const BudgetTextField: FC<Props> = ({ elementName, fullWidth, defaultValue, title, type }: Props) => {
  const { register, errors } = useFormContext();

  return (
    <>
      <InputLabel error={!!errors[elementName]}>{title}</InputLabel>
      <Controller
        as={<TextField fullWidth={fullWidth} size="small" />}
        ref={register}
        name={elementName}
        variant="outlined"
        type={type}
        error={errors[elementName]}
        helperText={errors[elementName]?.message ? errors[elementName].message : ''}
        defaultValue={defaultValue}
      />
    </>
  );
};

export default BudgetTextField;
