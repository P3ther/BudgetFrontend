import React, { FC } from 'react';
import { InputLabel, TextField, MenuItem, FormHelperText } from '@mui/material';
import { Controller, useFormContext } from 'react-hook-form';

export interface BudgetOption {
  id: string;
  name: string;
}

interface Props {
  options: BudgetOption[];
  elementName: string;
  fullWidth?: boolean;
  defaultValue?: string;
  title: string;
  type?: 'text' | 'number';
}

const BudgetSelect: FC<Props> = ({
  options,
  elementName,
  fullWidth,
  defaultValue,
  title,
  type = 'text',
}: Props) => {
  const { register, errors } = useFormContext();
  const error = errors[elementName];

  return (
    <>
      <InputLabel error={!!error}>{title}</InputLabel>
      <Controller
        as={
          <TextField fullWidth={fullWidth} select size="small">
            {options.map((option: BudgetOption) => (
              <MenuItem key={option.id} value={option.id}>
                {option.name}
              </MenuItem>
            ))}
          </TextField>
        }
        ref={register}
        name={elementName}
        variant="outlined"
        type={type}
        error={error}
        helperText={error?.message || ''}
        defaultValue={defaultValue}
      />
      {error && <FormHelperText error>{error.message}</FormHelperText>}
    </>
  );
};

export default BudgetSelect;
