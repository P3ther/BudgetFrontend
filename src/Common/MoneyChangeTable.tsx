import React, { ChangeEvent, useEffect, useState } from 'react';
import './MoneyChangeTable.css';
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Paper,
  IconButton,
  Tooltip,
  Grid,
  Select,
  MenuItem,
  Typography,
} from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import Moment from 'react-moment';
import EditIcon from '@material-ui/icons/Edit';
import { useSelector } from 'react-redux';
import MoneyChange from '../Interfaces/MoneyChange.interface';
import ListSelectPopover from './ListSelectPopover';
import EditExpense from '../Dialogs/EditExpense';
import EditIncome from '../Dialogs/EditIncome';
import BudgetPagination from './BudgetPagination';
import Currency from '../Interfaces/Currency.interface';
import { parseUnitAndCurrency } from './CurrencyHelper';
import { CategoryInterface } from '../Interfaces/Interfaces';
import { getDateTimeRange } from '../Redux/Selectors';

interface Props {
  type: string;
  isCategoryFilterable: boolean;
  categoryId?: number;
  tableValues: MoneyChange[];
  allCategories: CategoryInterface[];
  currencies: Currency[];
}

export default function MoneyChangeTable({
  type,
  isCategoryFilterable,
  categoryId,
  tableValues,
  allCategories,
  currencies,
}: Props) {
  const [displayedRows, setDisplayedRows] = useState<MoneyChange[]>([] as MoneyChange[]);
  const [isEditIncomeDialogOpened, setIsEditIncomeDialogOpened] = useState<boolean>(false);
  const [isEditExpenseialogOpened, setIsEditExpenseDialogOpened] = useState<boolean>(false);
  // const [rows, setRows] = useState<MoneyChange[]>([]);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [selectedCategories, setSelectedCategories] = useState<number[]>([]);
  const openSelectPopover = Boolean(anchorEl);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [editedMoneyChange, setEditedMoneyChange] = useState<MoneyChange>({} as MoneyChange);
  const dateRange = useSelector(getDateTimeRange);

  const handleListSelectPopoverOpen = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleListSelectPopoverClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    if (categoryId) {
      const tempArray: MoneyChange[] = [];

      tableValues.forEach((value) => {
        if (value.category === categoryId) {
          tempArray.push(value);
        }
      });
      setDisplayedRows(tempArray);
    } else {
      setDisplayedRows(tableValues);
    }
  }, [dateRange, type, categoryId, tableValues]);

  const handleNextPage = () => {
    setPage(page + 1);
  };

  const handlePreviousPage = () => {
    setPage(page - 1);
  };

  const handleChangeRowsPerPage = (event: ChangeEvent<any>) => {
    setRowsPerPage(event.target.value);
    setPage(0);
  };

  const filterByCategory = (categoryId: number) => {
    const newCategoriesArray = selectedCategories.filter((category) => category === categoryId);
    let tempCategoryIdArray: number[] = [];

    if (newCategoriesArray.length > 0) {
      tempCategoryIdArray = [...selectedCategories];
      const index = tempCategoryIdArray.indexOf(categoryId);

      tempCategoryIdArray.splice(index, 1);
      setSelectedCategories(tempCategoryIdArray);
    } else {
      tempCategoryIdArray = [...selectedCategories];
      tempCategoryIdArray.push(categoryId);
      setSelectedCategories(tempCategoryIdArray);
    }

    const newArray: MoneyChange[] = [];

    tempCategoryIdArray.forEach((id) => {
      const tempArray: MoneyChange[] = tableValues.filter((row) => row.category === id);

      tempArray.forEach((row) => newArray.push(row));
    });
    if (tempCategoryIdArray.length > 0) {
      setDisplayedRows(newArray);
    } else {
      setDisplayedRows(tableValues);
    }
  };
  const clearFilterCategory = () => {
    setSelectedCategories([]);
    setDisplayedRows(tableValues);
  };
  const handleOpenEditExpense = (key: number) => {
    const row: MoneyChange[] = tableValues.filter((row) => row.id === key);

    setEditedMoneyChange(row[0]);
    setIsEditExpenseDialogOpened(true);
  };

  const handleOpenEditIncome = (key: number) => {
    const row: MoneyChange[] = tableValues.filter((row) => row.id === key);

    setEditedMoneyChange(row[0]);
    setIsEditIncomeDialogOpened(true);
  };

  return (
    <Paper>
      <TableContainer>
        <ListSelectPopover
          anchorEl={anchorEl}
          handleClose={handleListSelectPopoverClose}
          open={openSelectPopover}
          type={type}
          filterByCategory={filterByCategory}
          selectedCategories={selectedCategories}
          clearFilterCategory={clearFilterCategory}
        />
        <EditExpense
          isEditExpenseialogOpened={isEditExpenseialogOpened}
          setIsEditExpenseDialogOpened={setIsEditExpenseDialogOpened}
          expense={editedMoneyChange}
          defaultTimes={dateRange}
          allCategories={allCategories}
          currencies={currencies}
        />
        <EditIncome
          isEditIncomeDialogOpened={isEditIncomeDialogOpened}
          setIsEditIncomeDialogOpened={setIsEditIncomeDialogOpened}
          income={editedMoneyChange}
          defaultTimes={dateRange}
          allCategories={allCategories}
          currencies={currencies}
        />
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell align="center" className="moneyChangeTableDate">
                Date/Time
              </TableCell>
              <TableCell align="center" className="moneyChangeTableName">
                Name
              </TableCell>
              <TableCell align="center" className="moneyChangeTablePrice">
                Price
              </TableCell>
              {!isCategoryFilterable ? (
                <TableCell align="center" className="moneyChangeTableCategory">
                  Category
                </TableCell>
              ) : (
                <TableCell
                  align="center"
                  onClick={handleListSelectPopoverOpen}
                  className="moneyChangeTableCategory pointerCursor"
                >
                  <span style={{ verticalAlign: 'middle', display: 'inline-block' }}>Category</span>
                  {anchorEl === null ? (
                    <ArrowDropDownIcon style={{ verticalAlign: 'middle', display: 'inline-block' }} />
                  ) : (
                    <ArrowDropUpIcon style={{ verticalAlign: 'middle', display: 'inline-block' }} />
                  )}
                </TableCell>
              )}
              <TableCell align="center" className="moneyChangeTableEdit">
                Edit
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {(displayedRows !== [] ? displayedRows : tableValues)
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row: MoneyChange, key: number) => (
                <TableRow key={row.id} style={{ height: '50px', overflow: 'hidden' }}>
                  <TableCell align="center" className="moneyChangeTableDate">
                    <Moment format="DD.MM.YYYY kk:mm" utc local>
                      {row.date}
                    </Moment>
                  </TableCell>
                  <TableCell align="center" className="moneyChangeTableName">
                    <Tooltip title={row.name} placement="top">
                      <span className="moneyChangeTableTextRow">{row.name}</span>
                    </Tooltip>
                  </TableCell>
                  <TableCell align="center" className="moneyChangeTablePrice">
                    {parseUnitAndCurrency(row.price, row.currencySign)}
                  </TableCell>
                  <TableCell align="center" className="moneyChangeTableCategory">
                    <Tooltip title={row.categoryName ? row.categoryName : ''} placement="top">
                      <span className="moneyChangeTableTextRow">{row.categoryName}</span>
                    </Tooltip>
                  </TableCell>
                  <TableCell align="center" className="moneyChangeTableEdit">
                    <IconButton
                      onClick={
                        type === 'Income'
                          ? () => handleOpenEditIncome(row.id)
                          : () => handleOpenEditExpense(row.id)
                      }
                    >
                      <EditIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
        <Grid container direction="row" justify="flex-end" alignItems="center">
          <Typography variant="body2" component="span">
            Rows Per Page:{' '}
          </Typography>
          <Select
            value={rowsPerPage}
            onChange={handleChangeRowsPerPage}
            style={{ marginRight: '25px', border: 'none' }}
          >
            <MenuItem value={5}>Five</MenuItem>
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
          </Select>
          <BudgetPagination
            numberOfRows={tableValues.length}
            rowsPerPage={rowsPerPage}
            nextPage={handleNextPage}
            previousPage={handlePreviousPage}
            typesOfPagination="list"
          />
        </Grid>
      </TableContainer>
    </Paper>
  );
}
