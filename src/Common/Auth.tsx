import jwt from 'jwt-decode';

interface Token {
  sub: string;
  exp: number;
}

export const getUser = () => {
  const userStr = localStorage.getItem('user');

  if (userStr) return JSON.parse(userStr);

  return null;
};

export const getToken = () => localStorage.getItem('token') || null;

export const isAuthenticated = () => {
  const tokenUnd: string | null = localStorage.getItem('token')?.substr(7) || null;
  let token: Token | null = null;

  if (tokenUnd !== null) {
    token = jwt(tokenUnd);
  }

  const tokenExp: number | undefined = token?.exp;
  const date: number = Date.now() / 1000;

  if (tokenExp! > date && token !== null) {
    return true;
  }

  removeUserSession();

  return false;
};

// set the token and user from the session storage
export const setUserSession = (token: string, user: string) => {
  localStorage.setItem('token', token);
  localStorage.setItem('user', JSON.stringify(user));
};

export const setLocalUsername = (user: string) => {
  localStorage.setItem('user', JSON.stringify(user));
};

export const removeUserSession = () => {
  localStorage.removeItem('token');
  localStorage.removeItem('user');
};

// Store User Theme
export const storeUserTheme = (themeFilename: string) => {
  localStorage.setItem('theme', themeFilename);
};

export const retrieveUserTheme = () => localStorage.getItem('theme') || '';

export const removeUserTheme = () => {
  localStorage.removeItem('theme');
};
