export const parseUnitAndCurrency = (value: number | undefined, sign: string | undefined): string => {
  let returnedValue: string = '';

  if (value !== undefined) {
    const roundedValue = Math.round((value + Number.EPSILON) * 100) / 100;

    returnedValue = `${roundedValue
      .toString()
      .split('')
      .reverse()
      .join('')
      .replace(/([0-9]{3})/g, '$1 ')
      .split('')
      .reverse()
      .join('')} ${sign}`;
  }

  return returnedValue;
};
