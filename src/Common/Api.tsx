import { AxiosResponse } from 'axios';
import { BudgetInterface } from '../Interfaces/Interfaces';
import Expense, { AddExpense } from '../Interfaces/Expense.interface';
import Income from '../Interfaces/Income.interface';
import User from '../Interfaces/User.interface';
import DateTimeRange from '../Interfaces/StartEndTime.interface';
import ChangePassword from '../Interfaces/ChangePassword.interface';
import { axiosInstance } from '../Api/AxiosInstance';
import Theme from '../Interfaces/Theme.interface';

enum EndPoints {
  login = 'login',
  budget = 'budget/',
  expense = 'expense/',
  income = 'income/',
  timedIncomes = 'income/time/',
  timedAndCategoryIncomes = 'income/time/category/',
  timedAndCategoryExpenses = 'expense/time/category/',
  editIncome = 'income/edit/',
  deleteIncome = 'income/delete/',
  editExpense = 'expense/edit/',
  deleteExpense = 'expense/delete/',
  changePassword = 'user/changePassword/',
  getInvestmentPlatforms = 'investmentPlatform/',
  investment = 'investment/',
  investmentData = 'investmentData/',
  themes = 'themes/',
}

export const api = {
  login: (user: User) => axiosInstance.post(EndPoints.login, user).then((res) => res),
  changePassword: (changePassword: ChangePassword) =>
    axiosInstance.post(EndPoints.changePassword, changePassword),
  getBudget: (year: number, month: number) =>
    axiosInstance.get<BudgetInterface>(`${EndPoints.budget + year}/${month}/`).then((res) => res.data),
  getTimedBudget: (startEndTime: DateTimeRange) =>
    axiosInstance.post<BudgetInterface>(EndPoints.budget, startEndTime).then((res) => res.data),
  getExpense: (id: string) => axiosInstance.get<string>(`${EndPoints.expense + id}/`).then((res) => res.data),
  addExpense: (expense: AddExpense) =>
    axiosInstance.post(EndPoints.expense, expense).then((res: AxiosResponse<Expense>) => res.data),
  addIncome: (income: Income): Promise<AxiosResponse<Income>> => axiosInstance.post(EndPoints.income, income),
  getTimedIncomes: (times: DateTimeRange) =>
    axiosInstance.post<Income[]>(EndPoints.timedIncomes, times).then((res) => res.data),
  getTimedAndCategoryIncomes: (times: DateTimeRange, categoryId: number) =>
    axiosInstance
      .post<Income[]>(`${EndPoints.timedAndCategoryIncomes + categoryId}/`, times)
      .then((res) => res.data),
  getTimedAndCategoryExpenses: (times: DateTimeRange, categoryId: number) =>
    axiosInstance
      .post<Expense[]>(`${EndPoints.timedAndCategoryExpenses + categoryId}/`, times)
      .then((res) => res.data),
  updateIncome: (income: Income) =>
    axiosInstance.post<Income>(EndPoints.editIncome, income).then((res) => res.data),
  deleteIncome: (income: Income) => axiosInstance.post<Income>(EndPoints.deleteIncome, income),
  updateExpense: (expense: Expense) =>
    axiosInstance.post<Expense>(EndPoints.editExpense, expense).then((res) => res.data),
  deleteExpense: (expense: Expense) => axiosInstance.post(EndPoints.deleteExpense, expense),
  getThemes: () => axiosInstance.get<Theme[]>(EndPoints.themes).then((res) => res.data),
};
