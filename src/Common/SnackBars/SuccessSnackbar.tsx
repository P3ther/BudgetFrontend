import { Slide, Snackbar } from '@material-ui/core';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import { ReducerActionsEnum } from '../../Redux/ReduxStore';
import { SnackbarMessage } from '../Snackbar/SnackbarMessage';
import './SuccessSnackbar.css';

interface Props { }

export default function SuccessSnackbar(props: Props) {
  const dispatch = useDispatch();
  const succesSnackbar: boolean = useSelector((state: RootStateOrAny) => state.successSnackbar);
  const succesSnackbarMessage: string = useSelector((state: RootStateOrAny) => state.succesSnackbarMessage);

  const setIsOpened = (isOpened: boolean) => {
    dispatch({
      type: ReducerActionsEnum.successSnackbar,
      payload: isOpened,
    });
  };

  return (
    <Snackbar
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      open={succesSnackbar}
      autoHideDuration={5000}
      disableWindowBlurListener
      onClose={() => setIsOpened(false)}
      message={<SnackbarMessage messageText={succesSnackbarMessage} type="success" />}
      TransitionComponent={Slide}
      ClickAwayListenerProps={{ mouseEvent: false }}
      ContentProps={{ className: 'success-snackbar-container' }}
    />
  );
}
