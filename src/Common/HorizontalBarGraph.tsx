interface Props {
  width?: number;
  percentage: number;
  barColor: string;
  color: string;
}

export default function HorizontalBarGraph({ width, percentage, barColor, color }: Props) {
  return (
    <div
      style={{
        width: width !== undefined ? `${width}vw` : '',
        height: '30px',
        backgroundColor: color,
        borderRadius: '1vh',
      }}
    >
      <div
        style={{
          height: '100%',
          width: `${percentage.toFixed(2)}%`,
          maxWidth: '100%',
          backgroundColor: barColor,
          borderRadius: '1vh',
        }}
      />
    </div>
  );
}
