/// <reference types="cypress" />
import commonPage from '../common.page';

export const waitforCommonPage = () => {
  cy.intercept('GET', '**/properties/').as('userProperties');
  cy.url().should('equal', Cypress.config('baseUrl')); 
  cy.findByTestId(commonPage.locators.loadingOverlay).should('not.exist');
  cy.wait('@userProperties');
  // cy.findByTestId(commonPage.locators.budgetLogo);
  // cy.findByTestId();
};
