const loginPage = {
  locators: {
    usernameInput: 'username-input-field',
    usernameInputError: 'username-input-error',
    passwordInput: 'password-input-field',
    loginButton: 'login-button',
  },
  messages: {
    incorrectCredentials: 'Username or password is incorrect.',
  },
};

export default loginPage;
