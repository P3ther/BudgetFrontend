import { waitforCommonPage } from './helpers/common.helper';

const commonPage = {
  locators: {
    budgetLogo: 'budget-logo',
    budgetAppbarIcons: 'budget-appbar-icons',
    loadingOverlay: 'loading-overlay',
  },
  methods: {
    waitforCommonPage: () => waitforCommonPage(),
  },
};

export default commonPage;
