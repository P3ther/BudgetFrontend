import axios from 'axios';
import { backEndAddress } from './Variables';
import User from '../../src/Interfaces/User.interface';


export const createUser = async (user: User) => {
    await axios.post(`${backEndAddress}sign-up/`, user).then((res) => res.data);
};

export const deleteUser = async (user: User) => {
  const adminUser : User = { username: Cypress.env('ADMIN_USERNAME'), password: Cypress.env('ADMIN_PASSWORD') };
  const adminToken: string = await axios.post(`${backEndAddress}login`, adminUser)
    .then((res) => res.headers.authorization);
  const headers = {
    'Content-Type': 'application/json',
    'Authorization': adminToken,
  };
  axios.delete(`${backEndAddress}user/delete/`, { headers, data: { username: user.username }})
    .then((res) => res.data);
};
