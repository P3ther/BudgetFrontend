import User from "../../src/Interfaces/User.interface";
import { createUser, deleteUser } from "./User";

const apiCalls = {
    User: {
        createUser: (user: User) => createUser(user),
        deleteUser: (user: User) => deleteUser(user),
    }
}


export default apiCalls;