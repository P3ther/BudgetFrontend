import User from "../../src/Interfaces/User.interface";
import faker from '@faker-js/faker';

export const getUser = (email: string = ''): User => {
  const firstName = faker.name.firstName();
  const lastName = faker.name.lastName();

  return {
    username: firstName + lastName,
    userEmail: email || getFakeEmail(firstName, lastName),
    password: 'Test1234!'
  };
};

const getFakeEmail = (firstName: string, lastName: string) =>
  `${firstName.toLowerCase()}.${lastName.toLowerCase()}}@test.com`;