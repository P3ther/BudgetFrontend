import User from '../../../src/Interfaces/User.interface';
import apiCalls from '../../api_calls/apiCalls';
import { getUser } from '../../api_calls/User.helper';
import commonPage from '../../pages/common.page';
import loginPage from '../../pages/login.page';

describe('Login Tests for Users', () => {

  let testUser: User;

  beforeEach(() => {
    testUser = getUser();
    apiCalls.User.createUser(testUser);
    cy.visit('');
  });

  afterEach(() => {
    apiCalls.User.deleteUser(testUser);
  });

  it('Use can login with valid credentials', () => {
    cy.findByTestId(loginPage.locators.usernameInput).type(testUser.username);
    cy.findByTestId(loginPage.locators.passwordInput).type(testUser.password);
    cy.findByTestId(loginPage.locators.loginButton).click();
    commonPage.methods.waitforCommonPage();
  });

  it('Use can not login with invalid credentials', () => {
    cy.findByTestId(loginPage.locators.usernameInput).type(testUser.username);
    cy.findByTestId(loginPage.locators.passwordInput).type('Wrong Password');
    cy.findByTestId(loginPage.locators.loginButton).click();
    cy.findByTestId(loginPage.locators.usernameInputError).should('have.text', loginPage.messages.incorrectCredentials)
  });
});