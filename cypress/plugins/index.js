/// <reference types="cypress" />
import dotenv from 'dotenv';

dotenv.config({ path: '.env.local' });
dotenv.config();
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars
module.exports = (on, config) => {
  config.env.ADMIN_USERNAME = process.env.ADMIN_USERNAME;
  config.env.ADMIN_PASSWORD = process.env.ADMIN_PASSWORD;
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
  return config;
}
